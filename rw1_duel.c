/*
 Robot Warfare 2D
 ================

 Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/* rw1_duel.c history:

   1.x   : 11.1998-06.1999
   1.99  : 01.2000-03.2000
   2.0.x : 08.1999,06.2000,04.2001,08.2001
   2.1.x : 11.2002,11.2005
   2.2.x : 01.2013 (converted to C)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "rw1_win.h"
#include "rw1_duel.h"
#include "my_text.h"
#include "robotwar.h"

int Platform = 0;

int MyReg = 0;
int MyReg2 = 0;

char hostname[32];
int NUM = 1;
int rtime = 0;
char sglob[100];
short reg,dx,dy;
unsigned long *map;
Robot *r[MNUM];
int JobStep = 0;
int EndFight = 0;

FILE *fresult = NULL;

//int f_s = 0; // flag for sound
int f_f = 0; // flag fast running
//int f_n = 0; // flag for many robots
int f_i = 0; // flag for stats output
int f_g = 0; // flag for fast goto
int f_d = 0; // flag for debug mode
int f_gr = 0; // flag for group mode
int f_dm = 0; // flag for deathmatch mode
//int f_v = 0; // flag for variables window
int f_o = 0; // flag for clean output

Text *add;
Line *line;

int NeedStop = 0;
int SleepStep = 0;

struct group
{
  char author[16]; // name of the host of the group of robots
  short emax,mmax,num; // max energy, max missles and number of robots in the group
} gr[MNUM];

int ngr = 0;
int maxgr = 0;

struct mstru
{
  short n; // type of the object that was hit by the missle
  short x; // coordinate x if the object that was hit by the missle
  short y; // coordinate y if the object that was hit by the missle
} ms[MNUM];

struct sstru
{
  unsigned short time;
  unsigned short all_shoot;
  unsigned short good_shoot;
  unsigned short kill_shoot;
  unsigned short min_missle;
  unsigned short max_missle;
  unsigned short fin_missle;
  unsigned short min_energy;
  unsigned short max_energy;
  unsigned short fin_energy;
  unsigned short boxes;
  unsigned short damages;
  unsigned short reason;
} rwstat[MNUM];

int selrobot[MNUM];
int myrobot[MNUM];

void Sound(short i,short d);
void Fire1(short i,short j,short k,short kr);
void Fire2(short i,short j,short k,short kr);
void RShow(short k);
void DrawSelect(int x,int y,int k);

unsigned char povx[NPOV][RW1SIZE][RW1SIZE];
unsigned char povy[NPOV][RW1SIZE][RW1SIZE];

int rstep[MNUM];

char _last[100];

//void msg(char *s){MessageBox(0,s,"Message",MB_OK|MB_ICONEXCLAMATION);}

void Message(char *s,int k)
{
//  if(f_f>=200) return; // !!!
  int i,j;
  char str[256];
  if(s==NULL||*s==0)
  {
     if(k>=0) sprintf(str,"%5.5u: Robot%u\n",rtime,k+1);
     else     sprintf(str,"%5.5u\n",rtime);
  }
  else
  {
//     WriteShell(_last);WriteShell("!");WriteShell(s);WriteShell("\n");
     if(f_o) sprintf(str,"%s\n",s);
     else {
      if(k>=0) sprintf(str,"%5.5u: Robot%u : %s\n",rtime,k+1,s);
      else     sprintf(str,"%5.5u: %s\n",rtime,s);
     }
  }
  WriteShell(str);

//  MessageBox(0,str,"Message",MB_OK);
//  int ok=0;
//  str[30]=0;
//  for(i=0;i<30;i++)
//  {
//     if(str[i]==0) ok=1;
//     if(ok) DrawChar(8*i,172,' ',130,134);
//     else DrawChar(8*i,172,str[i],129,134);
//  }

}

void Message1(char *s)
{
  Message(s,-1);

}

//void delayf(int n)
//{
//  if(!f_f) delay(n);
//}

long colors[272] = {
    0x010000,
    0xAA0000,//0000AA,
    0x00AA00,
    0xAAAA00,//00AAAA,
    0x0000AA,//AA0000,
    0xAA00AA,
    0x0055AA,//AA5500,
    0xAAAAAA,
    0x555555,
    0xFF0000,//0000FF,
    0x00FF00,
    0xFFFF00,//00FFFF,
    0x0000FF,//FF0000,
    0xFF00FF,
    0x00FFFF,//FFFF00,
    0xFFFFFF,
    };

typedef struct _Texture
{
  int col;
  int row;
  int size;
  long *image;
  int ifree;
} Texture;

void TextureInit3(Texture *t,int c,int r,long *img)
{
   t->col = c;
   t->row = r;
   t->size = c*r;
   t->image = img;
   t->ifree = 0;
}
 
void TextureInit2(Texture *t,int c,int r)
{
   int i;
   long sz = c*r;
   if(sz>32767) exit(1);
   t->size = sz;
   t->col = c;
   t->row = r;
   t->image = (long*)malloc(sz*sizeof(long));
   if(t->image==NULL) exit(1);
   for(i=0;i<sz;i++) t->image[i]=0;
   t->ifree = 1;
}

void TextureInit(Texture *t)
{
   TextureInit2(t,32,32);
}

long TextureGetPixel(Texture *t,int i,int j)
{
  return t->image[j*t->col+i];
}

void TexturePutPixel(Texture *t,int i,int j,long c)
{
  t->image[j*t->col+i]=c;
}

void TexturePutPixel2(Texture *t,int i,int j,long c)
{
  int kk = (j<<1)*t->col+(i<<1);
  t->image[kk]=c;kk++;
  t->image[kk]=c;kk+=t->col;
  t->image[kk]=c;kk--;
  t->image[kk]=c;
}

void TextureRight(Texture *t)
{
   int b,i,j,ii,jj;
   ii = t->col/2;
   jj = t->row/2;
   for(i=0;i<ii;i++){
   for(j=0;j<jj;j++){
       b = TextureGetPixel(t,i,j);
       TexturePutPixel(t,i,j,TextureGetPixel(t,j,t->col-i-1));
       TexturePutPixel(t,j,t->col-i-1,TextureGetPixel(t,t->col-i-1,t->col-j-1));
       TexturePutPixel(t,t->col-i-1,t->col-j-1,TextureGetPixel(t,t->col-j-1,i));
       TexturePutPixel(t,t->col-j-1,i,b);
   }}
}

void TextureLeft(Texture *t)
{
   int b,i,j,ii,jj;
   ii = t->col/2;
   jj = t->row/2;
   for(i=0;i<ii;i++){
   for(j=0;j<jj;j++){
       b = TextureGetPixel(t,i,j);
       TexturePutPixel(t,i,j,TextureGetPixel(t,t->col-j-1,i));
       TexturePutPixel(t,t->col-j-1,i,TextureGetPixel(t,t->col-i-1,t->col-j-1));
       TexturePutPixel(t,t->col-i-1,t->col-j-1,TextureGetPixel(t,j,t->col-i-1));
       TexturePutPixel(t,j,t->col-i-1,b);
   }}
}

void TextureDraw(Texture *t,int x,int y)
{
  int i,j;
  long *ptr = t->image;
  for(j=0;j<RW1SIZE;j++){
  for(i=0;i<RW1SIZE;i++){
      if(*ptr) WritePixel(x+i,y+j,*ptr);
      ptr++;
  }}
}

Texture trobot[MNUM];

char SprStr[100];

// char code[16];
// int codelen = 0; // 9 every time!

// s - string
// f - inverse flag
// code - pointer to bytes

int spr_compile(char *_s,int f,char *code)
{
  char *poo,*po;
  char s[100];
  int codelen = 0;
  strcpy(s,_s);
  po = strchr(s,';');
  if(po!=NULL)
  {
    while(1)
    {
     *po = 0;
     if(po==s) return 0;
     po--;
     if(*po!=' ') break;
    }
  }
  po = s;
  while(*po!=' ') po++;
  while(*po==' ') po++;
  if(*po!='D') return 0;
  po++;
  if(*po!='B') return 0;
  po++;
  while(1)
  {
    while(*po==' ') po++;
    poo = strchr(po,',');
    if(poo!=NULL) *poo=0;
    if(*po=='#')
       code[codelen] = hex2i(po+1);
    else
       code[codelen] = atoi(po);
    if(f) code[codelen]=~code[codelen];
    codelen++;
    if(poo==NULL) break;
    poo++;
    po = poo;
  }
  return 1;
}

int ReadSpr(char *s)
{
  Line *lin;
  int i,j,k,k1,k2,x,y;
  char code[16];
  char image[64];
  Text *spr = TextNew();
  TextLoad(spr,s);
  for(lin=spr->first;lin!=NULL;lin=(Line*)lin->next)
  {
     if(*lin->str==0) continue;
     if(*lin->str==';') continue;
     spr_compile(lin->str,0,code);
//     WriteShell(lin->str);
     k1 = code[8]&0x0F;
     k2 = (code[8]&0xF0)>>4;
     j = 0;
     for(y=0;y<8;y++)
     {
         i = 0x80;
         for(x=0;x<8;x++)
         {
            if(code[y]&i)
               image[j] = k1;
            else
               image[j] = k2;
            i >>= 1;
            j++;
         }
     }
     WriteSprite(image);
  }
  TextDel(spr);
  return 1;
}

void InitPov(void)
{
  int i,j,ii,jj,k,x,y;
  double a,c,s;
  if(f_f>=200) return;
  for(k=0;k<NPOV;k++)
  {  a = MY_PI/2.0/(NPOV+1)*(k+1);
     c = cos(a);
     s = sin(a);
     for(i=0;i<RW1SIZE;i++){ ii=i-16;
     for(j=0;j<RW1SIZE;j++){ jj=j-16;
       x = ii*c-jj*s+16;
       y = ii*s+jj*c+16;
       if(x<0||y<0||x>=RW1SIZE||y>=RW1SIZE) x=y=0;
       povx[k][i][j] = x;
       povy[k][i][j] = y;
     }}
  }
}

void LeftPov(short n,short k)
{
   int i,j,x,y,b;
   if(f_f>=200) return;
   x = (RobotGetX(r[n]))<<SBIT;
   y = (RobotGetY(r[n]))<<SBIT;
   if(k<NPOV)
   {
     WriteTile(x,y,TILE_EMPTY,RW1SIZE);
     for(i=0;i<RW1SIZE;i++){
     for(j=0;j<RW1SIZE;j++){
       b = TextureGetPixel(&trobot[n],povx[k][i][j],povy[k][i][j]);
       if(b) WritePixel(x+i,y+j,b);
     }}
     DrawSelect(x,y,n);
   }
   else
   {
     TextureLeft(&trobot[n]);
     RShow(n);
   }
}

void RightPov(short n,short k)
{
   int i,j,x,y,b;
   if(f_f>=200) return;
   x = (RobotGetX(r[n]))<<SBIT;
   y = (RobotGetY(r[n]))<<SBIT;
   if(k==0) TextureRight(&trobot[n]);
   if(k<NPOV)
   {
     WriteTile(x,y,TILE_EMPTY,RW1SIZE);
     for(i=0;i<RW1SIZE;i++){
     for(j=0;j<RW1SIZE;j++){
       b = TextureGetPixel(&trobot[n],povx[NPOV-k-1][i][j],povy[NPOV-k-1][i][j]);
       if(b) WritePixel(x+i,y+j,b);
     }}
     DrawSelect(x,y,n);
   }
   else
   {
     RShow(n);
   }
}

void Sound(short i,short d)
{
   static char str[32];
   sprintf(str,"%i %i",i,d);
}

#define ENERGYCOLOR 0x00FFFF // RGB(200,255,0)
#define STATE1COLOR 0x000096 // RGB(150,0,0)
#define STATE2COLOR 0x00C8C8 // RGB(200,200,0)
#define STATE3COLOR 0x009600 // RGB(0,150,0)
#define MISSLECOLOR 0xFF3F00 // RGB(0,63,255)

void DrawDigit(int x,int y,int d)
{
   switch(d)
   {
     case 0:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x,y+1,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x,y+3,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 1:
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x+1,y+1,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 2:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 3:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 4:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x,y+1,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 5:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 6:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x,y+3,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 7:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 8:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x,y+1,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x,y+3,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
     case 9:
       WritePixel(x,y,MISSLECOLOR);
       WritePixel(x+1,y,MISSLECOLOR);
       WritePixel(x+2,y,MISSLECOLOR);
       WritePixel(x,y+1,MISSLECOLOR);
       WritePixel(x+2,y+1,MISSLECOLOR);
       WritePixel(x,y+2,MISSLECOLOR);
       WritePixel(x+1,y+2,MISSLECOLOR);
       WritePixel(x+2,y+2,MISSLECOLOR);
       WritePixel(x+2,y+3,MISSLECOLOR);
       WritePixel(x,y+4,MISSLECOLOR);
       WritePixel(x+1,y+4,MISSLECOLOR);
       WritePixel(x+2,y+4,MISSLECOLOR);
       break;
   }
}

void DrawSelect(int x,int y,int k)
{
   int ii,xx,yy,kk;
   if(!selrobot[k]) return;
   xx = x+1;
   yy = y+RW1SIZE-6;
   /* Energy Level */
   kk = r[k]->E;
   for(ii=0;ii<kk;ii++)
   {
      WritePixel(xx,yy,ENERGYCOLOR);
      WritePixel(xx+1,yy,ENERGYCOLOR);
      yy -= 2;
   }
   // My Robots
   if(myrobot[k])
      ii = 0x00FF00; // RGB(0,255,0);
   else
      ii = 0x0000FF; // RGB(255,0,0);
   yy = y+1;
   for(xx=x+RW1SIZE-8;xx<=x+RW1SIZE-2;xx++)
      WritePixel(xx,yy,ii);
   for(yy=y+1;yy<=y+8;yy++)
      WritePixel(xx,yy,ii);
   // Missle Number
   kk = r[k]->M;
   if(kk==0) return;
   xx = x+RW1SIZE-4;
   yy = y+RW1SIZE-6;
   while(1)
   {
      ii = kk%10;
      kk /= 10;
      DrawDigit(xx,yy,ii);
      if(kk==0) break;
      xx -= 4;
   }
}

void RShow(short k)
{
   int x,y;
   if(f_f>=200) return;
   x = (RobotGetX(r[k]))<<SBIT;
   y = (RobotGetY(r[k]))<<SBIT;
   WriteTile(x,y,TILE_EMPTY,RW1SIZE);
   if(r[k]->E>0)
   {
      TextureDraw(&trobot[k],x,y);
      DrawSelect(x,y,k);
   }
}

void RSend(int x, int y)
{
   int i;
   for(i=0;i<NUM;i++)
   {
      if(r[i]==NULL) continue;
      if(r[i]->E<=0) continue;
      if(selrobot[i] && myrobot[i])
      {
         RobotSend(r[i],MAPLO_(x,y)>>4,-1,rtime,x,y);
      }
   }
}

void RSelect(int x1, int y1, int x2, int y2)
{
   int i,ii,jj;
   for(i=0;i<NUM;i++)
   {
      if(r[i]==NULL) continue;
      if(r[i]->E<=0) continue;
      ii = RobotGetX(r[i]);
      jj = RobotGetY(r[i]);
      if(ii>=x1&&ii<=x2&&jj>=y1&&jj<=y2)
         selrobot[i] = 1;
      else
         selrobot[i] = 0;
      RShow(i);
   }
}

#define NUMCOM 4000

short ncom=0,ncom2;
short command[NUMCOM];
short comi[NUMCOM];
short comx[NUMCOM];
short comy[NUMCOM];
short comk[NUMCOM];
short comn[NUMCOM];
short comt[NUMCOM];
short comd[NUMCOM];

void RoboDead(int nn,int kk)
{ int ii;
  if(r[nn]->E<=0) return;
  for(ii=0;ii<kk;ii++)
  {
     if(RobotDamage(r[nn])<=0)
     {
        MAP_(RobotGetX(r[nn]),RobotGetY(r[nn]),0);
        break;
     }
  }
}

#define KFIRE1 4
#define KFIRE2 6

void Fire1(short i,short j,short k,short kr)
{
  short ii,jj,kk,nn;
  short x = i<<SBIT;
  short y = j<<SBIT;
  switch(k)
  {
    case 0:
      MAP_(i,j,0);
      if(f_f<200) //tfire.Draw(x,y);
         WriteTile(x,y,TILE_FIRE,RW1SIZE);
      break;
    case 1:
      if(f_f<200)
      {
       if(y>=RW1SIZE)
          WriteTile(x,y-RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x<dx-RW1SIZE)
          WriteTile(x+RW1SIZE,y,TILE_FIRE,RW1SIZE);
       if(y<dy-RW1SIZE)
          WriteTile(x,y+RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x>=RW1SIZE)
          WriteTile(x-RW1SIZE,y,TILE_FIRE,RW1SIZE);
      }
      break;
    case 2:
      if(f_f<200)
      {
       if(x>=RW1SIZE&&y>=RW1SIZE)    // tfire.Draw(x-RW1SIZE,y-RW1SIZE);
          WriteTile(x-RW1SIZE,y-RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x<dx-RW1SIZE&&y>=RW1SIZE)  // tfire.Draw(x+RW1SIZE,y-RW1SIZE);
          WriteTile(x+RW1SIZE,y-RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x<dx-RW1SIZE&&y<dy-RW1SIZE)// tfire.Draw(x+RW1SIZE,y+RW1SIZE);
          WriteTile(x+RW1SIZE,y+RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x>=RW1SIZE&&y<dy-RW1SIZE)  // tfire.Draw(x-RW1SIZE,y+RW1SIZE);
          WriteTile(x-RW1SIZE,y+RW1SIZE,TILE_FIRE,RW1SIZE);
      }
      break;
    case 3:
      for(x=-1;x<=1;x++){ ii=i+x;
      for(y=-1;y<=1;y++){ jj=j+y;
          if(ii>=0&&ii<DDX&&jj>=0&&jj<DDY)
          {  if((MAPLO_(ii,jj)>>4)==6)
             {
               for(nn=0;nn<NUM;nn++)
               {
                if(RobotGetX(r[nn])==ii&&RobotGetY(r[nn])==jj)
                {
                   RoboDead(nn,3);
                   RShow(nn);
                   rwstat[kr].good_shoot++;
                   if(r[nn]->E<=0)
                   {  rwstat[kr].kill_shoot++;
                      rwstat[nn].reason = 3;
                   }
                   rwstat[nn].damages+=3;
                }
               }
             }
             else
             {
                if((MAPLO_(ii,jj)>>4)==3)
                {  command[ncom]=0x101;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((MAPLO_(ii,jj)>>4)==4)
                {  command[ncom]=0x102;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((MAPLO_(ii,jj)>>4)!=1)
                {  MAP_(ii,jj,0);
                   if(f_f<200) //tempty.Draw(ii<<SBIT,jj<<SBIT);
                      WriteTile(ii<<SBIT,jj<<SBIT,TILE_EMPTY,RW1SIZE);
                }
                else if(f_f<200) //thole.Draw(ii<<SBIT,jj<<SBIT);
                        WriteTile(ii<<SBIT,jj<<SBIT,TILE_EMPTY,RW1SIZE);
             }
          }
      }}
      break;
  }
//  if(f_f<200) TraceWait();
}



void Fire2(short i,short j,short k,short kr)
{
  short ii,jj,kk,nn;
  short x = i<<SBIT;
  short y = j<<SBIT;
  short siz2 = RW1SIZE<<1;
  switch(k)
  {
    case 0:
      MAP_(i,j,1<<4);
      if(f_f<200)
      {
//       tfire.Draw(x,y);
       WriteTile(x,y,TILE_FIRE,RW1SIZE);
//    case 1:
       if(y>=RW1SIZE)   //tfire.Draw(x,y-RW1SIZE);
          WriteTile(x,y-RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x<dx-RW1SIZE) //tfire.Draw(x+RW1SIZE,y);
          WriteTile(x+RW1SIZE,y,TILE_FIRE,RW1SIZE);
       if(y<dy-RW1SIZE) //tfire.Draw(x,y+RW1SIZE);
          WriteTile(x,y+RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x>=RW1SIZE)   //tfire.Draw(x-RW1SIZE,y);
          WriteTile(x-RW1SIZE,y,TILE_FIRE,RW1SIZE);
      }
      break;
    case 1:
      if(f_f<200)
      {
       if(x>=RW1SIZE&&y>=RW1SIZE)     //tfire.Draw(x-RW1SIZE,y-RW1SIZE);
          WriteTile(x-RW1SIZE,y-RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x<dx-RW1SIZE&&y>=RW1SIZE)   //tfire.Draw(x+RW1SIZE,y-RW1SIZE);
          WriteTile(x+RW1SIZE,y-RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x<dx-RW1SIZE&&y<dy-RW1SIZE) //tfire.Draw(x+RW1SIZE,y+RW1SIZE);
          WriteTile(x+RW1SIZE,y+RW1SIZE,TILE_FIRE,RW1SIZE);
       if(x>=RW1SIZE&&y<dy-RW1SIZE)   //tfire.Draw(x-RW1SIZE,y+RW1SIZE);
          WriteTile(x-RW1SIZE,y+RW1SIZE,TILE_FIRE,RW1SIZE);
      }
      break;
    case 2:
      if(f_f<200)
      {
       if(x>=siz2)   //tfire.Draw(x-siz2,y);
          WriteTile(x-siz2,y,TILE_FIRE,RW1SIZE);
       if(y>=siz2)   //tfire.Draw(x,y-siz2);
          WriteTile(x,y-siz2,TILE_FIRE,RW1SIZE);
       if(x<dx-siz2) //tfire.Draw(x+siz2,y);
          WriteTile(x+siz2,y,TILE_FIRE,RW1SIZE);
       if(y<dy-siz2) //tfire.Draw(x,y+siz2);
          WriteTile(x,y+siz2,TILE_FIRE,RW1SIZE);
      }
      break;
    case 3:
      for(ii=-2;ii<=2;ii++){
      for(jj=-2;jj<=2;jj++){
         x = ii+i;
         y = jj+j;
         if(x>=0&&y>=0&&x<DDX&&y<DDY&&f_f<200) //tfire.Draw(x<<SBIT,y<<SBIT);
            WriteTile(x<<SBIT,y<<SBIT,TILE_FIRE,RW1SIZE);
      }}
      break;
    case 4:
      for(x=-2;x<=2;x++){ ii=i+x;
      for(y=-2;y<=2;y++){ jj=j+y;
          if(ii>=0&&ii<DDX&&jj>=0&&jj<DDY)
          {  if((MAPLO_(ii,jj)>>4)==6)
             {
               for(nn=0;nn<NUM;nn++)
               {
                if(RobotGetX(r[nn])==ii&&RobotGetY(r[nn])==jj)
                {
                   RoboDead(nn,5);
                   RShow(nn);
                   rwstat[kr].good_shoot++;
                   if(r[nn]->E<=0)
                   {  rwstat[kr].kill_shoot++;
                      rwstat[nn].reason = 5;
                   }
                   rwstat[nn].damages+=5;
                }
               }
             }
             else
             {
                if((MAPLO_(ii,jj)>>4)==3)
                {  command[ncom]=0x101;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((MAPLO_(ii,jj)>>4)==4)
                {  command[ncom]=0x102;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((MAPLO_(ii,jj)>>4)!=1)
                { MAP_(ii,jj,0);
                  if(f_f<200) //tempty.Draw(ii<<SBIT,jj<<SBIT);
                     WriteTile(ii<<SBIT,jj<<SBIT,TILE_EMPTY,RW1SIZE);
                }
                else if(f_f<200) //thole.Draw(ii<<SBIT,jj<<SBIT);
                        WriteTile(ii<<SBIT,jj<<SBIT,TILE_HOLE,RW1SIZE);

             }
          }
      }}
      break;
  }
//  if(f_f<200) TraceWait();
}

void htrace(int x,int y,int xk)
{
  int i,j;
  if(f_f>=200) return;
  for(i=x;i<=xk;i+=4)
  {
    for(j=y+ 6;j<y+10;j++) WritePixel(i,j,TCOL);
    for(j=y+22;j<y+26;j++) WritePixel(i,j,TCOL);
  }
}

void vtrace(int x,int y,int yk)
{
  int i,j;
  if(f_f>=200) return;
  for(j=y;j<=yk;j+=4)
  {
    for(i=x+ 6;i<x+10;i++) WritePixel(i,j,TCOL);
    for(i=x+22;i<x+26;i++) WritePixel(i,j,TCOL);
  }
}

void exits(char *s,int e)
{
   char str[256],st2[256];
   strcpy(str,"\n\n");
   if(e==0) strcat(str,"Out of memory: ");
   if(e==1) strcat(str,"Unknown error: ");
   if(e==2) strcat(str,"File doesn't exist: ");
   if(e==3) strcat(str,"Can't open file: ");
   if(e==4) strcat(str,"Too many options: ");
   sprintf(st2,"%s %s !\n\n",str,s);
   WriteFatal(st2);
   exit(-1);
}

void exits1(char *s)
{
  exits(s,-1);
}

FILE *fout = NULL;

void commands(char *fstr)
{  int i;
   if(f_d>=2&&ncom)
   {
      fout=fopen(FILEOUT,"at");
      fprintf(fout,">>> T=%u %s COMMANDS ",rtime,fstr);
      fprintf(fout,"ncom=%u ncom2=%u\n",ncom,ncom2);
      for(i=0;i<ncom;i++)
      {
         fprintf(fout,"%u) ",i);
         if(i<ncom2) fprintf(fout," ");
         else fprintf(fout,"*");
         fprintf(fout,"[%4.4X]",command[i]);
         fprintf(fout," n=%i",comn[i]);
         fprintf(fout," x=%i",comx[i]);
         fprintf(fout," y=%i",comy[i]);
         fprintf(fout," k=%i",comk[i]);
         fprintf(fout," t=%i",comt[i]);
         fprintf(fout," d=%i",comd[i]);
         fprintf(fout,"\n");
      }
      fclose(fout);
   }
}

  int rn[MNUM];
  int re[MNUM];
  int rm[MNUM];
  int ra[MNUM];

  unsigned short zapRand;

int rw1_init(int argc0,char **argv0)
{
  int argc;
  char **argv;
  char *poa,*po,*poo;
  FILE *flist,*f;
  char str[100],str2[100];
  int i,j,k,kk1,kk2,a[MNUM];
  int i1,i2,i3,j1,j2,j3,nn,k2,ok,irob;

  for(i=0;i<MNUM;i++) 
  {
    trobot[i].size = 0;
    trobot[i].image = NULL;
    trobot[i].ifree = 0;
  }

  EndFight = 0;

  Rand = time(NULL) & 0xFF;
  if(Rand==0) Rand++;

  add = TextNew();

  for(i=0;i<MNUM;i++)
  {
     selrobot[i] = 0;
     myrobot[i] = 0;
  }
  strcpy(hostname,"@");

  argv = (char**)malloc(ARGCMAX*sizeof(char*));
  if(argv==NULL) exits("argv",0);
  j = 0;
  for(i=0;i<argc0;i++)
  {
     argv[j] = NULL;
     if(!argv0[i][0]) continue;
     if(argv0[i][0]=='@')
     {
        poa = &argv0[i][1];
        flist = fopen(poa,"rt");
        if(flist==NULL) exits(poa,2);
        while(1)
        {
          fgets(str,100,flist);
          if(feof(flist)) break;
          if(*str=='\n') continue;
          argv[j] = (char*)malloc(strlen(str)+1);
          if(argv[j]==NULL) exits("argv[]",0);
          po = str;
          k = 0;
          while(*po!='\n')
          {
//            if(*po==' ') continue;
//            if(*po=='\t') continue;
            if(*po==0) break;
            argv[j][k++] = *po;
            po++;
          }
          argv[j][k] = 0;
          if(++j>=ARGCMAX) exits("!",4);
        }
        fclose(flist);
     }
     else
     {
        argv[j] = (char*)malloc(strlen(argv0[i])+1);
        if(argv[j]==NULL) exits("argv[]",0);
        strcpy(argv[j],argv0[i]);
        j++;
     }
     if(j>=ARGCMAX) exits("!",4);
  }
  argc = j;

  reg = 1;
//  badreg = 0;

  for(i=0;i<MNUM;i++)
  {
     re[i] = ENERGYBEG;
     rm[i] = MISSLEBEG;
     ra[i] = -1;
  }

  i = 0;
  for(k=1;k<argc;k++)
  {
     if(argv[k][0]=='-')
     {
        switch(argv[k][1])
        {
           case 'm':
              po = &argv[k][2];
              NUM = atoi(po);
              if(NUM>MNUM) NUM=MNUM;
//              f_n = 1;
              break;
           case 'n':
              if(argv[k][2]=='#')
              {
                po = &(argv[k][3]);
                Rand = hex2i(po);
              }
              else
              {
                po = &(argv[k][2]);
                Rand = atoi(po);
              }
              break;
           case 'g':
              if(argv[k][2]=='r')
              {  f_gr = 1;
                 break;
              }
              po = &(argv[k][2]);
              f_g=atof(po);
              f_f=10;
              break;
           case 'i':
              f_i = 1;
              break;
           case 'd':
              if(argv[k][2]==0) f_d=1;
              if(argv[k][2]=='2') f_d=2;
              if(argv[k][2]=='3') f_d=3;
              if(argv[k][2]=='m') f_dm=1;
              if(argv[k][2]=='x') DDX=atoi(&argv[k][3]);
              if(argv[k][2]=='y') DDY=atoi(&argv[k][3]);
              break;
           case 'f':
              po = &(argv[k][2]);
              f_f = atoi(po);
              if(f_f==0) f_f=10;
              if(f_f==2) f_f=200;
              break;
           case 's':
              if(isdigit(argv[k][2]))
              {  // select robots
                 j = atoi(&argv[k][2]);
                 if(j) selrobot[j-1]=1;
                 else for(j=0;j<MNUM;j++) selrobot[j]=1;
                 break;
              }
              break;
           case 'h':
              po = &argv[k][2];
              strcpy(hostname,po);
              break;
           case 'a':
              strcpy(str,&argv[k][2]);
              line = TextAdd(add,str);
              line->id = 0;
              line->id2 = 0;
              line->type = 0;
              line->adr = 0;
              poo = str;
              po = str;
              while(*po!=',')
              {
                 if(!*po) break;
                 po++;
              }
              if(*po)
              {
                 *po=0;po++;
              }
              line->id = atoi(poo);
              poo = po;
              while(*po!=',')
              {
                 if(!*po) break;
                 po++;
              }
              if(*po)
              {
                 *po=0;po++;
              }
              line->id2 = atoi(poo);
              poo = po;
              while(*po!=',')
              {
                 if(!*po) break;
                 po++;
              }
              if(*po)
              {
                 *po=0;po++;
              }
              line->type = atoi(poo);
              if(line->type==6) line->adr=2;
              if(line->type==2) line->adr=5;
              poo = po;
              if(*po)
              {
                 line->adr = atoi(poo);
                 //poo = po;
              }
              break;
           case 'p':
              f_f = 5;
              Platform = argv[k][2]-'0';
              if(argv[k][3]!='-') break;
              strcpy(SprStr,&argv[k][4]);
              break;
           case 'o':
              f_o = 1;
              break;
           default:
              if(isdigit(argv[k][1]) && i>0)
              {
                 po = strchr(argv[k],'/');
                 if(po!=NULL)
                 {
                    po++;
                    rm[i-1] = atoi(po);
                 }
                 po = &argv[k][1];
                 re[i-1] = atoi(po);
                 if(re[i-1]>ENERGYMAX) re[i-1]=ENERGYMAX;
              }
              else exits1("Bad options");
              break;
        }
     }
     else
     {
        FILE *f11 = fopen(argv[k],"rb");
        if(f11==NULL) exits(argv[k],2);
        fclose(f11);
        rn[i++] = k;
     }
  }

  ok = 0;
  irob = i;
  if(irob>NUM&&irob<MNUM) NUM=irob;
  if(Platform) Rand = 0;
  zapRand = Rand;

  map = MakeMap(NUM,DDX,DDY);

  for(line=add->first;line!=NULL;line=(Line*)line->next)
  {
     #if 0
      WriteShell(line->str);WriteShell("\n");
      WriteShell("Object: ");WriteShellInt(line->type);
      WriteShell("Alpha: ");WriteShellInt(line->adr);
      WriteShell("X = ");WriteShellInt(line->id);
      WriteShell("Y = ");WriteShellInt(line->id2);
     #endif
     if(line->id<DDX&&line->id2<DDY)
     {
        if(line->type>=6)
           ra[ok++] = line->adr;
        if(line->type<6)
           MAP_(line->id,line->id2,(line->type<<4)|line->adr);
        if(line->type>=6&&ok<=NUM)
           MAP_(line->id,line->id2,(line->type<<4)|((ok-1)<<8));
     }
  }
  if(!zapRand) for(i=ok;i<NUM;i++) MAP_(MyRandom(DDX),MyRandom(DDY),0x60|(i<<8));

  if(f_d)
  {  fout=fopen(FILEOUT,"wt");
     if(fout==NULL) exits(FILEOUT,3);
  }
  if(f_d>=2) fclose(fout);

  po = strrchr(argv[rn[0]],'.');
  ok = 0;
  if(!stricmp(po,".rw1")) ok=1;
  if(!stricmp(po,".rw0")) ok=2;
  if(!ok) exits1("File extention is bad");
//  if(f_f>=200) badreg=0;
  for(i=0;i<MNUM;i++) r[i]=NULL;
  for(i=0;i<NUM;i++)
  {
     if(i<irob) j=rn[i];
     TextureInit(&trobot[i]);
     r[i]=RobotNew(i+1,argv[j]);
     if(r[i]==NULL) exits1("new Robot");
     r[i]->Platform = Platform;
     if(ra[i]>=0) r[i]->angle = ra[i];
  }
  if(*hostname=='@'&&NUM) strcpy(hostname,RobotGetAuthor(r[0]));

  for(i=0;i<ARGCMAX;i++)
  {
    if(i<argc) free(argv[i]);
  }
  free(argv);

  TextDel(add);

  return 1;
}


int rw1_draw(void)
{
  int i,j,k,kk1,kk2;
  int x[MNUM],y[MNUM];
  short col[MNUM];
  long kk;
  char *pstr;
  if(Platform==2) ReadSpr(SprStr);
  if(f_f<200)
  {
    k = 0;
    for(j=0;j<DDY;j++){
    for(i=0;i<DDX;i++){
        kk1 = (map[k]&0xFF)>>4;
        kk2 = map[k]&0x0F;
        k++;
        switch(kk1)
        {
           case 0: WriteTile(i<<SBIT,j<<SBIT,TILE_EMPTY,RW1SIZE);
                   break;
           case 1: WriteTile(i<<SBIT,j<<SBIT,TILE_HOLE,RW1SIZE);
                   break;
           case 2: WriteTile(i<<SBIT,j<<SBIT,TILE_STONE1+kk2-1,RW1SIZE);
                   break;
           case 3: WriteTile(i<<SBIT,j<<SBIT,TILE_PILE1,RW1SIZE); // TODO: pile rotation
                   break;
           case 4: WriteTile(i<<SBIT,j<<SBIT,TILE_REACTOR,RW1SIZE);
                   break;
           case 6: kk2 = map[k-1]>>8;
                   x[kk2] = i;
                   y[kk2] = j;
                   WriteTile(i<<SBIT,j<<SBIT,TILE_EMPTY,RW1SIZE);
                   break;
        }
    }}
    InitPov();
  }
  else
  {
    for(i=0;i<DDX;i++){
    for(j=0;j<DDY;j++){
        if((MAPLO_(i,j)>>4)==6)
        {
          k = MAPHI_(i,j);
          x[k] = i;
          y[k] = j;
        }
    }}
  }

//  return 1;

  for(k=0;k<NUM;k++)
  {
    rwstat[k].time = 0;
    rwstat[k].all_shoot = 0;
    rwstat[k].good_shoot = 0;
    rwstat[k].kill_shoot = 0;
    rwstat[k].min_missle = 100;
    rwstat[k].max_missle = 0;
    rwstat[k].fin_missle = 100;
    rwstat[k].min_energy = 100;
    rwstat[k].max_energy = 0;
    rwstat[k].fin_energy = 0;
    rwstat[k].boxes = 0;
    rwstat[k].damages = 0;
    rwstat[k].reason = 0;
    ms[k].n=-1;
    col[k]=16+k;
    colors[16+k] = (RobotGetB(r[k])<<16)|(RobotGetG(r[k])<<8)|RobotGetR(r[k]);
    if(colors[16+k]==0) colors[16+k]=BCOL;
    RobotSetLoc(r[k],x[k],y[k]);
    RobotSetMap(r[k],DDX,DDY,map);
    for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      if(r[k]->fimage) kk=colors[RobotGetI(r[k],i,j)];
      else kk=colors[col[k]];
#if 0
      if(kk==0) kk=BCOL;
#endif
      if(f_f<200) TexturePutPixel2(&trobot[k],i+4,j+4,kk);
    }}
    if(r[k]->fimage) kk=colors[col[k]];
    else kk=BCOL;

    if(f_f<200&&!Platform)
    {
       for(i=0;i<10;i++)
       {
          TexturePutPixel2(&trobot[k],3,i+3,kk);
          TexturePutPixel2(&trobot[k],12,i+3,kk);
          TexturePutPixel2(&trobot[k],i+3,3,kk);
          TexturePutPixel2(&trobot[k],i+3,12,kk);
       }
       for(i=0;i<4;i++)
       {
         if(RobotGetE(r[k],i)==GUN)
         {
          for(j=0;j<2;j++)
          {
             switch(i)
             {
               case 1:
                 TexturePutPixel2(&trobot[k],13+j,7,BCOL);
                 TexturePutPixel2(&trobot[k],13+j,8,BCOL);
                 break;
               case 0:
                 TexturePutPixel2(&trobot[k],7,2-j,BCOL);
                 TexturePutPixel2(&trobot[k],8,2-j,BCOL);
                 break;
               case 3:
                 TexturePutPixel2(&trobot[k],2-j,7,BCOL);
                 TexturePutPixel2(&trobot[k],2-j,8,BCOL);
                 break;
               case 2:
                 TexturePutPixel2(&trobot[k],7,13+j,BCOL);
                 TexturePutPixel2(&trobot[k],8,13+j,BCOL);
                 break;
             }
          }
         }
         if(RobotGetE(r[k],i)==EYE)
         {
          for(j=0;j<2;j++)
          {
             switch(i)
             {
               case 1:
                 TexturePutPixel2(&trobot[k],13,j+5,BCOL);
                 TexturePutPixel2(&trobot[k],13,j+7,colors[11]);
                 TexturePutPixel2(&trobot[k],13,j+9,BCOL);
                 break;
               case 0:
                 TexturePutPixel2(&trobot[k],j+5,2,BCOL);
                 TexturePutPixel2(&trobot[k],j+7,2,colors[11]);
                 TexturePutPixel2(&trobot[k],j+9,2,BCOL);
                 break;
               case 3:
                 TexturePutPixel2(&trobot[k],2,j+5,BCOL);
                 TexturePutPixel2(&trobot[k],2,j+7,colors[11]);
                 TexturePutPixel2(&trobot[k],2,j+9,BCOL);
                 break;
               case 2:
                 TexturePutPixel2(&trobot[k],j+5,13,BCOL);
                 TexturePutPixel2(&trobot[k],j+7,13,colors[11]);
                 TexturePutPixel2(&trobot[k],j+9,13,BCOL);
                 break;
             }
          }
         }
       }

       switch(r[k]->angle)
       {
         case 2: TextureLeft(&trobot[k]);
                 break;
         case 3: TextureRight(&trobot[k]);
         case 0: TextureRight(&trobot[k]);
         case 1: break;
       }
       RShow(k);
    }
  }

  for(i=0;i<NUM;i++)
  {
      k = 0;
      for(j=0;j<maxgr;j++)
      {
        if(!strcmp(gr[j].author,RobotGetAuthor(r[i]))){k=1;break;}
      }
      if(!k)
      {
        maxgr++;
        strcpy(gr[j].author,RobotGetAuthor(r[i]));
      }
      r[i]->E = re[i];
      r[i]->M = rm[i];
      RobotSetLast(r[i],_last);
      RobotSetRar(r[i],&r,NUM,f_gr);
      rstep[i] = -1;
      if(!strcmp(hostname,RobotGetAuthor(r[i]))) myrobot[i]=1;
  }

  if(f_i){unlink(FILEIOUT);fresult=fopen(FILEITMP,"wt");}
  else    fresult=NULL;

  if(f_i && fresult!=NULL)
  {
    fprintf(fresult,"* ver=" VERSION "\n");
    fprintf(fresult,"* num=%u\n",NUM);
    fprintf(fresult,"* dx=%u\n",DDX);
    fprintf(fresult,"* dy=%u\n",DDY);
    fprintf(fresult,"* rand=#%4.4X\n",zapRand);
  }

  rtime = 0;

/*
   for(i=0;i<sprNum;i++)
   {
      BitBlt(memDC,i<<SBIT,9<<SBIT,RW1SIZE,RW1SIZE,sprDC[i],0,0,SRCCOPY);
   }
*/

  return 1;
}

/*
  if(f_f<200)
  {

//    dac->SetHardPalette();

    DrawBar(0,160,320,40,134);
                    //0123456789012345678901234567890123456789
    DrawString(0,162,rwtitle,140,130);
    DrawString(0,172,"                               TIME    0",130,134);
    for(i=0;i<2;i++)
    {
      sprintf(str,"%s",r[i]->GetName());
      DrawString(56,181+i*10,str,143,134);
      DrawChar(48,181+i*10,':',130,134);
      sprintf(str,"ROBOT%u",i+1);
      DrawString(0,181+i*10,str,col[i],134);
    }

  //  Message("ROBOTS were started");

  //  for(i=0;i<5;i++)
  //  {  Fire1(MyRandom(20),MyRandom(10));
  //     Fire2(MyRandom(20),MyRandom(10));
  //     for(j=0;j<=NPOV;j++)
  //     {  LeftPov(1,j);
  //        RightPov(0,j);
  //     }
  //     getch();
  //  }

  }
*/

int substep = 0;
int ok = 1;
int ibre = 2000;

unsigned long rw1_step(void* param)
{
 static char str[2048];
 static int i,j,k,a[MNUM];
 static int i1,i2,i3,j1,j2,nn,k2,ix,iy,ibreak;
 static int mmi,ii;
 static int edrobo;
 static char *pstr,st1[256];
 static int old_f_f = -1;
 static int wins;

 sprintf(st1,"0x%8.8X",param);

// if(f_s && !SoundStep()) SoundRewind();

 if(EndFight) return 0;

 JobStep = 1;

//! while(!NeedStop)
//! {
//!  if(f_f==0) Sleep((int)TIMER_MS);
//!  while(SleepStep);

  if(substep==0)
  {

//  if(f_f>=200)
//  {
//     clrscr();
//     for(k=0;k<NUM;k++)
//     {
//        gotoxy(1,k+2);
//        cprintf("ROBOT %u '%s'",k+1,r[k]->GetName());
//     }
//  }
//  while(ok) // MAINLOOP
//  {
//     if(kbhit()&&getch()==27) break;

sprintf(str,"%u",rtime++);  // !!!

//     if(f_f<200) DrawString(320-strlen(str)*8,172,str,130,134);
//     if(f_f>=200&&rtime%123==0)
//     {
//        gotoxy(1,1);
//        cprintf(str);
//     }

     if(f_i&&rtime>=TIMEMAX) ok=0;

     if(f_g>0&&rtime>=f_g) f_g=f_f=0;
//     if(f_g==0&&f_d==2&&rtime>1) if(getch()==27) break;
     if(f_dm&&(rtime%TIMEDM)==0)
     {
        for(j=0;j<_N_box;j++)
        {
            if((MAPLO_(_X_box[j],_Y_box[j])>>4)==0)
            {   MAP_(_X_box[j],_Y_box[j],0x30);
                if(f_f<200) //tbox.Draw(_X_box[j]<<SBIT,_Y_box[j]<<SBIT);
                   WriteTile(_X_box[j]<<SBIT,_Y_box[j]<<SBIT,TILE_PILE1,RW1SIZE);
            }
        }
     }
     if(f_g==0&&f_d>=2)
     {  fout=fopen(FILEOUT,"at");
        if(fout==NULL) exits(FILEOUT,3);
     }

     if(f_gr)
     {
       for(k=0;k<maxgr;k++)
       {
         gr[k].emax = 0;
         gr[k].mmax = 0;
         gr[k].num = 0;
         for(ii=0;ii<NUM;ii++)
         {
            if(!strcmp(RobotGetAuthor(r[ii]),gr[k].author))
            {
               if(r[ii]->E <= 0) continue;
               gr[k].num++;
               if(r[ii]->E > gr[k].emax) gr[k].emax=r[ii]->E;
               if(r[ii]->M > gr[k].mmax) gr[k].mmax=r[ii]->M;
            }
         }

         if(k<2&&f_f<200) // ???
         {
         /*  sprintf(str,"%s [%u]",gr[k].author,gr[k].num);
           DrawString(56,181+k*10,str,143,134);
           DrawChar(STARTE,181+k*10,':',130,134);
           for(ii=0;ii<13;ii++)
           {
              if(ii<gr[k].emax) jj=0x7F;
              else jj=0x20;
              DrawChar(STARTE+(ii+1)*8,181+k*10,jj,143,134);
           }
           sprintf(str,"%u",gr[k].mmax);
           TraceWait();
           DrawString(288,181+10*k,"    ",143,134);
           DrawString(320-strlen(str)*8,181+10*k,str,143,134);*/
         }
       }
     }


     for(k=0;k<NUM;k++)
     {
        i = RobotGetX(r[k]);
        j = RobotGetY(r[k]);
        a[k] = r[k]->angle;

        if(MyReg2) r[k]->E -= 3;

        if(r[k]->E>0)
        {

           #ifdef FULLDEB
           if(f_g==0&&f_d>0)
           {
               fprintf(fout,"%4.4u <%s>\t",rtime,RobotGetName(r[k]));
               fprintf(fout,"[%2.2X] ",RobotGetCode(r[k],r[k]->P));
               fprintf(fout,"PC=%u ",r[k]->P);
               fprintf(fout,"SP=%i ",VARMAX-1-r[k]->sp);
               fprintf(fout,">>> ");
           }
           #endif

        /* <><><><><><><><><><><><><><> */
           pstr = RobotStep(r[k],fout);
        /* <><><><><><><><><><><><><><> */

           #ifdef FULLDEB
           if(f_g==0&&f_d>0)
           {
               fprintf(fout,"T=%i ",r[k]->T);
               fprintf(fout,"X=%i ",r[k]->X);
               fprintf(fout,"Y=%i ",r[k]->Y);
               fprintf(fout,"E=%i ",r[k]->E);
               fprintf(fout,"M=%i ",r[k]->M);
               fprintf(fout,"N=%i ",r[k]->N);
               fprintf(fout,"D=%i ",r[k]->D);
               fprintf(fout,"K=%i ",r[k]->K);
               fprintf(fout,"R=%i ",r[k]->R);
               fprintf(fout,"comm=%4.4X\n",r[k]->command);

               #ifdef MAPDRAW
               fprintf(fout,"-------map-------\n");
               for(int jm=0;jm<10;jm++){
               for(int im=0;im<20;im++){
                   fprintf(fout," %2.2X",MAPLO(im,jm));
               }
               fprintf(fout,"\n");
               }
               fprintf(fout,"-------map-------\n");
               #endif
           }
           #endif

           rwstat[k].time = rtime; // r[k]->T;

           if(r[k]->E<=0)
           {
              if((r[k]->command>>8)==5)
              {
                rwstat[k].reason=7;
                sprintf(str,"Robot %u died by send !",k+1);
              }
              else
              {
                rwstat[k].reason=4;
                sprintf(str,"Robot %u died by radar !",k+1);
              }
              Message1(str);
              RShow(k);
           }

           if(r[k]->halt)
           {
              r[k]->E=0;
              rwstat[k].reason=6;
              sprintf(str,"Robot %u : HALT !",k+1);
              Message1(str);
              RShow(k);
           }
        }
        if(rwstat[k].fin_missle < r[k]->M) rwstat[k].boxes++;
        if(rwstat[k].min_missle > r[k]->M) rwstat[k].min_missle=r[k]->M;
        if(rwstat[k].max_missle < r[k]->M) rwstat[k].max_missle=r[k]->M;
        rwstat[k].fin_missle = r[k]->M;
        if(rwstat[k].min_energy > r[k]->E) rwstat[k].min_energy=r[k]->E;
        if(rwstat[k].max_energy < r[k]->E) rwstat[k].max_energy=r[k]->E;
        rwstat[k].fin_energy = r[k]->E;

/*        if(k<2&&f_f<200)
        {
          int ii,jj;
          DrawChar(STARTE,181+k*10,':',130,134);
          for(ii=0;ii<13;ii++)
          {
             if(ii<r[k]->E) jj=0x7F;
             else jj=0x20;
             DrawChar(STARTE+(ii+1)*8,181+k*10,jj,143,134);
          }
          sprintf(str,"%u",r[k]->M);
          TraceWait();
          DrawString(288,181+10*k,"    ",143,134);
          DrawString(320-strlen(str)*8,181+10*k,str,143,134);
        }
*/
        if(RobotGetX(r[k])<0||r[k]->E<=0) /* robot is dead */
        {
           if(RobotGetX(r[k])>0&&RobotGetY(r[k])>0)
              MAP_(RobotGetX(r[k]),RobotGetY(r[k]),0);
           RobotSetX(r[k],-1);
           RobotSetY(r[k],-1);
           continue; // ???????
        }
        if(pstr!=NULL)
        {
           if(f_f<200) // !!!
              Message(pstr,k);
        }
        if(r[k]->command)
        {
#if 0
           WriteShell("r[k]->command = ");
           WriteShellInt(r[k]->command);
#endif
           command[ncom] = 0x1000 + (r[k]->command&0xF00) + k;
           comi[ncom] = r[k]->command_i;
           comx[ncom] = RobotGetX(r[k]);
           comy[ncom] = RobotGetY(r[k]);
           comt[ncom] = 0;
           comd[ncom] = 0;
           comn[ncom] = 0;
           comk[ncom++] = r[k]->command&0xFF;
        }
        if(r[k]->angle!=a[k]&&!Platform) /* robot rotated */
        {
           i1 = r[k]->angle - a[k];
           if(i1!=1&&i1!=-1)
           {
              if(i1<0) i1=1;
              else i1=-1;
           }
           command[ncom] = 0x200 + k;
           comx[ncom++] = i1;
        }
        if((i!=RobotGetX(r[k])||j!=RobotGetY(r[k]))&&!Platform) /* robot moved */
        {
           command[ncom] = 0x300 + k;
           comx[ncom] = i;
           comy[ncom++] = j;
        }
     }

     if(f_g==0&&f_d>=2) fclose(fout);

     ibreak = 0;
  }

  // end of if(substep==0)

     #define R1(i) ((i)/(double)NPOV)
     #define R2(i) ((NPOV-(i))/(double)NPOV)
     #define RR(i1,i2,j) ((i1)*R2(j)+(i2)*R1(j))

     if(substep==0 && f_f==100)
     {
        old_f_f = f_f;
        f_f = 300;
     }

     // commands processing
     //for(j=0;j<=NPOV;j++)
     j = substep;
     //{
        if(ncom==0) goto break1;
        ncom2 = ncom;
        for(i=0;i<ncom2;i++)
        {
        //   WriteShellInt(command[i]);
           switch(command[i]>>8)
           {
             case 0x00:
                  break;
             case 0x01: // explode
                  nn = command[i]&0xFF;
                  switch(nn)
                  {
                    case 1: Fire1(comx[i],comy[i],4-comk[i],comn[i]); break;
                    case 2: Fire2(comx[i],comy[i],4-comk[i],comn[i]); break;
                  }
                  Sound(FIREF-j,FIRES*nn);
                  if(comk[i]--==0)
                  {
                     command[i]=0;
                  }
                  break;
             case 0x02: // rotate
                  k = command[i]&0xFF;
                  if(r[k]->E<=0) break;
                  if(comx[i]>0) LeftPov(k,j);
                  else RightPov(k,j);
                  Sound(1,DELA/ncom2);
                  break;
             case 0x03: // move
                  k = command[i]&0xFF;
                  if(r[k]->E<=0) break;
                  i1 = comx[i]<<SBIT;
                  j1 = comy[i]<<SBIT;
                  i2 = (RobotGetX(r[k]))<<SBIT;
                  j2 = (RobotGetY(r[k]))<<SBIT;
                  //TraceWait();
                  if(f_f<200)
                  {
                     //tempty.Draw(i1,j1);
                     //tempty.Draw(i2,j2);
                     WriteTile(i1,j1,TILE_EMPTY,RW1SIZE);
                     WriteTile(i2,j2,TILE_EMPTY,RW1SIZE);
                  }
                  ix = RR(i1,i2,j);
                  iy = RR(j1,j2,j);
                  switch(rstep[k])
                  {
                    case 0:
                         htrace(i1,j1,i1+2);
                         if(a[k]==0) htrace(i1+6,j1,i1+22);
                         break;
                    case 1:
                         vtrace(i1,j1+26,j1+30);
                         if(a[k]==1) vtrace(i1,j1+6,j1+22);
                         break;
                    case 2:
                         htrace(i1+26,j1,i1+30);
                         if(a[k]==2) htrace(i1+6,j1,i1+22);
                         break;
                    case 3:
                         vtrace(i1,j1,j1+2);
                         if(a[k]==3) vtrace(i1,j1+6,j1+22);
                         break;
                  }
                  switch(a[k])
                  {
                    case 0: htrace(i1+26,j1,ix+6); break;
                    case 1: vtrace(i1,iy+26,j1); break;
                    case 2: htrace(ix+26,j1,i1); break;
                    case 3: vtrace(i1,j1+26,iy+6); break;
                  }
                  if(f_f<200)
                  {
                     TextureDraw(&trobot[k],ix,iy);
                     DrawSelect(ix,iy,k);
                  }
                  if(j==NPOV)
                  {
                     rstep[k]=a[k];
                  }
                  Sound(20+k*10,DELA/ncom2);
                  break;
             case 0x11: // shot
                  if(comd[i]) break;
                  k = command[i]&0xFF;
                  comt[i]++;
                  i2 = comx[i];
                  j2 = comy[i];
                  if(i2==RobotGetX(r[k])&&j2==RobotGetY(r[k]))
                  {
                     rwstat[k].all_shoot++;
                     RShow(k);
                  }
                  switch(comk[i])
                  {
                     case 0:
                          if(++comx[i]>=DDX) comd[i]=1;
                          else if(f_f<200)
                           WriteTile((comx[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     (comy[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     TILE_BALL,MISSIZE);
                          //tmissle0.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                     case 1:
                          if(--comy[i]<0) comd[i]=1;
                          else if(f_f<200)
                           WriteTile((comx[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     (comy[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     TILE_BALL,MISSIZE);
                          //tmissle1.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                     case 2:
                          if(--comx[i]<0) comd[i]=1;
                          else if(f_f<200)
                           WriteTile((comx[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     (comy[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     TILE_BALL,MISSIZE);
                          //tmissle2.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                     case 3:
                          if(++comy[i]>=DDY) comd[i]=1;
                          else if(f_f<200)
                           WriteTile((comx[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     (comy[i]<<SBIT)+RW1SIZE2-MISSIZE2,
                                     TILE_BALL,MISSIZE);
                          //tmissle3.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                  }
                  if(comd[i]) comn[i]=10;
                  else
                  {
                     comn[i]=MAPLO_(comx[i],comy[i])>>4;
                  }
                  if((MAPLO_(i2,j2)>>4)==5)
                  {
                     MAP_(i2,j2,0);
                     if(f_f<200) //tempty.Draw(i2<<SBIT,j2<<SBIT,5);
                        WriteTile(i2<<SBIT,j2<<SBIT,TILE_EMPTY,RW1SIZE);
                  }
                  if(comd[i]) break;
                  i1 = comx[i]<<SBIT;
                  j1 = comy[i]<<SBIT;
                  switch(comn[i])
                  {
                     case 0: MAP_(comx[i],comy[i],0x50|comk[i]); // 05.11.2002 !!!
                             break;
                     case 1: if(f_f<200) //thole.Draw(i1,j1);
                                WriteTile(i1,j1,TILE_HOLE,RW1SIZE);
                             break;
                     case 2: k2 = MAPLO_(comx[i],comy[i])&0x0F;
                             if(f_f<200) //tfire.Draw(i1,j1);
                                WriteTile(i1,j1,TILE_FIRE,RW1SIZE);
                             Sound(FIREF,FIRES); //SS

                             if(--k2>0)
                             {  if(f_f<200)
                                {  //tstone.Draw(i1,j1);
                                   WriteTile(i1,j1,TILE_STONE1+k2-1,RW1SIZE);
                                   //DrawChar(i1+4,j1+4,k2+'0',192,210); !!!
                                }
                                MAP_(comx[i],comy[i],0x20+k2);
                             }
                             else
                             {  if(f_f<200) //tempty.Draw(i1,j1);
                                   WriteTile(i1,j1,TILE_EMPTY,RW1SIZE);
                                MAP_(comx[i],comy[i],0);
                             }
                             comd[i] = 1;
                             break;
                     case 3: command[ncom]=0x101;
                             comk[ncom] = NPOV;
                             comn[ncom] = k;
                             comd[ncom] = 0;
                             comx[ncom] = comx[i];
                             comy[ncom++] = comy[i];
                             Sound(FIREF,FIRES);
                             comd[i] = 1;
                             break;
                     case 4: command[ncom]=0x102;
                             comk[ncom] = NPOV;
                             comn[ncom] = k;
                             comd[ncom] = 0;
                             comx[ncom] = comx[i];
                             comy[ncom++] = comy[i];
                             Sound(FIREF,FIRES);
                             comd[i] = 1;
                             break;
                     case 5: for(nn=0;nn<ncom2;nn++)
                             {
                               if(  nn != i &&
                                   (command[nn]>>8)==0x11 &&
                                    comx[nn]==comx[i] &&
                                    comy[nn]==comy[i] )
                               {
                                  comd[nn] = 1;
                                  comn[nn] = 5;
                               }
                             }
                             if(f_f<200)
                             {  //tfire.Draw(i1,j1); !!!
                                WriteTile(i1,j1,TILE_FIRE,RW1SIZE);
                                Sound(FIREF,FIRES); //SS
                                //tempty.Draw(i1,j1);
                                WriteTile(i1,j1,TILE_EMPTY,RW1SIZE);
                             }
                             MAP_(comx[i],comy[i],0);
                             comd[i] = 1;
                             break;
                     case 6: for(nn=0;nn<NUM;nn++)
                             {
                              if(RobotGetX(r[nn])==comx[i]&&RobotGetY(r[nn])==comy[i])
                              {
                                 RoboDead(nn,1);
                                 rwstat[k].good_shoot++;
                                 if(r[nn]->E<=0)
                                 {  rwstat[k].kill_shoot++;
                                    rwstat[nn].reason = 2;
                                 }
                                 rwstat[nn].damages++;
                                 break;
                              }
                             }
                             if(f_f<200) //tfire.Draw(i1,j1);
                             WriteTile(i1,j1,TILE_FIRE,RW1SIZE);
                             Sound(FIREF,FIRES);
                             if(nn<NUM) RShow(nn);
                             else WriteTile(i1,j1,TILE_EMPTY,RW1SIZE);
                             comd[i] = 1;
                             break;
                     default:
                             //if(f_f!=2) CloseGraph();
                             sprintf(str,"Fatal Error: 1001\n\n"
                                         "comn=%i x=%i y=%i\n",
                                         comn[i],comx[i],comy[i]);
                             WriteFatal(str);
                             exit(1);
                             break;
                  }
                  Sound(1000+100*k-comt[i]*10,DELA/ncom2);
                  break;
             case 0x12: // return energy of the robot (K)
                  k = command[i]&0xFF;
                  /*
                  if(f_f<200)
                  {
                     i1 = comx[i]<<SBIT;
                     j1 = comy[i]<<SBIT;
                     switch(comk[i])
                     {
                       case 0:
                         PutScreenPixel(i1+13,j1+7,15);
                         PutScreenPixel(i1+13,j1+8,15);
                         break;
                       case 1:
                         PutScreenPixel(i1+7,j1+2,15);
                         PutScreenPixel(i1+8,j1+2,15);
                         break;
                       case 2:
                         PutScreenPixel(i1+2,j1+7,15);
                         PutScreenPixel(i1+2,j1+8,15);
                         break;
                       case 3:
                         PutScreenPixel(i1+7,j1+13,15);
                         PutScreenPixel(i1+8,j1+13,15);
                         break;
                     }
                  }
                  */
                  command[i] = 0;
                  break;
             case 0x13: // return iformation about last missle (X,Y,N)
                  k = command[i]&0xFF;
                  r[k]->N = ms[k].n;
                  i2 = ms[k].x;
                  i3 = ms[k].y;
                  if(f_gr && r[k]->N==6)
                  {
                     for(k2=0;k2<NUM;k2++)
                     {
                         if( RobotGetX(r[k])==i2 &&
                             RobotGetY(r[k])==i3 &&
                             !strcmp(RobotGetAuthor(r[k]),
                                     RobotGetAuthor(r[k2]))
                           ) r[k]->N = 7;
                     }
                  }
                  k2 = r[k]->angle;
                  if(k2<0) k2+=4;
                  if(k2>3) k2-=4;
                  switch(k2)
                  {
                    case 0: r[k]->X=i3-RobotGetY(r[k]);
                            r[k]->Y=i2-RobotGetX(r[k]);
                            break;
                    case 1: r[k]->X=i2-RobotGetX(r[k]);
                            r[k]->Y=RobotGetY(r[k])-i3;
                            break;
                    case 2: r[k]->X=RobotGetY(r[k])-i3;
                            r[k]->Y=RobotGetX(r[k])-i2;
                            break;
                    case 3: r[k]->X=RobotGetX(r[k])-i2;
                            r[k]->Y=i3-RobotGetY(r[k]);
                            break;
                  }
                  command[i] = 0;
                  break;
             case 0x14: // robot step to the hole
                  k = command[i]&0xFF;
                  i2 = RobotGetX(r[k]);
                  i3 = RobotGetY(r[k]);
                  MAP_(i2,i3,0);//1<<4;
                  i2 <<= SBIT;
                  i3 <<= SBIT;
                  if(f_f<200) //tempty.Draw(i2,i3);
                     WriteTile(i2,i3,TILE_EMPTY,RW1SIZE);
                  r[k]->E = 0;
                  r[k]->N = 0;
                  rwstat[k].reason = 1;
                  sprintf(str,"Robot %u has gone into the HOLE !",k+1);
                  Message1(str);
                  command[i] = 0;
                  break;
             case 0x15: // send?
                  k = command[i]&0xFF;
             //     if(f_gr)
                  {
                   if(comk[i]==0)
                   {
                      for(i1=0;i1<NUM;i1++)
                      {
                             if(i1==k) continue;
                             RobotSend(r[i1],r[k]->send,k+1,
                                           r[k]->T,comx[i],comy[i]);
                             if(f_g==0&&f_d>0)
                             {
                                fprintf(fout,"T=%i\tROBOT%u '%s'\tSEND %i %i (%i,%i)\n",
                                  r[k]->T,k+1,RobotGetName(r[k]),
                                  r[k]->send,i1+1,
                                  comx[i],comy[i]);
                             }
                      }
                   }
                   if(comk[i]>0&&comk[i]<=NUM&&comk[i]!=k+1)
                   {
                      RobotSend(r[comk[i]-1],r[k]->send,k+1,
                                           r[k]->T,comx[i],comy[i]);
                             if(f_g==0&&f_d>0)
                             {
                                fprintf(fout,"T=%i\tROBOT%u '%s'\tSEND %i %i (%i,%i)\n",
                                  r[k]->T,k+1,RobotGetName(r[k]),
                                  r[k]->send,comk[i],
                                  comx[i],comy[i]);
                             }
                   }
                  }
                  command[i] = 0;
                  break;
             case 0x16: // set
                  k = command[i]&0xFF;
                  if(f_f<200 && Platform==2)
                     WriteTile(comx[i]<<SBIT,comy[i]<<SBIT,-comi[i],RW1SIZE);
                  command[i] = 0;
                  break;
           }
        }
//        delayf(40);

//        if(f_g==0&&f_d==3)
//        {
//           if(getch()==27)
//           {  ibreak = 1;
//              break;
//           }
//        }

     //}

  break1:

  if(substep==NPOV)
  {
     if(ibreak){ok=0;goto break2;} // ???

//     if(ncom==0) delayf(DELA); // !!!

//     commands("OLD");

     k = 0;
     for(i=0;i<ncom;i++)
     {
        j = command[i]>>8;
        if(j==0x11||j==0x01)
        {
           i1 = command[i]&0xFF;
           command[k] = command[i];
           if(j==0x11)
           {
             ms[i1].n = comn[i];
             ms[i1].x = comx[i];
             ms[i1].y = comy[i];
           }
           comn[k] = comn[i];
           comx[k] = comx[i];
           comy[k] = comy[i];
           comk[k] = comk[i];
           comt[k] = comt[i];
           comd[k] = comd[i];
           if(comd[i]==0) k++;
        }
     }
     ncom = k;

//     commands("NEW");

     if(ibre>1000)
     {
       mmi = 0;
       if(f_gr)
       {
        for(k=0;k<maxgr;k++)
        {
           if(gr[k].num>0)
           {
              mmi++;
              edrobo = k;
           }
        }
       }
       else
       {
        for(k=0;k<NUM;k++)
        {
           if(r[k]->E>0)
           {
              mmi++;
              edrobo = k;
           }
        }
       }
       if(mmi<=1) ibre=1;
     }
     else
     {
        ibre--;
        if(ibre<=0&&f_i) ok=0;
     }
  }

  if(substep==0 && old_f_f>0)
  {
        f_f = old_f_f;
        old_f_f = -1;
  }

  if(++substep==NPOV+1) substep=0;

  if(ok) { JobStep = 0; return ok;}  //???

 break2:


//  if(f_f<200) CloseGraph();

  if(f_gr)
  {
    if(mmi==1)
    {
       sprintf(str,"\n>>>%s WINS !\n\n",gr[edrobo].author);
       WriteShell(str); // ???
       if(fresult!=NULL) fputs(str,fresult);
    }
  }
  else
  {
    // mmi - number of alive robots
    // edrobo - identifier of alive robot
    if(mmi==1) //&&rwstat[edrobo].kill_shoot>0)
    {
       wins = edrobo+1;
       sprintf(str,"\n> ROBOT %u '%s' WINS !\n\n",wins,RobotGetName(r[edrobo]));
       WriteShell(str); // ???
       if(fresult!=NULL) fputs(str,fresult);
    }
  }

  for(k=0;k<NUM;k++)
  {
    if(f_i)
    {
     strcpy(str,"\n");
     sprintf(st1,"> robot %u\n",k+1);
     strcat(str,st1);
     sprintf(st1,"> name='%s'\n",RobotGetName(r[k]));
     strcat(str,st1);
     sprintf(st1,"> version=%u\n",RobotGetVersion(r[k]));
     strcat(str,st1);
     sprintf(st1,"> lcod=%u\n",RobotGetLCod(r[k]));
     strcat(str,st1);
     sprintf(st1,"> lvar=%u\n",RobotGetLVar(r[k]));
     strcat(str,st1);
     sprintf(st1,"> color=#%2.2X%2.2X%2.2X\n",
               RobotGetR(r[k]),RobotGetG(r[k]),RobotGetB(r[k]));
     strcat(str,st1);
     sprintf(st1,"> equip=%u%u%u%u\n",
               RobotGetE(r[k],0),RobotGetE(r[k],1),RobotGetE(r[k],2),RobotGetE(r[k],3));
     strcat(str,st1);
     sprintf(st1,"> time=%u\n",rwstat[k].time);
     strcat(str,st1);
     sprintf(st1,"> all_shoot=%u\n",rwstat[k].all_shoot);
     strcat(str,st1);
     sprintf(st1,"> good_shoot=%u\n",rwstat[k].good_shoot);
     strcat(str,st1);
     sprintf(st1,"> kill_shoot=%u\n",rwstat[k].kill_shoot);
     strcat(str,st1);
     sprintf(st1,"> min_missle=%u\n",rwstat[k].min_missle);
     strcat(str,st1);
     sprintf(st1,"> max_missle=%u\n",rwstat[k].max_missle);
     strcat(str,st1);
     sprintf(st1,"> fin_missle=%u\n",rwstat[k].fin_missle);
     strcat(str,st1);
     sprintf(st1,"> min_energy=%u\n",rwstat[k].min_energy);
     strcat(str,st1);
     sprintf(st1,"> max_energy=%u\n",rwstat[k].max_energy);
     strcat(str,st1);
     sprintf(st1,"> fin_energy=%u\n",rwstat[k].fin_energy);
     strcat(str,st1);
     sprintf(st1,"> boxes=%u\n",rwstat[k].boxes);
     strcat(str,st1);
     sprintf(st1,"> damages=%u\n",rwstat[k].damages);
     strcat(str,st1);
     sprintf(st1,"> reason=%u\n",rwstat[k].reason);
     strcat(str,st1);
     sprintf(st1,"\n");
     strcat(str,st1);
     WriteShell(str); // ???
     if(fresult!=NULL) fputs(str,fresult);
    }
    if(r[k]!=NULL) RobotDel(r[k]);
  }

  EndFight = 1;

  if(fresult!=NULL)
  {
     fclose(fresult);
     rename(FILEITMP,FILEIOUT);
     fresult = NULL;
  }

  if(f_d==1) fclose(fout);

//!   SleepStep = 1;
//!   InvalidateRect(hMainWnd,NULL,0);
//! }

//  if(f_s) SoundExit();

 for(i=0;i<MNUM;i++) if(trobot[i].ifree) free(trobot[i].image);

 JobStep = 0;
 return ok;
}

void DebugMAP(int x,int y,int z)
{
  FILE *fo;
  map[(DDX*(y))+(x)]=z;
  fo = fopen("map.log","at");
  fprintf(fo,"[%i:%i] %2.2X",x,y,z);
  if((z&0xFF)>=0x70)
  {
	  fprintf(fo," error\n");
  }
  fputc('\n',fo);
  fclose(fo);
}
