/* my_text.h - Alexander A. Shabarshin <ashabarshin@gmail.com>  */

#ifndef __MY_TEXT_H
#define __MY_TEXT_H

#ifdef __QUICKDRAW__ /* Hack to make it compilable in Xcode */
#define Line MyLine
#define Text MyText
#endif

#define MY_MAXLEN 1024 /* Limit for line length */

/* Enumeration for Text errors */

typedef enum myTextErr 
{ 
  TextErrMem = 1, /* Out of Memory */
  TextErrIdx = 2, /* Index Error */
  TextErrFil = 3, /* File Error */
  TextErrFld = 4  /* Wrong Field */
} TextErr;

/* Enumeration for Text fields to sort and to search */

typedef enum myTextFld
{
  TextFldNul, /* do not search */
  TextFldStr, /* line->str */
  TextFldTyp, /* line->type */
  TextFldId1, /* line->id */
  TextFldId2, /* line->id2 */
  TextFldAdr, /* line->adr */
  TextFldLen  /* line->len */
} TextFld;

/* Structure for Line of text representation */

typedef struct myLine
{
  void *next;           /* pointer to next line */
  char *str;            /* pointer to string */
  /* below fields may be used for your purposes: */
  int type;             /* type of the string */
  int id;               /* first identifier of the string */
  int id2;              /* second identifier of the string */
  unsigned short adr;   /* 16-bit word for address */
  unsigned short len;   /* 16-bit word for length */
} Line;

/* Structure for Text representation */

typedef struct myText
{
  long num;             /* number of lines */
  Line *first;          /* first line of text */ 
  Line *last;           /* last line of text */ 
  /* below fields are used for searching: */
  Line *find;           /* last found line */
  TextFld find_fld;     /* search by what field */  
  TextFld find_fld2;    /* search by what second field */  
  char *find_str;       /* searching string */ 
  int find_type;        /* searching type */
  int find_id;          /* searching identifier */ 
  int find_id2;         /* searching additional identifier */
  unsigned short find_adr; /* searching adr field */
  unsigned short find_len; /* searching len field */
} Text;

#ifdef __cplusplus
extern "C" {
#endif

/* Functions for Line object: */

Line* LineNew(char *s);
void LineDel(Line *l);
char* LineGetStr(Line *l);
void LineSetTyp(Line *l, int t);
int LineGetTyp(Line *l);
void LineSetId1(Line *l, int i);
int LineGetId1(Line *l);
void LineSetId2(Line *l, int i);
int LineGetId2(Line *l);
void LineSetAdr(Line *l, unsigned short u);
unsigned short LineGetAdr(Line *l);
void LineSetLen(Line *l, unsigned short u);
unsigned short LineGetLen(Line *l);

/* Functions for Text object: */

Text* TextNew(void);
void TextDel(Text *t);
void TextError(TextErr err);
long TextGetNumber(Text *t);
Line* TextAdd(Text *t, char *s);
Line* TextInsert(Text *t, long n, char *s);
void TextDelete(Text *t, long n);
void TextAllDelete(Text *t);
Line* TextGet(Text *t, long n);
long TextIndex(Text *t, Line *l);
int TextReplace(Text *t, Line *l1);
int TextSort(Text *t, TextFld field);
void TextSave(Text *t, char *filename);
void TextLoad(Text *t, char *filename);
void TextList(Text *t);
Line* TextFindFirstStr(Text *t, char *s);
Line* TextFindFirstTyp(Text *t, int y);
Line* TextFindFirstId1(Text *t, int i);
Line* TextFindFirstId2(Text *t, int i);
Line* TextFindFirstAdr(Text *t, unsigned short u);
Line* TextFindFirstLen(Text *t, unsigned short u);
Line* TextFindFirstStrTyp(Text *t, char *s, int y);
Line* TextFindFirstTypId(Text *t, int y, int i);
Line* TextFindNext(Text *t);

/* Other string manipulation functions */

int strwcmp(char *str, char *wld);
int strdels(char *s);
char* strskips(char* p);
int hexx(char c);
int hex2i(char *s);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif


