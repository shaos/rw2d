/*
 Robot Warfare 2D
 ================

 Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/* robotwar.c history:

   10 Nov 1998 - Start working on C++ version
   19 Jan 2013 - Rewritten as pure C program
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "robotwar.h"
#include "my_text.h"

#if 1
#define DEBUG
#endif

#if 0
#define RDUMP
#endif

#define MAXVAL  32767L
#define MINVAL -32768L

#define EXPR_ERR_STACK   -10
#define EXPR_ERR_OVERST  -11
#define EXPR_ERR_NONST   -12
#define EXPR_ERR_DIVZERO -20
#define EXPR_ERR_OVERFL  -21
#define EXPR_ERR_INDEX   -22

#define DelSpaces strdels
#define SkipSpace strskips

int DDX = 20;
int DDY = 10;

void StrUp(char *str)
{
  int i,f=1;
  for(i=0;i<(int)strlen(str);i++)
  {
     if(str[i]=='"')
     {
        if(f) f=0;
        else  f=1;
     }
     if(f) str[i]=toupper(str[i]);
  }
}

int StartWith(char *str,char *sw,short f)
{
   char *p1,*p2;
   p1 = sw;
   p2 = str;
   while(*p1!=0)
   {
     if(*p2==0) return 0;
     if(*p1!=*p2) return 0;
     p1++;
     p2++;
   }
   if(f&&(*p2!=' '&&*p2!='\t'&&*p2!='\n'&&*p2!=0)) return 0;
   return 1;
}

void Error(int e,char *s,short i)
{
  char str[100];
  switch (e)
  {
    case 0 : strcpy(str,"Out of memory");break;
    case 1 : strcpy(str,"Can't open file");break;
    case 2 : strcpy(str,"Illegal extention");break;
    case 3 : strcpy(str,"Too big name");break;
    case 4 : strcpy(str,"Syntax error");break;
    case 5 : strcpy(str,"Quotation is expected");break;
    case 6 : strcpy(str,"Duplicate definition");break;
    case 7 : strcpy(str,"Definition after START:");break;
    case 8 : strcpy(str,"Command before START:");break;
    case 9 : strcpy(str,"Too many vars");break;
    case 10: strcpy(str,"Unknown name");break;
    case 11: strcpy(str,"Out of index");break;
    case 12: strcpy(str,"Register is read only");break;
    case 13: strcpy(str,"Command after END");break;
    case 14: strcpy(str,"END is expected");break;
    case 15: strcpy(str,"Bad file format");break;
    case 16: strcpy(str,"Too many lines");break;
    case 17: strcpy(str,"Unknown label");break;
    default: strcpy(str,"Unknown error");break;
  }
  printf("\n");
  if(i>0) printf("(%u) ",i);
  printf("ERROR %i: %s !",e,str);
  if(s!=NULL) printf("\n\t%s",s);
  printf("\n\n");
  exit(1);
}

void Error2(int e,char *s)
{
  Error(e,s,0); 
}

void Error1(int e)
{
  Error(e,NULL,0); 
}


int _ElemSize = 1;

/*#########################################################*/

typedef struct tagElem
{
  char v[16];
  long i;
  short c;
} Elem;

typedef struct tagStackE
{
  int u;
  int n;
  Elem *p;
} StackE;

StackE* StackE_New(int n)
{
   StackE *s;
   s = (StackE*)malloc(sizeof(StackE));
   s->p = (Elem*)malloc(n*sizeof(Elem));
   s->u = 0;
   return s;
}

void StackE_Del(StackE *s)
{
   free(s->p);
   free(s);
}

int StackE_Push(StackE *s, Elem* b)
{
   s->p[s->u++] = *b;
   if(s->u==s->n) return 0;
   return 1;
}

Elem StackE_Pop(StackE *s)
{  Elem ee,o;
   ee.c = -1;
   o = s->p[--s->u];
   if(s->u<0) return ee;
   return o;
}

int StackE_Get(StackE *s)
{  return s->u;
}

Elem StackE_GetN(StackE *s, int i) /* 1,2,3 */
{  Elem ee,o;
   ee.c = -1;
   o = s->p[s->u-i];
   if(s->u-i<0) return ee;
   return o;
}

void StackE_List(StackE *s)
{
   int i;
   printf("\n\n");
   printf("STACK LIST\n");
   printf("==========\n");
   for(i=s->u-1;i>=0;i--)
       printf("%i) %i 0x%2.2X\n",s->u-i,s->p[i].c,(int)s->p[i].i);
   printf("==========\n\n");
}

/*#######################################################*/

int Oper(char *s, char **ss)
{
  int op = 0;
  int dd = 0;
  switch(s[0])
  {
     case '=': if(s[1]=='=')
               {op=0x90;dd=2;}
               else
               {op=0x60;dd=1;}
               break;
     case '&': if(s[1]=='&')
               {op=0x80;dd=2;}
               else
               {op=0xC0;dd=1;}
               break;
     case '|': if(s[1]=='|')
               {op=0x81;dd=2;}
               else
               {op=0xC1;dd=1;}
               break;
     case '!': if(s[1]=='=')
               {op=0x91;dd=2;}
               break;
     case '>': if(s[1]=='=')
               {op=0x94;dd=2;}
               else
               { if(s[1]=='>')
                 {op=0xD0;dd=2;}
                 else
                 {op=0x92;dd=1;}
               }
               break;
     case '<': if(s[1]=='=')
               {op=0x95;dd=2;}
               else
               { if(s[1]=='<')
                 {op=0xD1;dd=2;}
                 else
                 {  if(s[1]=='>')
                    {op=0x91;dd=2;}
                    else
                    {op=0x93;dd=1;}
                 }
               }
               break;
     case '+': op=0xA0;dd=1;break;
     case '-': op=0xA1;dd=1;break;
     case '*': op=0xB0;dd=1;break;
     case '/': op=0xB1;dd=1;break;
     case '%': op=0xB2;dd=1;break;
     case '^': op=0xC2;dd=1;break;
     case '?': op=0xFF;dd=1;break;
     case ':': op=0xF0;dd=1;break;
  }
  *ss = &s[dd];
  return op;
}

int OperU(char *s, char **ss)
{
  int op = 0;
  int dd = 0;
  switch(s[0])
  {
     case '+': op=0xEF;dd=1;break;
     case '-': op=0xE0;dd=1;break;
     case '~': op=0xE1;dd=1;break;
     case '!': op=0xE2;dd=1;break;
  }
  *ss = &s[dd];
  return op;
}

int Name(char *str, char **tail, char *name)
{
  int i,j,f;
  *tail = str;
  f = 1;
  i = 0;
  if(!isalpha(str[i])&&str[i]!='_'&&str[i]!='$'&&str[i]!='@') return 0;
  while(1)
  {  j = 0;
     if( (!f&&isdigit(str[i])) ||
         isalpha(str[i]) ||
         str[i]=='_' ||
         str[i]=='$' ||
         str[i]=='@'
       ) j=1;
     if(!j) break;
     name[i] = str[i];
     if(f==1) f=0;
     i++;
  }
  name[i] = 0;
  *tail = &str[i];
#ifdef DEBUG
   printf(">>> NAME '%s' (%s)\n",name,*tail);
#endif
  return 1;
}

int Numb(char *str, char **tail, int *number)
{
  int i,j,k,m,i_i,sgn=1;
  char s[10];
  *number = 0;
  *tail = str;
  if(str[0]=='\'') /* Character */
  {
     if(str[1]=='\\')
     {
        if(str[3]!='\'') return 0;
        if(str[2]=='n') *number=10;
        if(str[2]=='r') *number=13;
        if(str[2]=='t') *number=10;
        *tail = &str[4];
     }
     else
     {
        if(str[2]!='\'') return 0;
        *number = str[1];
        *tail = &str[3];
     }
     return 1;
  }
  if((str[0]=='0'&&(str[1]=='x'||str[1]=='X'))||str[0]=='#') /* Hexadecimal */
  {
     if(str[0]=='#')
          i = 1;
     else i = 2;
     i_i = i;
     while(1)
     {
        if( !(str[i]>='0'&&str[i]<='9') &&
            !(str[i]>='A'&&str[i]<='F') &&
            !(str[i]>='a'&&str[i]<='f') ) break;
        i++;
     }
     j = i;
     k = 1;
     *number = 0;
     for(i=j-1;i>=i_i;i--)
     {
       m = str[i];
       switch(m)
       {
         case 'a':
         case 'A':
              m = 10;
              break;
         case 'b':
         case 'B':
              m = 11;
              break;
         case 'c':
         case 'C':
              m = 12;
              break;
         case 'd':
         case 'D':
              m = 13;
              break;
         case 'e':
         case 'E':
              m = 14;
              break;
         case 'f':
         case 'F':
              m = 15;
              break;
      }
      if(m>=48&&m<58) m-=48;
      if(m<0||m>15) return 0;
      *number = *number + k*m;
      k *= 16;
     }
     *tail = &str[j];
#ifdef DEBUG
      printf(">>> HEX %4.4X | %s |\n",*number,*tail);
#endif
     return 1;
  }
  j = 0;
  if(str[0]=='-'){j++;sgn=-1;}
  if(str[0]=='+'){j++;sgn=1;}
  if(isdigit(str[j])) /* Integer */
  {  i = j;
     while(1)
     {
       if(!isdigit(str[i])) break;
       i++;
     }
     k = i-j;
     memcpy(s,&str[j],k);
     s[k] = 0;
     *number = sgn*atoi(s);
     *tail = &str[i];
#ifdef DEBUG
      printf(">>> INT %s=%i | %s |\n",s,*number,*tail);
#endif
     return 1;
  }
  return 0;
}

int expression(char *str,Text *t,unsigned char *mem,int without,int pass)
{
  char *po;
  char *tail = str;
  char s[100];
  int j,k,num;
  int lastth = 0;
  int posle = 0;
  Line *lin;
  Elem e;
  StackE *stack;
  int count = 10000;
  if(without) posle=1;
  stack = StackE_New(1000);
  j = 0;
  if(without)
  {
    mem[j++] = 0xF5; /* PUSH */
    mem[j++] = 0x0E; /* reg L */
    mem[j++] = 0xFF;
    e.c = 1;
    e.i = 0x60;
    StackE_Push(stack,&e);
  }
  while(1)
  {
    #ifdef DEBUG
     printf("|%s|\n",tail);
    #endif
    if(--count<0) return -3;
    po = tail;
    if(lastth)
    {
       if(*po=='['&&lastth==1) /* Array */
       {
#ifdef DEBUG
           printf("===> ARRAY\n");
#endif
          e.c = 1;
          e.i = 4;
          if(!StackE_Push(stack,&e)) return -4;
          tail++;
          lastth=0;
          continue;
       }
       if(*po==']')
       {
#ifdef DEBUG
           printf("===> ]\n");
#endif
          e = StackE_Pop(stack);
          while(e.i!=4)
          {
             mem[j++] = e.i;
             if(StackE_Get(stack)==0) return -6;
             e = StackE_Pop(stack);
          }
          mem[j++] = 0xF6; /* [] ??? */
          if(StackE_Get(stack)>0 || posle)
          {
             mem[j++] = 0xF3; /* LOAD */
          }
          lastth = 3;
          tail++;
          continue;
       }
       if(*po==')')
       {
#ifdef DEBUG
           printf("===> )\n");
#endif
          e = StackE_Pop(stack);
          while(e.i!=2)
          {
             mem[j++] = e.i;
             if(StackE_Get(stack)==0) return -6;
             e = StackE_Pop(stack);
          }
          lastth = 3;
          tail++;
          continue;
       }
       /* Binary Operator */
       if(*tail!=0)
       {
          k = Oper(po,&tail);
#ifdef DEBUG
           printf("Oper(%s,%s)=0x%2.2X\n",po,tail,k);
#endif
          if(k==0) return -1;
       }
       e = StackE_GetN(stack,1);
       if(e.c!=-1&&((e.i&0xF0)>=(k&0xF0)||*tail==0))
       {
          e = StackE_Pop(stack);
          if(e.i==0x60) mem[j++]=0xF4; /* SAVE */
          else mem[j++]=e.i;
       }
       if(*tail!=0)
       {
          e.c = 1;
          e.i = k;
          if(k==0x60) posle=1;
          if(k!=0xFF && !StackE_Push(stack,&e)) return -4;
       }
       if(*tail==0&&!StackE_Get(stack)) break;
       if(*tail!=0) lastth=0;
       continue;
    }
    /* Unary Operator */
    k = OperU(po,&tail);
    if(k>0)
    {
#ifdef DEBUG
        printf("OperU(%s,%s)=0x%2.2X\n",po,tail,k);
#endif
       e.c = 1;
       e.i = k;
       if(k<0xEF&&!StackE_Push(stack,&e)) return -4;
       continue;
    }
    if(Name(po,&tail,s))
    {
       for(lin=t->first;lin!=NULL;lin=lin->next)
       {
          if(lin->type > 0) /* !!! */
          {
             if(!strcmp(lin->str,s)) break;
          }
       }
       if(lin==NULL)
       {
          if(pass) return -2;
          lin = TextAdd(t,s);
          lin->adr = 0;
          lin->type = 11;
       }
       mem[j++] = 0xF5; /* PUSH */
       mem[j++] = (lin->adr)%256;
       mem[j++] = (lin->adr)/256;
       if((StackE_Get(stack)>0 || posle) && lin->type<10 && *tail!='[')
       {
          mem[j++] = 0xF3; /* LOAD */
       }
       lastth = 1;
       if(lin->type>=10) lastth = 2;
       continue;
    }
    if(Numb(po,&tail,&num))
    {
       mem[j++] = 0xF5; /* PUSH */
       mem[j++] = num%256;
       mem[j++] = num/256;
       lastth = 2;
       continue;
    }
    if(*po=='(')
    {
#ifdef DEBUG
        printf("===> (\n");
#endif
       e.c = 1;
       e.i = 2;
       if(!StackE_Push(stack,&e)) return -4;
       lastth = 0;
       tail++;
       continue;
    }
    return -5;
  }
  StackE_Del(stack);
  return j;
}

int EvalStep(StackE *stack, int *i,
             unsigned char *mem, int memlen,
             short *vars, int varslen, short *regs,
             short *value)
{
     int k,ok = 1;
     short *psh;
     unsigned short ush;
     Elem a,b,c,e;
     switch(mem[*i])
     {
        case 0x80: /* &&  A,B -> A&&B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i && b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x81: /* ||  A,B -> A||B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i || b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x90: /* ==  A,B -> A==B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i == b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x91: /* !=  A,B -> A!=B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i != b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x92: /* >   A,B -> A>B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i > b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x93: /* <   A,B -> A<B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i < b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x94: /* >=  A,B -> A>=B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i >= b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0x95: /* <=  A,B -> A<=B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i <= b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xA0: /* +   A,B -> A+B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i + b.i);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xA1: /* -   A,B -> A-B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i - b.i);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xB0: /* *   A,B -> A*B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i * b.i);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xB1: /* /   A,B -> A/B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             if(b.i==0) return EXPR_ERR_DIVZERO;
             *value = e.i = (a.i / b.i);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xB2: /* %   A,B -> A%B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             if(b.i==0) return EXPR_ERR_DIVZERO;
             *value = e.i = (a.i % b.i);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xC0: /* &   A,B -> A&B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i & b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xC1: /* |   A,B -> A|B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i | b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xC2: /* ^   A,B -> A^B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = (a.i ^ b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xD0: /* >>  A,B -> A>>B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             ush = a.i;
             *value = e.i = (ush >> b.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xD1: /* <<  A,B -> A<<B */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             ush = a.i;
             *value = e.i = (ush << b.i);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xE0: /* -   A -> -A */
             if(StackE_Get(stack)<1) return EXPR_ERR_STACK;
             a = StackE_Pop(stack);
             *value = e.i = (-a.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xE1: /* ~   A -> ~A */
             if(StackE_Get(stack)<1) return EXPR_ERR_STACK;
             a = StackE_Pop(stack);
             *value = e.i = (~a.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xE2: /* !   A -> !A */
             if(StackE_Get(stack)<1) return EXPR_ERR_STACK;
             a = StackE_Pop(stack);
             *value = e.i = (!a.i);
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xF0: /* ?:         A,B,C -> A?B:C */
             if(StackE_Get(stack)<3) return EXPR_ERR_STACK;
             c = StackE_Pop(stack); b = StackE_Pop(stack); a = StackE_Pop(stack);
             *value = e.i = a.i?b.i:c.i;
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xF1: /* DUP        A -> A,A */
             if(StackE_Get(stack)<1) return EXPR_ERR_STACK;
             a = StackE_Pop(stack);
             if(!StackE_Push(stack,&a)) return EXPR_ERR_OVERST;
             if(!StackE_Push(stack,&a)) return EXPR_ERR_OVERST;
             *value = a.i;
             break;
        case 0xF2: /* ROL        A,B -> B,A */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             if(!StackE_Push(stack,&b)) return EXPR_ERR_OVERST;
             if(!StackE_Push(stack,&a)) return EXPR_ERR_OVERST;
             *value = a.i;
             break;
        case 0xF3: /* LOAD       A -> mem[A] */
             if(StackE_Get(stack)<1) return EXPR_ERR_STACK;
             a = StackE_Pop(stack);
             ush = a.i;
             if(ush>=varslen)
             {  if(ush>=0xFF00U&&ush<0xFF10U) e.i = regs[ush-0xFF00];
                else return EXPR_ERR_INDEX;
             }
             else e.i = vars[ush];
             *value = e.i;
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xF4: /* SAVE       A,B -> .  (mem[A]=B) */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             ush = a.i;
             if(ush>=varslen)
             {  if(ush>=0xFF00U&&ush<0xFF10U) regs[ush-0xFF00] = b.i;
                else return EXPR_ERR_INDEX;
             }
             else vars[ush] = b.i;
             *value = b.i;
             break;
        case 0xF5: /* PUSH W   . -> W */
             if(*i>=memlen-2) return EXPR_ERR_STACK;
             *i=*i+1;
             psh = (short*)&mem[*i];
             *i=*i+1;
             *value = e.i = *psh;
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
        case 0xF6: /* []  A,B -> A[B] */
             if(StackE_Get(stack)<2) return EXPR_ERR_STACK;
             b = StackE_Pop(stack); a = StackE_Pop(stack);
             k = a.i;
             if(a.i>=0xFF00) k = regs[a.i-0xFF00]; // pointers
             *value = e.i = (k + b.i*_ElemSize);
             /*if(e.i>MAXVAL||e.i<MINVAL) return EXPR_ERR_OVERFL;*/
             if(!StackE_Push(stack,&e)) return EXPR_ERR_OVERST;
             break;
     }
     return ok;
}

int evaluate(unsigned char *mem,int memlen,
             short *vars,int varslen,short *regs)
{
  int i,k;
  short value = 0;
  StackE *stack;
  stack = StackE_New(256);
  for(i=0;i<memlen;i++)
  {
     k = EvalStep(stack,&i,mem,memlen,vars,varslen,regs,&value);
     if(k<1) return k;
  }
  if(StackE_Get(stack)) return EXPR_ERR_NONST;
  StackE_Del(stack);
  return 1;
}

#define HEX2(x1,x2) (((x1)<<4)|(x2))

#define MAXTRI 2000

typedef struct _Triplet
{
  unsigned char b1[5]; // Variable 1
  unsigned char b2[5]; // Variable 2
  unsigned char b3[5]; // Variable 3
  unsigned short adr;
  unsigned short w;
  char *str;
  short len;
} Triplet;

short CompExpr(char *str,char *name1,char *name2,char *name3)
{
   char s[100];
   char *p1,*p2;
   strcpy(s,str);
   p1 = strchr(s,'=');
   *p1=0;p1++;
   strcpy(name1,s);
   if(*p1=='-')
   {
     name3[0]=0;
     strcpy(name2,p1+1);
     return 0x21;
   }
   if(*p1=='~')
   {
     name3[0]=0;
     strcpy(name2,p1+1);
     return 0x2A;
   }
   p2 = strchr(p1,'+');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x22;
   }
   p2 = strchr(p1,'-');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x23;
   }
   p2 = strchr(p1,'/');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x24;
   }
   p2 = strchr(p1,'*');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x25;
   }
   p2 = strchr(p1,'%');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x26;
   }
   p2 = strchr(p1,'&');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x27;
   }
   p2 = strchr(p1,'|');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x28;
   }
   p2 = strchr(p1,'^');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x29;
   }
   strcpy(name2,p1);
   name3[0]=0;
   return 0x20;
}

void CompName(Text *v,char *pname,char *b,char *line,short n,short il,short *pvar)
{
   int i,j,k;
   Line *l;
   char *po;
   char str[100];
   int nsymbo = 0;
   int ndigit = 0;
   int nalpha = 0;
   char b5[5];
   char *rname = pname;
   while(*rname==' '||*rname=='\t')
   {  rname++;
      if(*rname==0) Error(4,line,il);
   }
   po = rname + strlen(rname) - 1;
   while(*po==' '||*po=='\t'){*po=0;po--;};
   if(strchr(rname,'[')!=NULL) // array element ???
   {
      strcpy(str,rname);
      po = strchr(str,'[');
      if(po!=NULL) *po=0;
      po++;
      k = strlen(po);
      if(po[k-1]!=']') Error(4,line,il);
      po[k-1]=0;
      l = TextFindFirstStr(v,str);
      if(l==NULL) Error(10,line,il);
      if(l->type!=2) Error(10,line,il);
      k = l->adr;
      CompName(v,po,(char*)b5,line,0,0,pvar);
      if(b5[0]>1) Error(4,line,il);
      if(b5[0]==0)
      {
         j = b5[1]+(b5[2]<<8);
         if(j>=l->len) Error(11,line,il);
         b[0] = 1;
         b[1] = (k+j)%256;
         b[2] = (k+j)/256;
      }
      if(b5[0]==1)
      {
         b[0] = 2;
         b[1] = k%256;
         b[2] = k/256;
         b[3] = b5[1];
         b[4] = b5[2];
      }
   }
   else
   {
      for(i=0;i<(int)strlen(rname);i++)
      {
         if(rname[i]=='_') nsymbo++;
         else
         {
            if(isdigit(rname[i])) ndigit++;
            else
            {
               if(isalpha(rname[i])) nalpha++;
               else Error2(4,line);
            }
         }
      }
      if(n)
      {
        if(nalpha==0) Error(4,line,il);
        l = TextFindFirstStr(v,rname);
        if(l==NULL)
        {
           l = TextAdd(v,rname);
           l->type = 1;
           l->adr = *pvar;
           (*pvar)++;
           if(*pvar>=VARMAX) Error(9,line,il);
        }
        if(l->type!=1) Error(6,line,il);
        if(l->adr>=0xFF00U) Error(12,line,il);
        b[0] = 1;
        b[1] = (l->adr)%256;
        b[2] = (l->adr)/256;
      }
      else
      {
        if(nalpha==0&&nsymbo==0&&ndigit>0)
        {  // number
           k = atoi(rname);
           b[0] = 0;
           b[1] = k%256;
           b[2] = k/256;
        }
        else // variable
        {
           l = TextFindFirstStr(v,rname);
           if(l==NULL) Error(10,line,il);
           if(l->type!=1) Error(10,line,il);
           b[0] = 1;
           b[1] = (l->adr)%256;
           b[2] = (l->adr)/256;
        }
      }
   }
}

short Prepro(Text *t)
{
   int i,j,k=0;
   Line *l;
   for(l=t->first;l!=NULL;l=(Line*)l->next)
   {
   }
   return k;
}

Robot* RobotNew(short num, char *s)
{
   char str[100],*sp,*po;
   int i,j,k,len,ftype;
   long fsize;
   unsigned short ks,ksf,offs;
   FILE *f;
   Line *l;
   Text *t;
   struct DebugFile *_fil,*_f_;
   struct DebugVar  *_var,*_v_;
   struct DebugLine *_lin,*_l_;
   Robot *r = (Robot*)malloc(sizeof(Robot));
   if(r==NULL) Error2(0,"Robot");
   r->number = num;
/* start of init */
   r->code = NULL;
   r->equip[0] = 0;
   r->equip[1] = 0;
   r->equip[2] = 0;
   r->equip[3] = 0;
   r->color = 0xFFFFFFL;
   r->fimage = 0;
   r->halt = 0;
   r->magic = 0;
   r->last = r->str0;
   r->X = r->Y = r->D = r->N = r->K = r->R = r->T = 0;
   r->A = r->B = r->C = r->P = r->L = r->S = r->H = 0;
   r->I = num;
   r->E = ENERGYBEG;
   r->M = MISSLEBEG;
   r->elec = 0;
   r->s_i = r->s_o = 0;
   *r->name = *r->author = 0;
   r->var = (short*)malloc(sizeof(short)*VARMAX);
   if(r->var==NULL) Error2(0,"VAR");
   for(i=0;i<VARMAX;i++) r->var[i]=0;
   r->sp = VARMAX-1;
   r->angle = MyRandom(4);
   r->StepJob = 0;
   r->Platform = 0;
   r->d_file = NULL;
   r->d_vars = NULL;
   r->d_line = NULL;
/* end of init */
   if(s==NULL) return r;
   ftype = 0;
   if(strlen(s)<100 && strchr(s,'\n')==NULL)
   {
     strcpy(str,s);
     for(i=0;i<(int)strlen(str);i++) str[i]=toupper(str[i]);
     len=strlen(str);
     if(len<4) Error2(2,s);
     if(str[len-4]=='.' && str[len-3]=='R' && str[len-2]=='W')
     {
       if(str[len-1]=='0') ftype = 1;
       else if(str[len-1]=='1') ftype = 2;
       else Error2(2,s);
     }
     else if(!strcmp(&str[len-6],".BCODE")) ftype = 1;
     else if(!strcmp(&str[len-6],".ROBOT")) ftype = 2;
     else Error2(2,s);
   }
   else ftype = 3;
   if(ftype==1)
   {
     f=fopen(s,"rb");
     fseek(f,0L,SEEK_END);
     fsize = ftell(f);
     fseek(f,0L,SEEK_SET);
     r->version = fgetc(f);
     if(r->version>2) Error2(15,s);
     ks = 0;
     ksf = fgetc(f);
     ksf += (unsigned short)fgetc(f)<<8;
     offs = fgetc(f);
     offs += (unsigned short)fgetc(f)<<8;
     for(i=5;i<fsize;i++) ks+=fgetc(f);
#if 0
     printf("KC=%u(%4.4X) KCF=%u(%4.4X)\n",ks,ks,ksf,ksf);
#endif
     if(ks!=ksf) Error2(15,s);
     fseek(f,5L,SEEK_SET);
     j = fgetc(f);
     if(j>=VARLEN) Error2(3,"ROBOTNAME");
     for(i=0;i<j;i++) r->name[i]=fgetc(f);
     r->name[i] = 0;
     if(fgetc(f)!=0) Error2(15,s);
     r->nvar = fgetc(f);
     r->nvar += (unsigned short)fgetc(f)<<8;
     r->cur = fgetc(f);
     r->cur += (unsigned short)fgetc(f)<<8;
     r->color = fgetc(f);
     r->color<<=8;
     r->color |= fgetc(f);
     r->color<<=8;
     r->color |= fgetc(f);
     for(i=0;i<4;i++) r->equip[i]=fgetc(f);
     j = fgetc(f);
     r->fimage = 0;
     if(j!=0&&j!=0x20) Error2(15,s);
     if(j==0x20)
     {
        for(j=0;j<8;j++){
        for(i=0;i<8;i+=2){
           k = fgetc(f);
           r->image[i][j] = (k&0xF0)>>4;
           r->image[i+1][j] = k&0x0F;
        }}
        r->fimage = 1;
     }
     i = 0;
     while(1)
     {
        if(ftell(f)>=offs) break;
        r->author[i] = fgetc(f);
        if(r->author[i]==0) break;
        if(++i>=VARLEN) Error2(3,"AUTHORNAME");
     }
     r->author[i]=0;

     fseek(f,offs,SEEK_SET);
     r->code = (unsigned char*)malloc(r->cur);
     if(r->code==NULL) Error2(0,"code");
     for(i=0;i<r->cur;i++) r->code[i]=fgetc(f);
     if(r->code[r->cur-1]!=0xFF) Error2(15,s);

     if(fgetc(f)=='D')
     {
        _f_=NULL;
        _v_=NULL;
        _l_=NULL;
        if(fgetc(f)!='E') Error2(15,s);
        if(fgetc(f)!='B') Error2(15,s);
        if(fgetc(f)!='U') Error2(15,s);
        if(fgetc(f)!='G') Error2(15,s);
        if(fgetc(f)!=0x0) Error2(15,s);
        while(1)
        {
          _fil = (struct DebugFile*)malloc(sizeof(struct DebugFile));
          if(_fil==NULL) Error2(0,"Robot Debug");
          _fil->next = NULL;
          if(r->d_file==NULL) r->d_file = _fil;
          else
          {
            for(_f_=r->d_file;_f_->next!=NULL;_f_=_f_->next);
            _f_->next = _fil;
          } 
          for(i=0;i<VARLEN;i++)
          {
            _fil->name[i] = fgetc(f);
            if(_fil->name[i]==0) break;
          }
          if(i==0) break;
          if(i==VARLEN) while(fgetc(f));
#if 0
          sprintf(str,"FILE '%s'\n",_fil->name);
          WriteShell(str);
#endif
        }
        if(_f_!=NULL&&_f_->next!=NULL){free(_f_->next);_f_->next=NULL;}
        while(1)
        {
          _var = (struct 
	  DebugVar*)malloc(sizeof(struct DebugVar));
          if(_var==NULL) Error2(0,"Robot Debug");
          _var->next = NULL;
          if(r->d_vars==NULL) r->d_vars = _var;
          else
          {
            for(_v_=r->d_vars;_v_->next!=NULL;_v_=_v_->next);
            _v_->next = _var;
          } 
          for(i=0;i<VARLEN;i++)
          {
            _var->name[i] = fgetc(f);
            if(_var->name[i]==0) break;
          }
          if(i==0) break;
          if(i==VARLEN) while(fgetc(f));
          _var->size = fgetc(f);
          _var->size |= fgetc(f)<<8;
          _var->addr = fgetc(f);
          _var->addr |= fgetc(f)<<8;
#if 0
          sprintf(str,"%s %4.4X %i\n",_var->name,_var->addr,_var->size);
          WriteShell(str);
#endif
        }
        if(_v_!=NULL&&_v_->next!=NULL){free(_v_->next);_v_->next=NULL;}
        if(r->d_vars!=NULL&&*r->d_vars->name==0){free(r->d_vars);r->d_vars=NULL;}
        while(1)
        {
          _lin = (struct DebugLine*)malloc(sizeof(struct DebugLine));
          if(_lin==NULL) Error2(0,"Robot Debug");
          _lin->next = NULL;
          if(r->d_line==NULL) r->d_line = _lin;
          else
          {
            for(_l_=r->d_line;_l_->next!=NULL;_l_=_l_->next);
            _l_->next = _lin;
          } 
          _lin->id = fgetc(f);
          if(_lin->id==0xFF) break;
          _lin->line = fgetc(f);
          _lin->line |= fgetc(f)<<8;
          _lin->addr = fgetc(f);
          _lin->addr |= fgetc(f)<<8;
#if 0
          sprintf(str,"%i-%i %4.4X\n",_lin->id,_lin->line,_lin->addr);
          WriteShell(str);
#endif
        }
        if(_l_!=NULL&&_l_->next!=NULL){free(_l_->next);_l_->next=NULL;}
     } 
     fclose(f);
   }
   if(ftype==2||ftype==3)
   {
     t = TextNew();
     if(t==NULL) Error1(0);
     if(ftype==2) TextLoad(t,s);
     else /* ftype==3 */
     {
        sp = (char*)malloc(strlen(s)+1);
	if(sp==NULL) Error2(0,"RobotNew");
        strcpy(sp,s);
        po = strtok(sp,"\n");
	while(po!=NULL)
	{
	  l = TextAdd(t,po);
	  if(l==NULL||l->str==NULL) Error2(0,"RobotNew");
	  po = strtok(NULL,"\n");
	}
	free(sp);
#if 0
        TextList(t);
#endif
     }
     i = 0;
     for(l=t->first;l!=NULL;l=(Line*)l->next)
     {
        l->id2=0;
        l->id=++i;
     }
     /* Preprocessing */
     if(Prepro(t) && ftype==2)
     {  str[strlen(str)-1]='_';
        TextSave(t,str);
     }
     RobotCompile(r,t);
     TextDel(t);
   }
   return r;
}

void RobotDel(Robot *r)
{
   free(r->var);
   free(r->code);
   if(r->d_file!=NULL)
   {  struct DebugFile *df,*df1;
      for(df=r->d_file;df!=NULL;df=df1){df1=df->next;free(df);}
   }
   if(r->d_vars!=NULL)
   {  struct DebugVar *dv,*dv1;
      for(dv=r->d_vars;dv!=NULL;dv=dv1){dv1=dv->next;free(dv);}
   }
   if(r->d_line!=NULL)
   {  struct DebugLine *dl,*dl1;
      for(dl=r->d_line;dl!=NULL;dl=dl1){dl1=dl->next;free(dl);}
   }
   free(r);
}

void RobotCompile(Robot *r, Text *t)
{
   Line *l,*l2,*l3;
   Text *vars;
   Triplet *ttt;
   char str[100];
   char st2[100];
   char st3[100];
   short files[MAXLINE];
   short lines[MAXLINE];
   char b5[5];
   char *p0,*p1,*p2,*p3,*p4,*p5;
   short *psh;
   long rr,gg,bb;
   int i,j,k=0;
   int frobot  = 0;
   int fauthor = 0;
   int fcolor  = 0;
   int fbody   = 0;
   int nimage  = 0;
   int ntripl  = 0;
   int ffront  = 0;
   int fback   = 0;
   int fleft   = 0;
   int fright  = 0;
   int fend    = 0;
   int ln = -1;
   int ifok=0;
   for(l=t->first;l!=NULL;l=(Line*)l->next)
   {  
      files[k]=l->id2;
      lines[k++]=l->id;
      if(k>MAXLINE) Error1(16);
      if(l->str[0]==0) continue;
      p1=l->str;
      p2=l->str;
      while(*p2==' '||*p2=='\t') p2++;
      while(*p2!=0)
      { *p1=*p2;
        p1++;p2++;
      }
      *p1=0;
      if(l->str[0]=='%') continue;
      j = strlen(l->str)-1;
      while(l->str[j]==' '||l->str[j]=='\t')
      {  l->str[j] = 0;
         j--;
      }
      if(l->str[0]==0) continue;
      StrUp(l->str);
   }
   r->cur=0;
   r->nvar=0;
   vars = TextNew();
   if(vars==NULL) Error1(0);
   l=TextAdd(vars,"X");l->type=1;l->adr=0xFF00U;
   l=TextAdd(vars,"Y");l->type=1;l->adr=0xFF01U;
   l=TextAdd(vars,"D");l->type=1;l->adr=0xFF02U;
   l=TextAdd(vars,"N");l->type=1;l->adr=0xFF03U;
   l=TextAdd(vars,"K");l->type=1;l->adr=0xFF04U;
   l=TextAdd(vars,"R");l->type=1;l->adr=0xFF05U;
   l=TextAdd(vars,"T");l->type=1;l->adr=0xFF06U;
   l=TextAdd(vars,"E");l->type=1;l->adr=0xFF07U;
   l=TextAdd(vars,"M");l->type=1;l->adr=0xFF08U;
   l=TextAdd(vars,"I");l->type=1;l->adr=0xFF09U;
   l=TextAdd(vars,"S");l->type=1;l->adr=0xFF0FU;
   ttt = (Triplet*)malloc(sizeof(Triplet)*MAXTRI);
   if(ttt==NULL) Error2(0,"Triplet");
   for(i=0;i<MAXTRI;i++) ttt->str=NULL;
   for(l=t->first;l!=NULL;l=(Line*)l->next)
   {  ln++;
      if(l->str[0]==0){l->id=0;continue;}
      if(l->str[0]=='%'){l->id=0;continue;}
      if(fend) Error(13,l->str,lines[ln]);
      strcpy(str,l->str);
      p1=str;
      if(StartWith(l->str,"ROBOT",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(frobot) Error(6,l->str,lines[ln]);
         l->id=0;
         frobot=1;
         p1=SkipSpace(p1+5);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<16;j++)
         {
            if(*p1==0) Error(5,l->str,lines[ln]);
            if(*p1=='"') break;
            r->name[j]=*p1;
            p1++;
         }
         r->name[j]=0;
         while(*p1!='"'&&*p1!=0) p1++;
         if(*p1==0) Error(5,l->str,lines[ln]);
         p1=SkipSpace(p1+1);
         if(*p1!='%'&&*p1!=0) Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"AUTHOR",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(fauthor) Error(6,l->str,lines[ln]);
         l->id=0;
         fauthor=1;
         p1=SkipSpace(p1+6);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<16;j++)
         {
            if(*p1==0) Error(5,l->str,lines[ln]);
            if(*p1=='"') break;
            r->author[j]=*p1;
            p1++;
         }
         r->author[j]=0;
         while(*p1!='"'&&*p1!=0) p1++;
         if(*p1==0) Error(5,l->str,lines[ln]);
         p1=SkipSpace(p1+1);
         if(*p1!='%'&&*p1!=0) Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"COLOR",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(fcolor) Error(6,l->str,lines[ln]);
         l->id=0;
         fcolor=1;
         p1=SkipSpace(p1+5);
         p2 = strchr(p1,'%');
         if(p2!=NULL) *p2=0;
         else p2 = &p1[strlen(p1)];
         p2--;
         while(*p2==' '||*p2=='\t'){*p2=0;p2--;};
         if(strlen(p1)!=6) Error(4,l->str,lines[ln]);
         rr = HEX2(hexx(p1[0]),hexx(p1[1]));
         gg = HEX2(hexx(p1[2]),hexx(p1[3]));
         bb = HEX2(hexx(p1[4]),hexx(p1[5]));
         r->color = (rr<<16)|(gg<<8)|bb;
         continue;
      }
      if(StartWith(l->str,"IMAGE",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(nimage>=8) Error(6,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+5);
         for(j=0;j<8;j++)
         {
           if(*p1==0||*p1=='%') Error(4,l->str,lines[ln]);
           r->image[j][nimage] = hexx(*p1);
           p1 = SkipSpace(p1+1);
         }
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         nimage++;
         r->fimage=1;
         continue;
      }
      if(StartWith(l->str,"FRONT",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(ffront) Error(6,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+5);
         j=0;
         if(StartWith(p1,"EYE",1)){j=1;r->equip[0]=1;}
         if(StartWith(p1,"GUN",1)){j=1;r->equip[0]=2;}
         if(j==0) Error(4,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         ffront=1;
         continue;
      }
      if(StartWith(l->str,"BACK",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(fback) Error(6,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+4);
         j=0;
         if(StartWith(p1,"EYE",1)){j=1;r->equip[2]=1;}
         if(StartWith(p1,"GUN",1)){j=1;r->equip[2]=2;}
         if(j==0) Error(4,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         fback=1;
         continue;
      }
      if(StartWith(l->str,"LEFT",1))
      {
         p1=SkipSpace(p1+4);
         if(fbody)
         {
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           l->adr = r->cur++;
           l->id = 0x35;
         }
         else
         {
           if(fleft) Error(6,l->str,lines[ln]);
           l->id=0;
           j=0;
           if(StartWith(p1,"EYE",1)){j=1;r->equip[3]=1;}
           if(StartWith(p1,"GUN",1)){j=1;r->equip[3]=2;}
           if(j==0) Error(4,l->str,lines[ln]);
           p1=SkipSpace(p1+3);
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           fleft=1;
         }
         continue;
      }
      if(StartWith(l->str,"RIGHT",1))
      {
         p1=SkipSpace(p1+5);
         if(fbody)
         {
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           l->adr = r->cur++;
           l->id = 0x36;
         }
         else
         {
           if(fright) Error(6,l->str,lines[ln]);
           l->id=0;
           j=0;
           if(StartWith(p1,"EYE",1)){j=1;r->equip[1]=1;}
           if(StartWith(p1,"GUN",1)){j=1;r->equip[1]=2;}
           if(j==0) Error(4,l->str,lines[ln]);
           p1=SkipSpace(p1+3);
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           fright=1;
         }
         continue;
      }
      if(StartWith(l->str,"GOTO",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         l2 = TextFindFirstStrTyp(vars,p2,0);
         if(l2==NULL)
         {
            l2 = TextAdd(vars,p2);
            l2->type = -1;
            l2->adr = TextIndex(t,l);
         }
         l->adr = r->cur;
         l->id = 0x30;
         l->id2 = l2->adr;
         r->cur += 3;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"CALL",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         l2 = TextFindFirstStrTyp(vars,p2,0);
         if(l2==NULL)
         {
            l2 = TextAdd(vars,p2);
            l2->type = -1;
            l2->adr = TextIndex(t,l);
         }
         l->adr = r->cur;
         l->id = 0x31;
         l->id2 = l2->adr;
         r->cur += 3;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"RET",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = r->cur++;
         l->id = 0x33;
         continue;
      }
      if(StartWith(l->str,"STEP",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = r->cur++;
         l->id = 0x34;
         continue;
      }
      if(StartWith(l->str,"SAY",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<100;j++)
         {
            if(*p1=='"') break;
            if(*p1==0) break;
            st2[j]=*p1;
            p1++;
         }
         st2[j]=0;
         ttt[ntripl].b1[0]=99;
         ttt[ntripl].str = (char*)malloc(strlen(st2)+1);
         if(ttt[ntripl].str==NULL) Error(0,"Compile",lines[ln]);
         strcpy(ttt[ntripl].str,st2);
         l->adr = r->cur;
         l->id = 0x37;
         l->id2 = ntripl;
         r->cur += strlen(st2)+2;
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,l->str,lines[ln]);
         if(*p1!='"') Error(4,l->str,lines[ln]);
         p1=SkipSpace(p1+1);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"PRINT",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+5);
         p2=strtok(p1," \t");
         CompName(vars,p2,(char*)b5,l->str,0,lines[ln],&r->nvar);
         l->adr = r->cur;
         l->id = 0x38;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) r->cur+=4;
         else r->cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"TEST",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<100;j++)
         {
            if(*p1=='"') break;
            if(*p1==0) break;
            st2[j]=*p1;
            p1++;
         }
         st2[j]=0;
         ttt[ntripl].b1[0]=100;
         ttt[ntripl].str = (char*)malloc(strlen(st2)+1);
         if(ttt[ntripl].str==NULL) Error(0,"Compile",lines[ln]);
         strcpy(ttt[ntripl].str,st2);
         l->adr = r->cur;
         l->id = 0x51;
         l->id2 = ntripl;
         r->cur += strlen(st2)+4;
         if(*p1!='"') Error(4,l->str,lines[ln]);
         p1=SkipSpace(++p1);
         if(*p1!=':') Error(4,l->str,lines[ln]);
         p1=SkipSpace(++p1);
         p2=strtok(p1," \t");
         l2 = TextFindFirstStrTyp(vars,p2,0);
         if(l2==NULL)
         {
            l2 = TextAdd(vars,p2);
            l2->type = -1;
            l2->adr = TextIndex(t,l);
         }
         ttt[ntripl].b3[0] = 98;
         ttt[ntripl].b3[1] = (l2->adr)&0xFF;
         ttt[ntripl].b3[2] = (l2->adr)>>8;
         p2=strtok(NULL," \t");
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,l->str,lines[ln]);
         if(p2!=NULL&&*p2!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"DUMP",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         l->adr = r->cur;
         l->id = 0x3F;
         l->id2 = 0x00;
         l->len = atoi(p2);
         r->cur+=3;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"DEF",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+3);
         strcpy(st2,p1);
         p2 = strchr(st2,'[');
         if(p2!=NULL) *p2=0;
         else Error(4,l->str,lines[ln]);
         p2++;
         p3 = strchr(p2,']');
         if(p3!=NULL) *p3=0;
         else Error(4,l->str,lines[ln]);
         p3++;
         p3=SkipSpace(p3);
         k = 0x01;
         if(*p3!=0&&*p3!='%')
         {
            if(*p3!='=') Error(4,l->str,lines[ln]);
            p3=SkipSpace(p3+1);
            if(*p3!='{') Error(4,l->str,lines[ln]);
            p3++;
            p4 = strchr(p3,'}');
            if(p4!=NULL) *p4=0;
            else Error(4,l->str,lines[ln]);
            p4++;
            p4=SkipSpace(p4);
            if(*p4!=0&&*p4!='%') Error(4,l->str,lines[ln]);
            i=1;
            p5=p4=p3;
            while(*p4!=0)
            {
               if(*p4==',') i++;
               if(*p4==' '||*p4=='\t') p4++;
               *p5=*p4;
               p4++;p5++;
            }
            *p5=0;
            ttt[ntripl].b1[0] = r->nvar%256;
            ttt[ntripl].b1[1] = r->nvar/256;
            ttt[ntripl].b1[2] = i%256;
            ttt[ntripl].b1[3] = i/256;
            ttt[ntripl].str = (char*)malloc(2*i);
            ttt[ntripl].len = 2*i;
            psh = (short*)ttt[ntripl].str;
            ntripl++;
            p4 = strtok(p3,",");
            for(j=0;j<i;j++)
            {
               psh[j] = atoi(p4);
               p4 = strtok(NULL,",");
            }
            k = 0x02;
         }
         l2 = TextFindFirstStr(vars,str);
         if(l2!=NULL) Error(6,l->str,lines[ln]);
         j = atoi(p2);
         if(j<1) Error(4,l->str,lines[ln]);
         if(j>=VARMAX) Error(9,l->str,lines[ln]);
         if((long)r->nvar+j>=(long)VARMAX) Error(4,l->str,lines[ln]);
         l2 = TextAdd(vars,st2);
         l2->type = 2;
         l2->adr = r->nvar;
         l2->len = j;
         l->id = k;
         if(k==1) l->id2 = r->nvar;
         else l->id2 = ntripl-1;
         l->len = j;
         l->adr = r->cur;
         if(k==1) r->cur += 5;
         else r->cur += 7+2*i;
         r->nvar += j;
         continue;
      }
      if(StartWith(l->str,"ACT",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         p2=strtok(p1," \t");
         k=0;b5[0]=b5[2]=b5[3]=b5[4]=0;
         if(!strcmp(p2,"FRONT")){k=1;b5[1]=0;}
         if(!strcmp(p2,"RIGHT")){k=1;b5[1]=1;}
         if(!strcmp(p2,"BACK")){k=1;b5[1]=2;}
         if(!strcmp(p2,"LEFT")){k=1;b5[1]=3;}
         if(!k) CompName(vars,p2,(char*)b5,l->str,0,lines[ln],&r->nvar);
         l->adr = r->cur;
         l->id = 0x3A;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) r->cur+=4;
         else r->cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"SPY",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = r->cur++;
         l->id = 0x3B;
         continue;
      }
      if(StartWith(l->str,"RADAR",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+5);
         p2=strtok(p1," \t");
         CompName(vars,p2,(char*)b5,l->str,0,lines[ln],&r->nvar);
         l->adr = r->cur;
         l->id = 0x3C;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) r->cur+=4;
         else r->cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"SHOT",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         CompName(vars,p2,(char*)b5,l->str,0,lines[ln],&r->nvar);
         l->adr = r->cur;
         l->id = 0x60;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) r->cur+=4;
         else r->cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"SEND",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p0=SkipSpace(p1+4);
         p1=strtok(p0," \t");
         p2=strtok(NULL," \t");
         CompName(vars,p1,(char*)ttt[ntripl].b1,l->str,0,lines[ln],&r->nvar);
         if(p2==NULL) ttt[ntripl].b2[0]=ttt[ntripl].b2[1]=ttt[ntripl].b2[2]=0;
         else CompName(vars,p2,(char*)ttt[ntripl].b2,l->str,0,lines[ln],&r->nvar);
         j = 1;
         if(ttt[ntripl].b1[0]<2) j+=3;
         else j+=5;
         if(ttt[ntripl].b2[0]<2) j+=3;
         else j+=5;
         l->adr = r->cur;
         l->id = 0x61;
         l->type = 101;
         l->id2 = ntripl;
         r->cur += j;
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,l->str,lines[ln]);
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"RECV",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         CompName(vars,p2,(char*)b5,l->str,1,lines[ln],&r->nvar);
         l->adr = r->cur;
         l->id = 0x62;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) r->cur+=4;
         else r->cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"POP",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = r->cur++;
         l->id = 0x63;
         continue;
      }
      if(StartWith(l->str,"IF",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+2);
         // expression !!!

         p3=strchr(p1,':');
         if(p3==NULL) Error(4,l->str,lines[ln]);
         *p3=0;
         p3=SkipSpace(p3+1);
         k=0;
         if(!k) p2=strchr(p1,'<');
         if(!k&&p2!=NULL) // < <= <>
         {
            *p2=0;p2++;
            if(*p2=='>') // <>
            {
               *p2=0;p2++;
               l->id = 0x15;
               ifok = 1;
            }
            if(!ifok&&*p2=='=') // <=
            {
              *p2=0;p2++;
              l->id = 0x12;
              ifok = 1;
            }
            if(!ifok) l->id = 0x10;
            k=1;
         }
         if(!k) p2=strchr(p1,'>');
         if(!k&&p2!=NULL) // > >=
         {
            *p2=0;p2++;
            if(*p2=='=')
            {
              *p2=0;p2++;
              l->id = 0x13;
            }
            else l->id = 0x11;
            k=1;
         }
         if(!k) p2=strchr(p1,'!');
         if(!k&&p2!=NULL) // !=
         {
            *p2=0;p2++;
            if(*p2!='=') Error(4,l->str,lines[ln]);
            *p2=0;p2++;
            l->id = 0x15;
            k=1;
         }
         if(!k) p2=strchr(p1,'=');
         if(!k&&p2!=NULL) // == =
         {
            *p2=0;p2++;
            if(*p2=='='){*p2=0;p2++;};
            l->id = 0x14;
            k=1;
         }
         if(k==0) Error(4,l->str,lines[ln]);
         //printf("IF (%2.2X) <%s> <%s> ",l->id,p1,p2);
         CompName(vars,p1,(char*)ttt[ntripl].b1,l->str,0,lines[ln],&r->nvar);
         CompName(vars,p2,(char*)ttt[ntripl].b2,l->str,0,lines[ln],&r->nvar);
         j = 3;
         if(ttt[ntripl].b1[0]<2) j+=3;
         else j+=5;
         if(ttt[ntripl].b2[0]<2) j+=3;
         else j+=5;
         p1 = strtok(p3," \t");
         l2 = TextFindFirstStrTyp(vars,p1,0);
         if(l2==NULL)
         {
            l2 = TextAdd(vars,p1);
            l2->type = -1;
            l2->adr = TextIndex(t,l);
         }
         l->adr = r->cur;
         ttt[ntripl].b3[0] = 98;
         ttt[ntripl].b3[1] = (l2->adr)%256;
         ttt[ntripl].b3[2] = (l2->adr)/256;
         l->id2 = ntripl;
         l->len = 2;
         r->cur += j;
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,"TRI>MAXTRI",lines[ln]);
         p2 = strtok(NULL," \t");
         if(p2!=NULL&&*p2!='%') Error(4,l->str,lines[ln]);
         continue;
      }
      if(StartWith(l->str,"END",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = r->cur++;
         l->id = 0xFF;
         fend = 1;
         continue;
      }
      // label or simple expression
      {
         if(StartWith(l->str,"START:",1)) fbody=1;
         if(!fbody) Error(8,l->str,lines[ln]);
         p2=p1;
         while(*p1)
         {
            while(*p1==' '||*p1=='\t')
            {
               p1++;
               if(*p1=='%')
               { *p1=0;
                 p1--;
                 *p1=0;
               }
            }
            *p2 = *p1;
            p1++;
            p2++;
         }
         *p2 = 0;
         j = strlen(str);
         if(str[j-1]==':') // LABEL
         {  l->id = 0; // !!!
            str[j-1]=0;
            k=1;
            for(j=0;j<(int)strlen(str);j++)
            {
               if(!isalpha(str[j])&&!isdigit(str[j])&&str[j]!='_') k=0;
            }
            if(!k) Error(4,l->str,lines[ln]);
            l2 = TextFindFirstStr(vars,str);
            while(l2!=NULL)
            {
               if(l2->type!=-1) Error(6,l->str,lines[ln]);
               l3 = TextGet(t,l2->adr);
               switch(l3->id)
               {
                  case 0x30:
                  case 0x31:
                    l3->id2 = r->cur;
                    break;
                  default: // IF, TEST
                    if(ttt[l3->id2].b3[0]!=98) Error(99,l->str,lines[ln]);
                    ttt[l3->id2].b3[1] = r->cur%256;
                    ttt[l3->id2].b3[2] = r->cur/256;
               }
               l2 = TextFindNext(vars);
            }
            l2 = TextAdd(vars,str);
            l2->type = 0;
            l2->adr = r->cur;
         }
         else // Expression !!!
         {
            char name1[20];
            char name2[20];
            char name3[20];
            if(strchr(str,'=')==NULL) Error(4,l->str,lines[ln]);
            k = CompExpr(str,name1,name2,name3);
            l->adr = r->cur;
            l->id = k;
            l->id2 = ntripl;
            l->len = 3;
            if(name3[0]==0) l->len--;
            else CompName(vars,name3,(char*)ttt[ntripl].b3,l->str,0,lines[ln],&r->nvar);
            CompName(vars,name1,(char*)ttt[ntripl].b1,l->str,1,lines[ln],&r->nvar);
            CompName(vars,name2,(char*)ttt[ntripl].b2,l->str,0,lines[ln],&r->nvar);
            k = 1;
            if(ttt[ntripl].b1[0]<2) k+=3;
            else k+=5;
            if(ttt[ntripl].b2[0]<2) k+=3;
            else k+=5;
            if(name3[0]!=0)
            {
               if(ttt[ntripl].b3[0]<2) k+=3;
               else k+=5;
            }
            r->cur += k;
            ntripl++;
            if(ntripl>=MAXTRI) Error(99,"TRI>MAXTRI",lines[ln]);
         }
/*         p1 = str; */
      }
   }
   if(fend==0) Error(14,"end of file",lines[ln]);

   #if 0
     printf("\n\nTEXT:\n");
     printf("=====\n");
     TextList(t);
     printf("\n\nVARS:\n");
     printf("=====\n");
     TextList(vars);
     printf("\n\n");
   #endif

   for(l=vars->first;l!=NULL;l=(Line*)l->next)
   {
      if(l->type==-1)
      {
         for(l2=vars->first;l2!=NULL;l2=(Line*)l2->next)
         {
            if(!strcmp(l->str,l2->str)&&l2->type==0) break;
         }
         if(l2==NULL)
         {  l3 = TextGet(t,l->adr);
            Error(17,l3->str,l->adr+1);
         }
      }
   }

   r->code = (unsigned char*)malloc(r->cur);
   if(r->code==NULL) Error2(0,"Compile");
#if 0
   printf("\n name=%s\n",r->name);
   printf(" var=%u\n",r->nvar);
   printf(" code=%u\n",r->cur);
#endif   

   i = 0;
   ln = -1;
   for(l=t->first;l!=NULL;l=(Line*)l->next)
   {
      if(l->id==0) continue;
      ln++;
      r->code[i] = l->id;
      if(r->code[i]==0x01) // DEF
      {  i++;
         r->code[i++] = (l->id2)%256;
         r->code[i++] = (l->id2)/256;
         r->code[i++] = (l->len)%256;
         r->code[i++] = (l->len)/256;
         continue;
      }
      if(r->code[i]==0x02) // DEF 2
      {  i++;
         r->code[i++] = ttt[l->id2].b1[0];
         r->code[i++] = ttt[l->id2].b1[1];
         r->code[i++] = (l->len)%256;
         r->code[i++] = (l->len)/256;
         r->code[i++] = ttt[l->id2].b1[2];
         r->code[i++] = ttt[l->id2].b1[3];
         psh = (short*)ttt[l->id2].str;
         for(j=0;j<(ttt[l->id2].len>>1);j++)
         {
             r->code[i++] = psh[j]&0xFF;
             r->code[i++] = (psh[j]>>8)&0xFF;
         }
         continue;
      }
      if(r->code[i]<0x20) // IF
      {  i++;
         if(ttt[l->id2].b3[0]!=98) Error(99,l->str,lines[ln]);
         r->code[i++] = ttt[l->id2].b3[1];
         r->code[i++] = ttt[l->id2].b3[2];
         r->code[i++] = ttt[l->id2].b1[0];
         r->code[i++] = ttt[l->id2].b1[1];
         r->code[i++] = ttt[l->id2].b1[2];
         if(ttt[l->id2].b1[0]==2)
         {
            r->code[i++] = ttt[l->id2].b1[3];
            r->code[i++] = ttt[l->id2].b1[4];
         }
         r->code[i++] = ttt[l->id2].b2[0];
         r->code[i++] = ttt[l->id2].b2[1];
         r->code[i++] = ttt[l->id2].b2[2];
         if(ttt[l->id2].b2[0]==2)
         {
            r->code[i++] = ttt[l->id2].b2[3];
            r->code[i++] = ttt[l->id2].b2[4];
         }
         continue;
      }
      if(r->code[i]<0x30) // EXPRESSIONS
      {  i++;
         r->code[i++] = ttt[l->id2].b1[0];
         r->code[i++] = ttt[l->id2].b1[1];
         r->code[i++] = ttt[l->id2].b1[2];
         if(ttt[l->id2].b1[0]==2)
         {
            r->code[i++] = ttt[l->id2].b1[3];
            r->code[i++] = ttt[l->id2].b1[4];
         }
         r->code[i++] = ttt[l->id2].b2[0];
         r->code[i++] = ttt[l->id2].b2[1];
         r->code[i++] = ttt[l->id2].b2[2];
         if(ttt[l->id2].b2[0]==2)
         {
            r->code[i++] = ttt[l->id2].b2[3];
            r->code[i++] = ttt[l->id2].b2[4];
         }
         if(l->len==3)
         {
            r->code[i++] = ttt[l->id2].b3[0];
            r->code[i++] = ttt[l->id2].b3[1];
            r->code[i++] = ttt[l->id2].b3[2];
            if(ttt[l->id2].b3[0]==2)
            {
               r->code[i++] = ttt[l->id2].b3[3];
               r->code[i++] = ttt[l->id2].b3[4];
            }
         }
         else if(l->len!=2) Error(99,l->str,lines[ln]);
         continue;
      }
      if((r->code[i]>=0x33&&r->code[i]<=0x36)||r->code[i]==0x3B||
          r->code[i]==0x63||r->code[i]==0xFF) // 1
      {  i++;
         continue;
      }
      if(r->code[i]==0x37) // SAY
      {  i++;
         p1 = ttt[l->id2].str;
         for(j=0;j<(int)strlen(p1);j++) r->code[i++]=p1[j];
         r->code[i++]=0;
         free(p1);
         continue;
      }
      if(r->code[i]==0x51) // TEST
      {  i++;
         r->code[i++] = ttt[l->id2].b3[1];
         r->code[i++] = ttt[l->id2].b3[2];
         p1 = ttt[l->id2].str;
         for(j=0;j<(int)strlen(p1);j++) r->code[i++]=p1[j];
         r->code[i++]=0;
         free(p1);
         continue;
      }
      if(r->code[i]==0x3F) // DUMP
      {  i++;
         r->code[i++]=l->id2;
         if(l->id2==0)
         {
            r->code[i++]=l->len;
         }
         continue;
      }
      if(r->code[i]==0x30||r->code[i]==0x31) // GOTO, CALL
      {  i++;
         r->code[i++] = (l->id2)%256;
         r->code[i++] = (l->id2)/256;
         continue;
      }
      if(r->code[i]==0x61) // SEND
      {  i++;
         r->code[i++] = ttt[l->id2].b1[0];
         r->code[i++] = ttt[l->id2].b1[1];
         r->code[i++] = ttt[l->id2].b1[2];
         if(ttt[l->id2].b1[0]==2)
         {
            r->code[i++] = ttt[l->id2].b1[3];
            r->code[i++] = ttt[l->id2].b1[4];
         }
         r->code[i++] = ttt[l->id2].b2[0];
         r->code[i++] = ttt[l->id2].b2[1];
         r->code[i++] = ttt[l->id2].b2[2];
         if(ttt[l->id2].b2[0]==2)
         {
            r->code[i++] = ttt[l->id2].b2[3];
            r->code[i++] = ttt[l->id2].b2[4];
         }
         continue;
      }
      if(r->code[i]==0x38||r->code[i]==0x3A||r->code[i]==0x3C|| // PRINT, ACT, RADAR
         r->code[i]==0x60||r->code[i]==0x62)  // SHOT, RECV
      {  i++;
         r->code[i++] = l->type;
         r->code[i++] = (l->len)%256;
         r->code[i++] = (l->len)/256;
         if(l->type==2)
         {
            r->code[i++] = (l->id2)%256;
            r->code[i++] = (l->id2)/256;
         }
         continue;
      }
      Error(99,"COMMAND",lines[ln]);
   }
   if(r->cur!=i)
   {
      Error2(99,"CUR not eq I");
   }

   #ifdef RDUMP
     printf("\n// DUMP:\n");
     printf("#define RLEN %u\n",cur);
     printf("#define VLEN %u\n",r->nvar+2);
     printf("unsigned char robot[%u] = {\n",cur);
     for(i=0;i<cur;i++)
     {
        printf(" 0x%2.2X",r->code[i]);
        if(i!=cur-1) printf(",");
        if(i%10==9) printf("\n");
     }
     printf("\n};\n\n");
   #endif

   for(i=0;i<MAXTRI;i++) if(ttt->str!=NULL) free(ttt->str);
   free(ttt);

   TextDel(vars);
}

void RobotSave(Robot *r, char *fname)
{
   char *po,str[100];
   int off,i,j,k,fsize;
   unsigned short kc;
   FILE* f;
   strcpy(str,fname);
   po = strrchr(str,'.');
   if(po!=NULL) *po=0;
   strcat(str,".rw0");
   f = fopen(str,"wb");
   if(f==NULL) Error2(1,str);
   off = 0;
   fputc(2,f); /* VER 2.0 */
   fputc(0,f);fputc(0,f);
   fputc(0,f);fputc(0,f);
   off += 5;
   k = strlen(r->name);
   fputc(k,f);
   for(i=0;i<k;i++) fputc(r->name[i],f);
   fputc(0,f);
   off += k+2;
   fputc(r->nvar%256,f);fputc(r->nvar/256,f);
   fputc(r->cur%256,f);fputc(r->cur/256,f);
   off += 4;
   fputc((r->color>>16)&0xFF,f);
   fputc((r->color>>8)&0xFF,f);
   fputc(r->color&0xFF,f);
   off += 3;
   for(i=0;i<4;i++) fputc(r->equip[i],f);
   off += 4;
   if(r->fimage)
   {
      fputc(0x20,f);
      for(j=0;j<8;j++){
      for(i=0;i<8;i+=2){
         k = (r->image[i][j]<<4)|r->image[i+1][j];
         fputc(k,f);
      }}
      off += 33;
   }
   else
   {
      fputc(0,f);
      off++;
   }
   for(i=0;i<=(int)strlen(r->author);i++)
   {
      fputc(r->author[i],f);
      off++;
   }
   fputc(0,f);
   fputc(0,f);
   off += 2;
   for(i=0;i<r->cur;i++)
   {
      fputc(r->code[i],f);
   }
   fclose(f);
   f = fopen(str,"rb+");
   if(f==NULL) Error2(1,str);
   fseek(f,0L,SEEK_END);
   fsize = ftell(f);
   fseek(f,5L,SEEK_SET);
   kc = 0;
   for(i=5;i<fsize;i++)
   {
     kc += fgetc(f);
   }
   fseek(f,1L,SEEK_SET);
   fputc(kc%256,f);
   fputc(kc/256,f);
   fputc(off%256,f);
   fputc(off/256,f);
   fclose(f);
}

short RobotVar(Robot *r, short f)
{
   int i = f;
#ifndef _WITHOUT_INTERPRETER
   int j;
   long k;
   switch(r->code[r->P++])
   {
     case 0:
       i = r->code[r->P++];
       i += r->code[r->P++]<<8;
       break;
     case 1:
       j = r->code[r->P++];
       j += r->code[r->P++]<<8;
       if(j>=0xFF00)
       {
          switch(j)
          { case 0xFF00: i=r->X; break;
            case 0xFF01: i=r->Y; break;
            case 0xFF02: i=r->D; break;
            case 0xFF03: i=r->N; break;
            case 0xFF04: i=r->K; break;
            case 0xFF05: i=r->R; break;
            case 0xFF06: i=r->T; break;
            case 0xFF07: i=r->E; break;
            case 0xFF08: i=r->M; break;
            case 0xFF09: i=r->I; break;
            case 0xFF0A: i=r->A; break;
            case 0xFF0B: i=r->B; break;
            case 0xFF0C: i=r->C; break;
            case 0xFF0D: i=r->P; break;
            case 0xFF0E: i=r->L; break;
            case 0xFF0F: i=r->S; break;
          }
          if(f) i=j;
       }
       else
       {
          if(j<0||j>=VARMAX) return 0;
          if(f) i=j;
          else i=r->var[j];
       }
       break;
     case 2:
       i = r->code[r->P++];
       i += r->code[r->P++]<<8;
       j = r->code[r->P++];
       j += r->code[r->P++]<<8;
       if(j>=0xFF00)
       {
          switch(j)
          { case 0xFF00: k=(long)i+r->X; break;
            case 0xFF01: k=(long)i+r->Y; break;
            case 0xFF02: k=(long)i+r->D; break;
            case 0xFF03: k=(long)i+r->N; break;
            case 0xFF04: k=(long)i+r->K; break;
            case 0xFF05: k=(long)i+r->R; break;
            case 0xFF06: k=(long)i+r->T; break;
            case 0xFF07: k=(long)i+r->E; break;
            case 0xFF08: k=(long)i+r->M; break;
            case 0xFF09: k=(long)i+r->I; break;
            case 0xFF0A: k=(long)i+r->A; break;
            case 0xFF0B: k=(long)i+r->B; break;
            case 0xFF0C: k=(long)i+r->C; break;
            case 0xFF0D: k=(long)i+r->P; break;
            case 0xFF0E: k=(long)i+r->L; break;
            case 0xFF0F: k=(long)i+r->S; break;
          }
       }
       else
       {
          if(j<0||j>=VARMAX) return 0;
          k = (long)i+r->var[j];
       }
       if(k<0||k>=VARMAX) return 0;

       if(f) i=k;
       else i=r->var[k];
       break;
   }
   return i;
#else
   printf("RobotVar(%0x8.8X,%i)",r,i);
   return 0;
#endif
}

short RobotAdr(Robot *r)
{
   short i;
   i = r->code[r->P++];
   i += r->code[r->P++]<<8;
   return i;
}

char* RobotStep(Robot *r, FILE *f)
{
#ifndef _WITHOUT_INTERPRETER
   char str2[256],*po;
   short i,j,k,k2,k3,xx,yy,dd,lastcmd,nfree=NFREE;
   int ad1,ad2,ad3,tmp;
   char *str;
   str = r->last;
   r->StepJob = 1;
 freeloop:
   po = NULL;
   r->R = MyRandom(1000);
   r->command = 0;
   if(r->E<=0){r->StepJob=0;return NULL;}
   if((r->xr>0&&MAPLO(r->xr-1,r->yr)==0x40)||
      (r->yr>0&&MAPLO(r->xr,r->yr-1)==0x40)||
      (r->xr<r->dx-1&&MAPLO(r->xr+1,r->yr)==0x40)||
      (r->yr<r->dy-1&&MAPLO(r->xr,r->yr+1)==0x40))
   {
      r->elec++;
      if(r->elec>=ENERGWAIT)
      {  r->elec = 0;
         if(r->E>0) r->E++;
         if(r->E>ENERGYMAX) r->E=ENERGYMAX;
      }
   }
   else r->elec=0;
   if(r->P>=r->cur) r->halt=1;
   if(r->halt){r->StepJob=0;return NULL;}
   lastcmd = r->code[r->P++];
#if 0
   WriteShellInt(lastcmd);
   WriteShell("\n");
#endif
   switch(lastcmd)
   {
     case 0xFF:
          r->halt=1;
          break;
     case 0x00:
          break;
     case 0x01:
          ad1 = RobotAdr(r);
          ad2 = RobotAdr(r);
          tmp = ad1+ad2;
          break;
     case 0x02:
          ad1 = RobotAdr(r);
          ad2 = RobotAdr(r);
          ad3 = RobotAdr(r);
          for(k=0;k<ad3;k++) r->var[ad1+k]=RobotAdr(r);
          tmp = ad2;
          break;
     case 0x10:
          ad1 = RobotAdr(r);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i<j) r->P=ad1;
          break;
     case 0x11:
          ad1 = RobotAdr(r);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i>j) r->P=ad1;
          break;
     case 0x12:
          k = RobotAdr(r);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i<=j) r->P=k;
          break;
     case 0x13:
          k = RobotAdr(r);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i>=j) r->P=k;
          break;
     case 0x14:
          k = RobotAdr(r);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i==j) r->P=k;
          break;
     case 0x15:
          k = RobotAdr(r);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i!=j) r->P=k;
          break;
     case 0x20:
          ad1 = RobotVar(r,1)&0xFFFF;
          i = RobotVar(r,0);
          if(ad1 < 0xFF00) r->var[ad1] = i;
          else // 2.0
          {
             if(ad1==0xFF0A) r->A = i;
             if(ad1==0xFF0B) r->B = i;
             if(ad1==0xFF0C) r->C = i;
          }
          break;
     case 0x21:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          r->var[k] = -i;
          break;
     case 0x22:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          r->var[k] = i+j;
          break;
     case 0x23:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          r->var[k] = i-j;
          break;
     case 0x24:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(j) r->var[k] = i/j;
          else  r->var[k] = 32767;
          break;
     case 0x25:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          r->var[k] = i*j;
          break;
     case 0x26:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(j) r->var[k] = i%j;
          else  r->var[k] = 0;
          break;
     case 0x27:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          r->var[k] = i&j;
          break;
     case 0x28:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          r->var[k] = i|j;
          break;
     case 0x29:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          r->var[k] = i^j;
          break;
     case 0x2A:
          k = RobotVar(r,1);
          i = RobotVar(r,0);
          r->var[k] = ~i;
          break;
     case 0x30:
          k = RobotAdr(r);
          r->P = k;
          break;
     case 0x31:
          k = RobotAdr(r);
          r->var[r->sp--] = r->P;
          if(r->sp < r->nvar) r->halt=1;
          r->P = k;
          break;
     case 0x33:
          if(r->sp<VARMAX-1) r->sp++;
          else r->halt=1;
          r->P = r->var[r->sp];
          break;
     case 0x34:
          i = r->xr;
          j = r->yr;
          r->N = 0;
          switch(r->angle)
          {
             case 0: i++; break;
             case 1: j--; break;
             case 2: i--; break;
             case 3: j++; break;
          }
          if(i<0||i>=r->dx){r->N=10;break;}
          if(j<0||j>=r->dy){r->N=10;break;}
          switch(MAPLO(i,j)>>4)
          {
             case 1:
                  r->command=0x400+r->number-1;
                  break;
             case 0:
             case 5:
             case 3:
                  MAP(r->xr,r->yr,0);
                  r->xr = i;
                  r->yr = j;
                  if((MAPLO(i,j)>>4)==3) r->M+=MISSLEADD;
                  MAP(r->xr,r->yr,0x60+((r->number-1)<<8));
                  break;
             case 2:
                  r->N = 2;
                  break;
             case 4:
                  r->N = 4;
                  break;
             case 6:
                  r->N = 6;
                  break;
          }
          break;
     case 0x35:
          r->angle++;
          if(r->angle>3) r->angle-=4;
          break;
     case 0x36:
          r->angle--;
          if(r->angle<0) r->angle+=4;
          break;
     case 0x37:
          i = 0;
          j = r->code[r->P++];
          while(j!=0)
          {  if(i<99) str[i++] = j;
             j = r->code[r->P++];
          }
          str[i] = 0;
          RobotPrintVar(r,str);
          po = str;
     //     if(last!=NULL) strcpy(last,po);
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tSAY <%s>\n",r->T,r->number,r->name,str);
          break;
     case 0x38:
          i = RobotVar(r,0);
          sprintf(str,"%i",i);
          po = str;
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tPRINT %s\n",r->T,r->number,r->name,str);
          break;
     case 0x3A:
          i = RobotVar(r,0);
          j = r->equip[i&3];
          k = r->angle-(i&3);
          if(k<0) k+=4;
          if(k>3) k-=4;
          if(j==1)
          {
             r->D = 0;
             xx = r->xr;
             yy = r->yr;
             r->command = 0x200+k;
             while(1)
             {   r->D++;r->K=0;
                 switch(k)
                 {
                    case 0: xx++; break;
                    case 1: yy--; break;
                    case 2: xx--; break;
                    case 3: yy++; break;
                 }
                 if(xx<0||xx>=r->dx||yy<0||yy>=r->dy){r->N=10;break;}
                 r->N = MAPLO(xx,yy)>>4;
                 if(r->N==6)
                 {
                    i = MAPHI(xx,yy);
                    r->K = ((Robot*)r->rar[i])->E;
                    if(r->f_gr)
                    {
                       if(!strcmp(r->author,RobotGetAuthor(((Robot*)r->rar[i])))) r->N=7;
                    }
                 }
                 if(r->N==2) r->K=MAPLO(xx,yy)&0xF;
                 if(r->N==5)
                 {  i=MAPLO(xx,yy)&0xF;
                    if(xx==r->xr)
                    {
                       if(yy>r->yr)
                       {
                          if(i==1) r->K=1;
                       }
                       else
                       {
                          if(i==3) r->K=1;
                       }
                    }
                    if(yy==r->yr)
                    {
                       if(xx>r->xr)
                       {
                          if(i==2) r->K=1;
                       }
                       else
                       {
                          if(i==0) r->K=1;
                       }
                    }
                 }
                 if(r->N>=1) break;
             }
          }
          if(j==2&&r->M>0)
          {
             r->M--;
             r->command = 0x100 + k;
          }
          break;
     case 0x3B:
          r->command = 0x300;
          break;
     case 0x3C:
          k2 = RobotVar(r,0);
          k3 = 0;
          if(r->f_gr&&k2==7)
          {  k2 = 6;
             k3 = 1;
          }
          k = r->angle;
          while(k<0) k+=4;
          while(k>3) k-=4;
          dd = r->dx+r->dy+2;
          r->E--;
          r->X = r->Y = 0;
          for(i=0;i<r->dx;i++){
          for(j=0;j<r->dy;j++){
             if(k2==(int)(MAPLO(i,j)>>4))
             {
                if(k2==6&&i==r->xr&&j==r->yr) continue;
                if(k3 && strcmp(r->author,RobotGetAuthor(((Robot*)r->rar[MAPHI(i,j)]))))
                   continue;
                xx=r->xr-i;if(xx<0)xx=-xx;
                yy=r->yr-j;if(yy<0)yy=-yy;
                if(xx+yy<dd)
                {
                   switch(k)
                   {
                     case 0: r->X=j-r->yr; r->Y=i-r->xr; break;
                     case 1: r->X=i-r->xr; r->Y=r->yr-j; break;
                     case 2: r->X=r->yr-j; r->Y=r->xr-i; break;
                     case 3: r->X=r->xr-i; r->Y=j-r->yr; break;
                   }
                   dd = xx+yy;
                }
             }
          }}
          xx = r->X;
          yy = r->Y;
          if(xx<0) xx=-xx;
          if(yy<0) yy=-yy;
          if(xx<yy) r->D=xx;
          else r->D=yy;
          if(r->X>=0&&r->Y>=0) r->K=0;
          if(r->X<0&&r->Y>=0) r->K=1;
          if(r->X<0&&r->Y<0) r->K=2;
          if(r->X>=0&&r->Y<0) r->K=3;
          break;
     case 0x3F:
          i = r->code[r->P++];
          j = r->code[r->P++];
          if(i!=0){r->halt=1;break;}
          sprintf(str,"DUMP %i",j);
          po = str;
          if(f!=NULL)
          {
             fprintf(f,"T=%u\tROBOT%u '%s':\n",r->T,r->number,r->name);
             for(i=0;i<r->nvar;i++)
             {
                if(i%j==0) fprintf(f," ");
                fprintf(f,"%i ",r->var[i]);
                if(i%j==j-1||i==r->nvar-1) fprintf(f,"\n");
             }
          }
          break;
     case 0x40:
          k = r->code[r->P++];
          for(i=0;i<k;i++)
              str2[i] = r->code[r->P++];
          RobotRegArr(r);
          j = evaluate((unsigned char*)str2,k,r->var,VARMAX,r->regs);
          if(j < 0) 
          {
#if 0
             WriteShell(">>> Evaluate Error ");
             WriteShellInt(j);
#endif
             r->halt=1;
          }
          RobotArrReg(r);
          break;
     case 0x41:
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(i) r->P=j;
          break;
     case 0x42:
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(!i) r->P=j;
          break;
     case 0x43:
          i = RobotVar(r,0);
          r->P = i;
          break;
     case 0x44:
          i = RobotVar(r,0);
          r->var[r->sp--] = r->P;
          if(r->sp<r->nvar) r->halt=1;
          r->P = i;
          break;
     case 0x51:
          k = RobotAdr(r);
          i = 0;
          j = r->code[r->P++];
          while(j!=0)
          {  if(i<99) str2[i++] = j;
             j = r->code[r->P++];
          }
          str2[i] = 0;
          RobotPrintVar(r,str2);
          i = strwcmp(r->last,str2);
          if(i) r->P=k;
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tTEST '%s'%c='%s'\n",
                                 r->T,r->number,r->name,str2,i?'=':'!',r->last);
          break;
     case 0x52:
          i = 0;
          j = r->code[r->P++];
          while(j!=0)
          {  if(i<99) str2[i++] = j;
             j = r->code[r->P++];
          }
          str2[i] = 0;
          RobotPrintVar(r,str2);
          r->L = strwcmp(r->last,str2);
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tTEST2 '%s'%c='%s'\n",
                                 r->T,r->number,r->name,str2,i?'=':'!',r->last);
          break;
     case 0x60:
          i = RobotVar(r,1);
          if(i<VARMAX) r->var[i++]=r->X; /* 0 */
          if(i<VARMAX) r->var[i++]=r->Y; /* 1 */
          if(i<VARMAX) r->var[i++]=r->D; /* 2 */
          if(i<VARMAX) r->var[i++]=r->N; /* 3 */
          if(i<VARMAX) r->var[i++]=r->K; /* 4 */
          if(i<VARMAX) r->var[i++]=r->R; /* 5 */
          if(i<VARMAX) r->var[i++]=r->T; /* 6 */
          if(i<VARMAX) r->var[i++]=r->E; /* 7 */
          if(i<VARMAX) r->var[i++]=r->M; /* 8 */
          if(i<VARMAX) r->var[i++]=r->I; /* 9 */
          if(i<VARMAX) r->var[i++]=r->A; /* 10 */
          if(i<VARMAX) r->var[i++]=r->B; /* 11 */
          if(i<VARMAX) r->var[i++]=r->C; /* 12 */
          if(i<VARMAX) r->var[i++]=r->P; /* 13 */
          if(i<VARMAX) r->var[i++]=r->L; /* 14 */
          if(i<VARMAX) r->var[i]=r->S;   /* 15 */
          /* TODO: Register H */
          break;
     case 0x61:
          r->send = RobotVar(r,0);
          k = RobotVar(r,0)&0xFF;
          r->command = 0x500 + k;
          r->E--;
          break;
     case 0x62:
          k2 = RobotVar(r,1);
          if(r->s_i==r->s_o)
          {  r->var[k2] = 0;
             r->N = r->X = r->Y = r->K = 0;
          }
          else
          {  r->var[k2] = r->s_k[r->s_o];
             r->N = r->s_n[r->s_o];
             r->K = r->s_t[r->s_o];
             i = r->s_x[r->s_o];
             j = r->s_y[r->s_o];
             k = r->angle;
             while(k<0) k+=4;
             while(k>3) k-=4;
             switch(k)
             {
               case 0: r->X=j-r->yr; r->Y=i-r->xr; break;
               case 1: r->X=i-r->xr; r->Y=r->yr-j; break;
               case 2: r->X=r->yr-j; r->Y=r->xr-i; break;
               case 3: r->X=r->xr-i; r->Y=j-r->yr; break;
             }
             r->s_o++;
             if(r->s_o>=SENDBUF) r->s_o=0;
          }
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tRECV %i %i (%i,%i) T=%i [%i]\n",
                                 r->T,r->number,r->name,r->var[k2],r->N,r->X,r->Y,r->K,k2);
          break;
     case 0x63:
          r->sp++;
          if(r->sp>=VARMAX) r->sp=VARMAX-1;
          break;
     case 0x64:
          xx = RobotVar(r,0);
          yy = RobotVar(r,0);
          /* TODO */
          tmp = xx+yy;
          break;
     case 0x65:
          xx = RobotVar(r,0);
          yy = RobotVar(r,0);
          /* TODO */
          tmp = xx+yy;
          break;
     case 0x66:
          i = 0;
          j = r->code[r->P++];
          while(j!=0)
          {  if(i<99) str2[i++] = j;
             j = r->code[r->P++];
          }
          str2[i] = 0;
          RobotPrintVar(r,str2);
          break;
     case 0x67:
          i = RobotVar(r,0);
          /* TODO */
          tmp = i;
          break;
     case 0x68:
          i = RobotVar(r,0);
          /* TODO */
          tmp = i; 
          break;
     case 0x69:
          i = RobotVar(r,0);
          j = RobotVar(r,0);
          if(r->Platform)
          {
#if 0
             WriteShell("SELECT\n");
             WriteShellInt(r->xr);
             WriteShellInt(r->yr);
#endif
             r->xr = i;
             r->yr = j;
          }
          break;
     case 0x6A:
          i = RobotVar(r,0);
          j = RobotVar(r,0)&0xFF;
          if(r->Platform)
          {
#if 0
             WriteShell("SET\n");
             WriteShellInt(i);
             WriteShellInt(j);
#endif
             r->command_i = i;
             r->command = 0x600+j;
          }
          break;
     case 0x70:
          i = RobotVar(r,1);
          j = RobotVar(r,1);
          /* TODO */
          tmp = i+j;
          break;
     case 0x71:
          i = RobotVar(r,1);
          j = RobotVar(r,0);
          /* TODO */
          tmp = i+j;
          break;
     case 0x72:
          i = RobotVar(r,1);
          /* TODO */
          tmp = i;
          break;
     default:
          r->halt=1;
   }
   r->S = r->sp - VARMAX + 1;
   if( lastcmd==0x00 ||
       lastcmd==0x01 ||
       lastcmd==0x20 ||
       lastcmd==0x30 ||
       lastcmd==0x31 ||
       lastcmd==0x33 ||
       (lastcmd>=0x41 && lastcmd<=0x44) ||
       lastcmd==0x63
     ){if(--nfree) goto freeloop;}
   r->T++;
   r->StepJob = 0;
   sprintf(str2,"%i",tmp); // trick to compile
   return po;
#else
   printf("RobotVar(%0x8.8X,%i)",r,f);
   return NULL;
#endif
}

void RobotSetImage(Robot *r, int f, void *pv)
{
   int i,j,k;
   char **im2 = (char**)pv;
   if(f==0||pv==NULL)
   { 
     r->fimage = 0;
     return;
   }
   if(f==1)
   { 
     char *im = (char*)pv;
     for(j=0;j<8;j++){
     for(i=0;i<8;i++){
        r->image[i][j] = *im;
        im++;
     }}
     r->fimage = 1;
     return;
   }
   if(f!=2) return;
   for(j=0;j<8;j++){
   for(i=0;i<8;i++){
      r->image[i][j] = im2[i][j];
   }}
   r->fimage = 1;
}

void RobotSetName(Robot *r, char *n)
{
   if(strlen(n)>VARLEN) Error2(3,n);
   strcpy(r->name,n);
}

void RobotSetAuthor(Robot *r, char *n)
{
   if(strlen(n)>VARLEN) Error2(3,n);
   strcpy(r->author,n);
}

short RobotSend(Robot *r,short k,short n,short t,short x,short y)
{
   r->s_k[r->s_i] = k;
   r->s_n[r->s_i] = n;
   r->s_t[r->s_i] = t;
   r->s_x[r->s_i] = x;
   r->s_y[r->s_i] = y;
   r->s_i++;
   if(r->s_i==SENDBUF) r->s_i=0;
   if(r->s_i==r->s_o)
   {  r->s_o++;
      if(r->s_o==SENDBUF) r->s_o=0;
      return 0;
   }
   return 1;
}

int RobotGetValue(Robot *r,int adr)
{
  int i = 0;
  if(adr>=0xFF00)
  {
     switch(adr)
     {
            case 0xFF00: i=r->X; break;
            case 0xFF01: i=r->Y; break;
            case 0xFF02: i=r->D; break;
            case 0xFF03: i=r->N; break;
            case 0xFF04: i=r->K; break;
            case 0xFF05: i=r->R; break;
            case 0xFF06: i=r->T; break;
            case 0xFF07: i=r->E; break;
            case 0xFF08: i=r->M; break;
            case 0xFF09: i=r->I; break;
            case 0xFF0A: i=r->A; break;
            case 0xFF0B: i=r->B; break;
            case 0xFF0C: i=r->C; break;
            case 0xFF0D: i=r->P; break;
            case 0xFF0E: i=r->L; break;
            case 0xFF0F: i=r->S; break;
     }
   }
   else
   {
     if(adr<VARMAX) i = r->var[adr];
   }
   return i;
}

int RobotPrintVar(Robot *r, char *s)
{
   int i,i1,i2,k = 0;
   char str[256],st[100],*po,*poo;
   while(1)
   {
     po = strchr(s,'&');
     if(po==NULL) break;;
     *po = 0;
     po++;
     strcpy(str,s);
     for(i=0;i<4;i++)
     {
        if(*po==0) return 0;
        st[i] = *po;
        po++;
     }
     st[i] = 0;
     i1 = hex2i(st);
     if(*po==',')
     {
        po++;
        for(i=0;i<4;i++)
        {
           if(*po==0) return 0;
           st[i] = *po;
           po++;
        }
        st[i] = 0;
        i2 = hex2i(st);
        i = 0;
        if(i1<0xFF00)
        {
           i = RobotGetValue(r, i1 + RobotGetValue(r,i2));
        }
        sprintf(st,"%i",i);
        strcat(str,st);
        strcat(str,po);
     }
     else
     {
        if(*po!=' ') return 0;
        i = RobotGetValue(r,i1);
        sprintf(st,"%i",i);
        strcat(str,st);
        strcat(str,po);
     }
     strcpy(s,str);
     k++;
   }
   return k;
}

void RobotRegArr(Robot *r)
{
   r->regs[0x0] = r->X;
   r->regs[0x1] = r->Y;
   r->regs[0x2] = r->D;
   r->regs[0x3] = r->N;
   r->regs[0x4] = r->K;
   r->regs[0x5] = r->R;
   r->regs[0x6] = r->T;
   r->regs[0x7] = r->E;
   r->regs[0x8] = r->M;
   r->regs[0x9] = r->I;
   r->regs[0xA] = r->A;
   r->regs[0xB] = r->B;
   r->regs[0xC] = r->C;
   r->regs[0xD] = r->P;
   r->regs[0xE] = r->L;
   r->regs[0xF] = r->S;
}

void RobotArrReg(Robot *r)
{
   r->X = r->regs[0x0];
   r->Y = r->regs[0x1];
   r->D = r->regs[0x2];
   r->N = r->regs[0x3];
   r->K = r->regs[0x4];
   r->R = r->regs[0x5];
   r->T = r->regs[0x6];
   r->E = r->regs[0x7];
   r->M = r->regs[0x8];
   r->I = r->regs[0x9];
   r->A = r->regs[0xA];
   r->B = r->regs[0xB];
   r->C = r->regs[0xC];
   r->P = r->regs[0xD];
   r->L = r->regs[0xE];
   r->S = r->regs[0xF];
}


// <><><><><><> RAND and MAP <><><><><><><>

unsigned short Rand = 14;

unsigned short MyRandom(unsigned short i)
{
  unsigned short tmp;
  Rand = (0x6255*Rand+0x3619)&0xFFFF;
  tmp = (Rand&0xFF00)|((Rand>>8)^(Rand&0xFF));
  if(i) tmp=(tmp&0x7FFF)%i;
  return tmp;
}

short _M_reaktor = 2;
short _M_yama    = 2;
short _M_yaschik = 5;
short _M_kamen   = 20;
short _X_box[DMBOX];
short _Y_box[DMBOX];
short _N_box = 0;

unsigned long* MakeMap(int n,int x,int y)
{
   int i,j,k,c,sz,dx,dy,n2;
   int reaktor,yama,yaschik,kamen;
   unsigned long *map;
   dx = x;
   dy = y;
   sz = dx*dy;
   n2 = n;
   if(n*100>sz) n2=sz/100;
//   printf("\n v2.0 MAP %dx%d %4.4X\n",dx,dy,Rand);
   reaktor = n2 * _M_reaktor;
   yama    = n2 * _M_yama;
   yaschik = n2 * _M_yaschik;
   kamen   = n2 * _M_kamen;
//   map = malloc(sz);
   map = (unsigned long*)malloc(sizeof(unsigned long)*sz);
   for(i=0;i<sz;i++) map[i]=0;
   if(!Rand) return map;
   i = 0;
   for(k=0;k<kamen;k++)
   {
      while(map[i]) i=MyRandom(sz);
      map[i] = 0x25;
   }
   for(k=0;k<yama;k++)
   {
      while(map[i]) i=MyRandom(sz);
      map[i] = 0x10;
   }
   for(k=0;k<yaschik;k++)
   {
      while(map[i]) i=MyRandom(sz);
      map[i] = 0x30;
      if(_N_box<DMBOX)
      {
         _X_box[_N_box]=i%dx;
         _Y_box[_N_box]=i/dx;
         _N_box++;
      }
   }
   for(k=0;k<reaktor;k++)
   {
      while(map[i]) i=MyRandom(sz);
      map[i] = 0x40;
   }
   for(k=0;k<n;k++)
   {
      while(map[i]) i=MyRandom(sz);
      map[i] = 0x60 + (k<<8);
   }

#if 0
   char str[256];
   str[0] = 0;
   k = 0;
   for(j=0;j<dy;j++){
       strcat(str,"|");
   for(i=0;i<dx;i++){
       c = map[k++]>>4;
       switch(c)
       {
          case 0: strcat(str," "); break;
          case 1: strcat(str,"O"); break;
          case 2: strcat(str,"@"); break;
          case 3: strcat(str,"#"); break;
          case 4: strcat(str,"*"); break;
          case 5: strcat(str,"+"); break;
          case 6: strcat(str,"R"); break;
          default: strcat(str,"?");
       }
   }strcat(str,"|\n");
   }
   MessageBox(NULL,str,"MAP",MB_OK);
#endif

   return map;
}
