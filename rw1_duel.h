#ifndef __RW1_DUEL_H
#define __RW1_DUEL_H

#define VERSION "2.2-alpha"

//#define FULLDEB
//#define MAPDRAW

extern int NUM;
extern int DDX,DDY;

#define MNUM     256
#define RW1SIZE  32
#define RW1SIZE2 16
#define SBIT     5
#define NOREG    2000
#define TCOL     0x303030
#define BCOL     0x010000
#define NPOV     4
#define FIRES    50
#define FIREF    23
#define SSIZE    8000
#define TIMEMAX  10000
#define TIMELIM  250
#define TIMEDM   1000
#define SNUM     3
#define DELA     56
#define ARGCMAX  256
#define STARTE   176
#define FILEOUT  "RW1_DUEL.OUT"
#define FILEIOUT "rw1_win.out"
#define FILEITMP "rw1_win.tmp"
#define MISSIZE  8
#define MISSIZE2 4

#define MY_PI 3.14159265358979323846

extern int f_f;
extern int f_d;
extern int f_v;
extern int f_s;
extern int NeedStop;
extern int SleepStep;
extern int JobStep;
extern int EndFight;
extern int selrobot[MNUM];
extern int myrobot[MNUM];
extern unsigned long *map;
extern int NUM;
extern int rtime;
extern char hostname[32];
extern int MyReg;
extern char path[144];
extern char cPath[256];

#ifdef __cplusplus
extern "C" {
#endif
int rw1_init(int argc,char **argv);
int rw1_draw(void);
unsigned long rw1_step(void* param);
extern void RShow(short k);
extern void RSend(int x, int y);
extern void RSelect(int x1, int y1, int x2, int y2);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
