/* my_text.c - Alexander A. Shabarshin <ashabarshin@gmail.com>

   RELEASED AS PUBLIC DOMAIN, USE ON YOUR OWN RISK!!!

   29 Nov 1996 - First C++ version (http://home.ural.ru/~shabun/public/TEXT.ZIP)
   31 Oct 2000 - Modified for ANSI-C (includable file text.hpp without classes)
   28 Nov 2005 - Divided to my_text.h and my_text.c files (with fixes and comments)
   14 Feb 2013 - Added some old functions for string manipulations
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_text.h"

/* <><><><><><><><><><> Functions for Line object <><><><><><><><><><> */

/* Constructor from string. Return pointer to new Line object. */
Line* LineNew(char *s)
{
   Line* l;
   char ss[1];
   ss[0]=0;
   if(s==NULL) s=ss;
   l = (Line*)malloc(sizeof(Line));
   if(l==NULL) return NULL;
   l->str=(char*)malloc(strlen(s)+1);
   if(l->str==NULL) TextError(TextErrMem);
   strcpy(l->str,s);
   l->next=NULL;
   l->type=l->id=l->id2=0;
   l->adr=l->len=0;
   return l;
}

/* Destructor. Delete Line object by pointer. */
void LineDel(Line *l)
{
   if(l==NULL) return;
   if(l->str!=NULL) free(l->str);
   free(l);
}

/* Get string from Line object. */
char* LineGetStr(Line *l)
{
   if(l==NULL) return NULL;
   return l->str;
}

/* Set type field of Line object. */
void LineSetTyp(Line *l, int t)
{
   if(l==NULL) return;
   l->type = t;
}

/* Get type field of Line object. */
int LineGetTyp(Line *l)
{
   if(l==NULL) return 0;
   return l->type;
}

/* Set 1st identifier of Line object. */
void LineSetId1(Line *l, int i)
{
   if(l==NULL) return;
   l->id = i;
}

/* Get 1st identifier of Line object. */
int LineGetId1(Line *l)
{
   if(l==NULL) return 0;
   return l->id;
}

/* Set 2nd identifier of Line object. */
void LineSetId2(Line *l, int i)
{
   if(l==NULL) return;
   l->id2 = i;
}

/* Get 2nd identifier of Line object. */
int LineGetId2(Line *l)
{
   if(l==NULL) return 0;
   return l->id2;
}

/* Set adr field of Line object. */
void LineSetAdr(Line *l, unsigned short u)
{
   if(l==NULL) return;
   l->adr = u;
}

/* Get adr field of Line object. */
unsigned short LineGetAdr(Line *l)
{
   if(l==NULL) return 0;
   return l->adr;
}

/* Set len field of Line object. */
void LineSetLen(Line *l, unsigned short u)
{
   if(l==NULL) return;
   l->len = u;
}

/* Get len field of Line object. */
unsigned short LineGetLen(Line *l)
{
   if(l==NULL) return 0;
   return l->len;
}


/* <><><><><><><><><><> Functions for Text object <><><><><><><><><><> */

/* Constructor without parameters. Return pointer to new Text object. */
Text* TextNew(void)
{
   Text* t;
   t = (Text*)malloc(sizeof(Text));
   if(t==NULL) TextError(TextErrMem);
   t->first=NULL;
   t->last=NULL;
   t->find=NULL;
   t->num=0L;   
   return t;
}

/* Destructor. Delete Text object by pointer. */
void TextDel(Text *t)
{
   if(t==NULL) return;
   TextAllDelete(t);
   free(t);
}

/* Write error with specified code to stdout and exit program. */
void TextError(TextErr err)
{
   char s[32];
   switch(err)
   { 
     case TextErrMem: strcpy(s,"Out of Memory"); break;
     case TextErrIdx: strcpy(s,"Index Error");   break;
     case TextErrFil: strcpy(s,"File Error");    break;
     case TextErrFld: strcpy(s,"Wrong Field");   break;
     default: strcpy(s,"Unknown Error");
   }
   printf("\n\nTextError %u: %s !!!\n\n",(int)err,s);
   exit((int)err); 
}

/* Return number of lines in Text object. */
long TextGetNumber(Text *t)
{
   if(t==NULL) return -1;
   return t->num;
}

/* Add string as line to end of text. Return pointer to new Line object. */
Line* TextAdd(Text *t, char *s)
{
   Line *l;
   if(t==NULL||s==NULL) return NULL;
   l=LineNew(s);
   if(l==NULL) TextError(TextErrMem);
   if(t->num) t->last->next=l;
   else t->first=l;
   t->last=l;
   t->num++;
   return l;
}

/* Insert string as line to be with specified index. Return pointer to new Line object. */
Line* TextInsert(Text *t, long n, char *s)
{
   long i;
   Line *l,*lp;
   if(t==NULL||s==NULL) return NULL;
   if(n<=0||n>t->num) TextError(TextErrIdx);
   if(n==t->num) l=TextAdd(t,s);
   else
   {  if(n==0L)
      {  l=t->first;
         t->first=LineNew(s);
         if(t->first==NULL) TextError(TextErrMem);
         t->first->next=l;
         l=t->first;
      }
      else
      {  lp=t->first;
         for(i=0L;i<n-1;i++) lp=lp->next;
         l=lp->next;
         lp->next=LineNew(s);
         if(lp->next==NULL) TextError(TextErrMem);
         ((Line*)lp->next)->next=(void*)l;
         l=lp->next;
      }
      t->num++;
   }
   return l;
}

/* Delete line with specified index. */
void TextDelete(Text *t, long n)
{
   long i;
   Line *l,*lp;
   if(t==NULL) return;
   if(n<0||n>=t->num) TextError(TextErrIdx);
   if(n)
   {  lp=t->first;
      for(i=0L;i<n-1;i++) lp=lp->next;
      l=lp->next;
      if(n==t->num-1) t->last=lp;
      lp->next=l->next;
   }
   else
   {  l=t->first;
      t->first=l->next;
   }
   free(l);
   t->num--;
}

/* Delete all lines in Text object. */
void TextAllDelete(Text *t)
{
   Line *l,*l2;
   if(t==NULL) return;
   l=t->first;
   while(l!=NULL)
   { l2=l->next;
     free(l);
     l=l2;
     t->num--;
   }
   t->first=NULL;
   t->last=NULL;
   t->find=NULL;
}

/* Return pointer to Line object by index (0 for first). */
Line* TextGet(Text *t, long n)
{
   long i;
   Line *l;
   if(t==NULL) return NULL;
   if(n<0||n>=t->num) TextError(TextErrIdx);
   l=t->first;
   for(i=0L;i<n;i++) l=l->next;
   return l;
}

/* Return index of Line object (0 for first, -1 if not found). */
long TextIndex(Text *t, Line *l)
{
   long i;
   Line *l2;
   if(t==NULL) return -1;
   i=0;
   l2=t->first;
   while(l2!=l)
   {  i++;
      if(l2==NULL) return -1;
      l2=l2->next;
   }
   return i;
}

/* Swap Line object and next Line object. Return 2 if first/last, 1 if not, 0 if error. */
int TextReplace(Text *t, Line *l1)
{
   Line *l,*l2;
   if(t==NULL) return 0;
   l2=l1->next;
   if(l1==t->first&&l2!=t->last)
   {  t->first=l2;
      l1->next=l2->next;
      l2->next=l1;
      return 2;
   }
   if(l1!=t->first&&l2==t->last)
   {  l=t->first;
      while(l->next!=l1) l=l->next;
      l->next=l2;
      l1->next=NULL;
      l2->next=l1;
      t->last=l1;
      return 2;
   }
   if(l1==t->first&&l2==t->last)
   {  t->first=l2;
      l1->next=NULL;
      l2->next=l1;
      t->last=l1;
      return 2;
   }
   l=t->first;
   while(l->next!=l1) 
   {  l=l->next;
      if(l==NULL) return 0;
   }
   l->next=l2;
   l1->next=l2->next;
   l2->next=l1;
   return 1;
}

/* Slow sorting for Text object by field identifier. Return 1 if Ok or 0 if no lines. */
int TextSort(Text *t, TextFld field)
{  int ff;
   Line *l1;
   Line *l2;
   if(t==NULL) return 0;
   ff=1;
   if(t->num<1) return 0;
   switch(field)
   {
      case TextFldNul:
              break;
      case TextFldStr:
              while(ff)
              {  ff=0;
                 l1=t->first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(strcmp(l1->str,l2->str)>0)
                    {  ff=1;
                       TextReplace(t,l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case TextFldTyp:
              while(ff)
              {  ff=0;
                 l1=t->first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->type > l2->type)
                    {  ff=1;
                       TextReplace(t,l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case TextFldId1:
              while(ff)
              {  ff=0;
                 l1=t->first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->id > l2->id)
                    {  ff=1;
                       TextReplace(t,l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case TextFldId2:
              while(ff)
              {  ff=0;
                 l1=t->first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->id2 > l2->id2)
                    {  ff=1;
                       TextReplace(t,l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case TextFldAdr:
              while(ff)
              {  ff=0;
                 l1=t->first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->adr > l2->adr)
                    {  ff=1;
                       TextReplace(t,l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case TextFldLen:
              while(ff)
              {  ff=0;
                 l1=t->first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->len > l2->len)
                    {  ff=1;
                       TextReplace(t,l1);
                    }
                    l1=l2;
                 }
              }
              break;
      default: TextError(TextErrFld);
   }
   return 1;
}

/* Save Text object to text file. */
void TextSave(Text *t, char *filename)
{
   Line *l;
   FILE *f;
   if(t==NULL) return;
   f=fopen(filename,"wt");
   if(f==NULL) TextError(TextErrFil);
   l=t->first;
   while(l!=NULL)
   {  fputs(l->str,f);
      fputc('\n',f);
      l=l->next;
   }
   fclose(f);
}

/* Load Text object from text file. */
void TextLoad(Text *t, char *filename)
{
   int i;
   char c,*buf,*po;
   FILE *f;
   if(t==NULL) return;
   f=fopen(filename,"rt");
   if(f==NULL) TextError(TextErrFil);
   buf=(char*)malloc(MY_MAXLEN);
   if(buf==NULL) TextError(TextErrMem);
   TextAllDelete(t);
   t->num=0L;
   while(!feof(f))
   {  i=0;
      c=0;
      while(!feof(f)&&c!='\n'&&i<MY_MAXLEN-1)
      {  c=(char)fgetc(f);
         if(c=='\n'||feof(f)) buf[i++]=0;
         else buf[i++]=c;
      }
      if(i==MY_MAXLEN-1) buf[i]=0;
      po = strrchr(buf,'\r');
      if(po!=NULL) *po=0;
      TextAdd(t,buf);
   }
   fclose(f);
   free(buf);
}

/* Write list of lines to stdout */
void TextList(Text *t)
{  Line *l;
   if(t==NULL) return;
   for(l=t->first;l!=NULL;l=l->next)
    printf("%i\t%i\t%i\t#%4.4X\t%u\t%s\n", 
     l->type,l->id,l->id2,l->adr,l->len,l->str);
}

/* Search Line object by string. Return pointer to first one or NULL. */
Line* TextFindFirstStr(Text *t, char *s)
{
   if(t==NULL||s==NULL) return NULL;
   t->find_fld=TextFldStr;
   t->find_fld2=TextFldNul;
   t->find_str=s;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by type. Return pointer to first one or NULL. */
Line* TextFindFirstTyp(Text *t, int y)
{
   if(t==NULL) return NULL;
   t->find_fld=TextFldTyp;
   t->find_fld2=TextFldNul;
   t->find_type=y;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by 1st identifier. Return pointer to first one or NULL. */
Line* TextFindFirstId1(Text *t, int i)
{
   if(t==NULL) return NULL;
   t->find_fld=TextFldId1;
   t->find_fld2=TextFldNul;
   t->find_id=i;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by 2nd identifier. Return pointer to first one or NULL. */
Line* TextFindFirstId2(Text *t, int i)
{
   if(t==NULL) return NULL;
   t->find_fld=TextFldId2;
   t->find_fld2=TextFldNul;
   t->find_id2=i;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by adr. Return pointer to first one or NULL. */
Line* TextFindFirstAdr(Text *t, unsigned short u)
{
   if(t==NULL) return NULL;
   t->find_fld=TextFldAdr;
   t->find_fld2=TextFldNul;
   t->find_adr=u;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by len. Return pointer to first one or NULL. */
Line* TextFindFirstLen(Text *t, unsigned short u)
{
   if(t==NULL) return NULL;
   t->find_fld=TextFldLen;
   t->find_fld2=TextFldNul;
   t->find_len=u;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by string and type. Return pointer to first one or NULL. */
Line* TextFindFirstStrTyp(Text *t, char *s, int y)
{
   if(t==NULL||s==NULL) return NULL;
   t->find_fld=TextFldStr;
   t->find_fld2=TextFldTyp;
   t->find_str=s;
   t->find_type=y;
   t->find=t->first;
   return TextFindNext(t);
}

/* Search Line object by type and id. Return pointer to first one or NULL. */
Line* TextFindFirstTypId(Text *t, int y, int i)
{
   if(t==NULL) return NULL;
   t->find_fld=TextFldTyp;
   t->find_fld2=TextFldId1;
   t->find_type=y;
   t->find_id=i;
   t->find=t->first;
   return TextFindNext(t);
}

/* Return pointer to next Line object or NULL if no more search results. */
Line* TextFindNext(Text *t)
{
   int res;
   Line *l,*ll;
   if(t==NULL) return NULL;
   ll = NULL;
   l = t->find;
   while(l!=NULL)
   {
      res = 0;
      switch(t->find_fld)
      {
         case TextFldNul: res++; break;
         case TextFldStr: if(!strcmp(l->str,t->find_str)) res++; break;
         case TextFldTyp: if(l->type==t->find_type) res++; break;
         case TextFldId1: if(l->id==t->find_id) res++; break;
         case TextFldId2: if(l->id2==t->find_id2) res++; break;
         case TextFldAdr: if(l->adr==t->find_adr) res++; break;
         case TextFldLen: if(l->len==t->find_len) res++; break;
      }
      switch(t->find_fld2)
      {
         case TextFldNul: res++; break;
         case TextFldStr: if(!strcmp(l->str,t->find_str)) res++; break;
         case TextFldTyp: if(l->type==t->find_type) res++; break;
         case TextFldId1: if(l->id==t->find_id) res++; break;
         case TextFldId2: if(l->id2==t->find_id2) res++; break;
         case TextFldAdr: if(l->adr==t->find_adr) res++; break;
         case TextFldLen: if(l->len==t->find_len) res++; break;
      }
      if(res==2) ll=l;
      l = l->next;
      if(res==2) break;
   }
   t->find = l;
   return ll;
}

/* <><><><><><><><><><> Other String Manipulation Functions <><><><><><><><><><> */

int wild_cmp(char *str, char *wld)
{
  int i,j,k;
  char *ps,*pw;
#if 0
  static int wild_n = 0;
  printf("\n%u)\t'%s'\n\t'%s'\n",++wild_n,str,wld);
#endif
  if(*str==0&&*wld==0) return 1;
  ps = &str[1];
  pw = &wld[1];
  if(*str==*wld) return wild_cmp(ps,pw);
  if(*wld=='?') return wild_cmp(ps,pw);
  if(*wld=='*')
  {
     if(*pw==0) return 1;
     while(*pw!=*ps)
     {
        laba:
        if(*ps++==0) return 0;
     }
     if(wild_cmp(ps,pw)) return 1;
     else goto laba;
  }
  return 0;
}

/* Compare string with wildcard that may have '?' for one and '*' for many characters */
int strwcmp(char *str, char *wld)
{
  int i,j,k;
  k =0;
  for(i=0;i<(int)strlen(wld);i++)
  {
    if(wld[i]=='*') k++;
  }
  if((int)strlen(str)<i-k) return 0;
  return wild_cmp(str,wld);
}

/* Delete spaces from string and return new length of the string */
int strdels(char *s)
{
  int i,j=0;
  for(i=0;i<(int)strlen(s);i++)
  {
     if(s[i]!=' '&&s[i]!='\t')
     {
        s[j] = s[i];
        j++;
     }
  }
  s[j] = 0;
  return j;
}

/* Skip spaces and return pointer to next character that is not space */
char* strskips(char* p)
{
  while(*p==' '||*p=='\t') p++;
  return p;
}

/* Convert hexadecimal digit to integer */
int hexx(char c)
{  
  int i = -1;
  switch(c)
  {
    case '0': i=0;break;
    case '1': i=1;break;
    case '2': i=2;break;
    case '3': i=3;break;
    case '4': i=4;break;
    case '5': i=5;break;
    case '6': i=6;break;
    case '7': i=7;break;
    case '8': i=8;break;
    case '9': i=9;break;
    case 'a': i=10;break;
    case 'b': i=11;break;
    case 'c': i=12;break;
    case 'd': i=13;break;
    case 'e': i=14;break;
    case 'f': i=15;break;
    case 'A': i=10;break;
    case 'B': i=11;break;
    case 'C': i=12;break;
    case 'D': i=13;break;
    case 'E': i=14;break;
    case 'F': i=15;break;
  }
  return i;
}

/* Convert string with hexadecimal number to integer */
int hex2i(char *s)
{
  char *ptr;
  long l = strtol(s,&ptr,16);
  if(*ptr) return -1;
  return (int)l;
}
