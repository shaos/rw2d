rw2d
====

Game for programmers "Robot Warfare 2D" is based on source code of shareware game "Robot Warfare 1 for Windows"

Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

Released under MIT license (see file LICENSE) in 2013

https://gitlab.com/shaos/rw2d

Old source code for DOS could be found here:

https://gitlab.com/shaos/rw1o
