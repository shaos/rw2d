#ifndef _ROBOTWAR_H
#define _ROBOTWAR_H
#include "my_text.h"

#define VARMAX  32512
#define MAXLINE 1000
#define STRLEN  32000
#define SENDBUF 100
#define DMBOX   100
#define NFREE   10

#define   MISSLEBEG  5
#define   MISSLEADD  10
#define   MISSLESPE  5
#define   ENERGYBEG  5
#define   ENERGWAIT  5
#define   ENERGYMAX  13

#define EYE 1
#define GUN 2

#if 0
#define MAP(x,y,z)  DebugMAP(x,y,z)
#else
#define MAP(x,y,z)  r->map[(DDX*(y))+(x)]=z;
#endif
#define MAP_(x,y,z)  map[(DDX*(y))+(x)]=z;
#define MAPLO(x,y) (r->map[(DDX*(y))+(x)]&0xFF)
#define MAPLO_(x,y) (map[(DDX*(y))+(x)]&0xFF)
#define MAPHI(x,y) (r->map[(DDX*(y))+(x)]>>8)
#define MAPHI_(x,y) (map[(DDX*(y))+(x)]>>8)

#define VARLEN 32

struct DebugFile
{
  char name[VARLEN];
  struct DebugFile *next;
};

struct DebugVar
{
  char name[VARLEN];
  int size,addr;
  struct DebugVar *next;
};

struct DebugLine
{
  int id,line,addr;
  struct DebugLine *next;
};

typedef struct _Robot
{
  short dx,dy,xr,yr,version;
  unsigned long *map;
  short cur,nvar,number,elec,magic;
  unsigned char *code;
  short *var;
  char name[VARLEN];
  char author[VARLEN];
  char str0[100];
  void **rar; /* all robots */
  short nrar;
  short f_gr;
  char *last;
  long color;
  short regs[16];
  unsigned char image[8][8];
  unsigned char equip[4];
  short X,Y,D,N,K,R,T,E,M,I,A,B,C,P,L,S,H;
  short command,angle,halt,command_i;
  short fimage,sp,send;
  short s_i,s_o;
  short s_k[SENDBUF];
  short s_n[SENDBUF];
  short s_t[SENDBUF];
  short s_x[SENDBUF];
  short s_y[SENDBUF];
  int StepJob;
  int Platform;
  struct DebugFile *d_file;
  struct DebugVar  *d_vars;
  struct DebugLine *d_line;
} Robot;

extern unsigned short Rand;
  
extern short _X_box[DMBOX];
extern short _Y_box[DMBOX];
extern short _N_box;

extern int DDX;
extern int DDY;

#ifdef __cplusplus
extern "C" {
#endif

  Robot* RobotNew(short num, char *s);
  void RobotDel(Robot *r);
  void RobotCompile(Robot *r, Text *t);
  short RobotVar(Robot *r, short f);
  short RobotAdr(Robot *r);
  char* RobotStep(Robot *r, FILE *f);
  void RobotSave(Robot *r, char *fname);
  short RobotSend(Robot *r, short k, short n, short t, short x, short y);
  int RobotPrintVar(Robot *r, char *s);
  int RobotGetValue(Robot *r, int adr);
  void RobotRegArr(Robot *r);
  void RobotArrReg(Robot *r);
  void RobotSetImage(Robot *r, int f, void *pv);
  void RobotSetName(Robot *r, char *n);
  void RobotSetAuthor(Robot *r, char *n);

  unsigned short MyRandom(unsigned short i);
  unsigned long* MakeMap(int n,int x,int y);
  void DebugMAP(int x,int y,int z);

#ifdef __cplusplus
} /* extern "C" */
#endif

#define RobotSetMap(r,x,y,m) r->dx=x;r->dy=y;r->map=m;
#define RobotSetLoc(r,x,y) r->xr=x;r->yr=y;
#define RobotSetX(r,x) r->xr=x;
#define RobotSetY(r,y) r->yr=y;
#define RobotGetX(r) r->xr
#define RobotGetY(r) r->yr
#define RobotGetI(r,i,j) r->image[i][j]
#define RobotGetR(r) ((r->color>>16)&0xFF)
#define RobotGetG(r) ((r->color>>8)&0xFF)
#define RobotGetB(r) ((r->color)&0xFF)
#define RobotGetE(r,i) r->equip[i]
#define RobotGetCode(r,a) r->code[a]
#define RobotGetNumber(r) r->number
#define RobotGetName(r) r->name
#define RobotGetAuthor(r) r->author
#define RobotGetLCod(r) r->cur
#define RobotGetLVar(r) r->nvar
#define RobotGetVersion(r) r->version
#define RobotDamage(r) (--(r->E))
#define RobotSetVar(r,v) r->nvar=v;
#define RobotSetCode(r,l,c) r->cur=l;r->code=c;
#define RobotSetColor(r,k,z,s) r->color=((((long)k)<<16)|(z<<8)|s);
#define RobotSetEquip(r,a,b,c,d) r->equip[0]=a;r->equip[1]=b;r->equip[2]=c;r->equip[3]=d;
#define RobotSetLast(r,l) r->last=l;
#define RobotSetRar(r,a,n,g) r->rar=(void**)a;r->nrar=n;r->f_gr=g;

#endif
