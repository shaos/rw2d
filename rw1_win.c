/*
 Robot Warfare 2D
 ================

 Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/* RW1_WIN.CPP - Shabarshin A.A. 22.07.1999 */

#include <windows.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <direct.h>
#include "rw1_win.h"
#include "rw1_duel.h"

#define TIMER_ID   111
#define TIMER_MS   36
#define SCROLLSTEP 16

#define WM_MYREG (WM_USER+100)
#define WM_MYCOM (WM_USER+101)

LRESULT CALLBACK WindowFunc (HWND, UINT, WPARAM, LPARAM);

char szWinName[] = "RW1 Arena v" VERSION;
char lpszArgs_[256];
HINSTANCE hThisInst;
HWND hMainWnd,hwpar,hwvar;
HBITMAP hbPile[5],hbEmpty,hbStone[5],hbReactor,hbHole,hbFire,hbBall;
HDC     hdPile[5],hdEmpty,hdStone[5],hdReactor,hdHole,hdFire,hdBall;
HBITMAP hBit;
HBRUSH hBrush;
HDC memDC;
int maxX,maxY;
int winX,winY;
int begX,begY;
int scrX,scrY;
int globTick = 0;
int fReg = 0;
int nReg = 0;
DWORD rw1_step_id;
int TimerJob = 0;
int fEmpty = 0;
int fHidden = 0;

char Options[] =
"@listfile - file with list of options\n"
"name.rw0 - name of the robot to load\n"
"-mN - number of robots N\n"
"-nN - identifier of the map to use (if 0 then empty)\n"
"-gN - fast run N ticks\n"
"-gr - group mode\n"
"-i  - generate information at the end\n"
"-d  - debug mode with writing trace to RW1_WIN.OUT\n"
"-d2 - debug mode with steps\n"
"-d3 - debug mode with substeps\n"
"-dm - deathmatch mode with reappearence of boxes and reactors every 1000 ticks\n"
"-f  - fast battle\n"
"-f2 - superfast battle without visualization\n"
"-E/M - initial energy and missles for last robot (5/5 by default)\n"
"-sN - select robot with identifier N (if 0 then select all)\n"
"-hNAME - name of the author to control his/her robots\n"
"-aX,Y,N[,K] - put object N with optional param K to the cell (X,Y)\n"
"-dxN - set window width to N cells (20 by default)\n"
"-dyN - sey window height to N cells (10 by default)\n"
"-v - variables watch window\n"
"-o - output to the console without additional info\n"
;

char path[144];
char cPath[256];

void SetPathes(void)
{
 HKEY hkey;
 DWORD dw;
 getcwd(cPath,255);
 if(ERROR_SUCCESS !=
  RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\SHAOS\\RW1", 0, KEY_READ, &hkey))
   strcpy(path,cPath);
 else {
  dw = 255;
  if(ERROR_SUCCESS !=
   RegQueryValueEx(hkey, "Path", NULL, NULL, (unsigned char*)path, &dw))
    strcpy(path,cPath);
  RegCloseKey(hkey);
 }
// MessageBox(NULL,path,"Path",MB_OK);
}

#define MAXSPR 256
HBITMAP sprBitmap[MAXSPR];
HDC sprDC[MAXSPR];
int sprIndex[MAXSPR];
int sprNum = 0;
int sprStp = 9;
int sprBeg = 0x1000;

#define CELLSIZE  32

#define MAX_COLO 255

long GetResizePixel(char *img,short ddx,short ddy,double fi,double fj)
{
   int i,j,k,i0,j0,i1,j1,r,g,b;
   long lk;
   int colo[4];
   int colr[4];
   int colg[4];
   int colb[4];
   double di,dj,mi,mj;
   double d1,d2,dd[4],ddd;
   double dr,dg,db;
   di = (ddx-1)*fi;
   dj = (ddy-1)*fj;
   if(di<0) di=0;
   if(di>=ddx) di=ddx-1;
   if(dj<0) dj=0;
   if(dj>=ddx) dj=ddy-1;
   i0 = di;
   j0 = dj;
   i1 = i0;
   j1 = j0;
   mi = di-i0;
   mj = dj-j0;
   if(mi>0) i1++;
   if(mj>0) j1++;
   if(i0>=ddx) i0=ddx-1;
   if(i1>=ddx) i1=ddx-1;
   if(j0>=ddy) j0=ddy-1;
   if(j1>=ddy) j1=ddy-1;
   colo[0] = img[i0+j0*ddx];
   colo[1] = img[i1+j0*ddx];
   colo[2] = img[i0+j1*ddx];
   colo[3] = img[i1+j1*ddx];
#if 0
   printf("%2.2X %2.2X %2.2X %2.2X\n",colo[0],colo[1],colo[2],colo[3]);
#endif
   for(i=0;i<4;i++)
   {
      k = colo[i]&7;
#if 0
      j = (colo[i]&8)>>3;
#endif
      colr[i] = ((k&4)>>2)*MAX_COLO;
      colg[i] = ((k&2)>>1)*MAX_COLO;
      colb[i] = ((k&1))*MAX_COLO;
      if(!k) colr[i]<<=1;
      if(!k) colg[i]<<=1;
      if(!k) colb[i]<<=1;
   }
   d1 = di-i0;
   d2 = dj-j0;
   dd[0] = sqrt(d1*d1+d2*d2);
   d1 = di-i1;
   d2 = dj-j0;
   dd[1] = sqrt(d1*d1+d2*d2);
   d1 = di-i0;
   d2 = dj-j1;
   dd[2] = sqrt(d1*d1+d2*d2);
   d1 = di-i1;
   d2 = dj-j1;
   dd[3] = sqrt(d1*d1+d2*d2);
   for(i=0;i<4;i++) dd[i] = 1-dd[i];
   ddd = dd[0]+dd[1]+dd[2]+dd[3];
   for(i=0;i<4;i++) dd[i] = dd[i]/ddd;
   dr = 0;
   dg = 0;
   db = 0;
   for(i=0;i<4;i++)
   {
      dr += colr[i]*dd[i];
      dg += colg[i]*dd[i];
      db += colb[i]*dd[i];
   }
   r = dr;
   g = dg;
   b = db;
   if(r<0) r=0;
   if(g<0) g=0;
   if(b<0) b=0;
   if(r>MAX_COLO) r=MAX_COLO;
   if(g>MAX_COLO) g=MAX_COLO;
   if(b>MAX_COLO) b=MAX_COLO;
   lk = (r|(g<<8)|(b<<16));
   return lk;
}

int CreateSprite(char *image,int index)
{
  int i,j,k;
  double dcol,drow;
  long *bigimage;
  bigimage = (long*)malloc(sizeof(long)*CELLSIZE*CELLSIZE);
  if(bigimage==NULL) return 0;
  dcol = drow = CELLSIZE;
  k = 0;
  for(j=0;j<CELLSIZE;j++){
  for(i=0;i<CELLSIZE;i++){
     bigimage[k++] = GetResizePixel(image,8,8,i/dcol,j/drow);
  }}
  sprBitmap[index] = CreateCompatibleBitmap(memDC,CELLSIZE,CELLSIZE);
  sprDC[index] = CreateCompatibleDC(memDC);
  SelectObject(sprDC[index],sprBitmap[index]);
  k = 0;
  for(j=0;j<CELLSIZE;j++){
  for(i=0;i<CELLSIZE;i++){
     SetPixel(sprDC[index],i,j,bigimage[k++]);
  }}
  free(bigimage);
  return index;
}

HANDLE hStdOut;

HWND CreateShell (LPCSTR szTitle)
{
   if(fHidden) return 0;
   FreeConsole();
   AllocConsole();
   SetConsoleTitle(szTitle);
   hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
   return NULL;
}

int WriteShell(char* str)
{
   DWORD dwBuf;
   if(fHidden) return 0;
   return WriteConsole(hStdOut,str,lstrlen(str),&dwBuf,NULL);
}

int WriteShellInt(int i)
{
   char str[32];
   if(fHidden) return 0;
   sprintf(str,"%i (%4.4X) %c\n",i,i,(i>0x20&&i<0xFF)?i:' ');
   return WriteShell(str);
}

int WriteFatal(char* str)
{
   MessageBox(0,str,"Fatal Error",MB_OK|MB_ICONEXCLAMATION);
   return 1;
}

int WritePixel(int x, int y, long c)
{
   SetPixel(memDC,x,y,c);
   return 1;
}

int WriteSprite(char* image)
{
   CreateSprite(image,sprNum);
   sprIndex[sprNum] = sprBeg + sprNum*sprStp;
   return sprNum++;
}

int WriteTile(int x, int y, int t, int s)
{
   int nn,r = 1;
   if(t<0)
   {
     nn = ((-t)&(sprBeg-1))/sprStp;
//     WriteShell("SET ");
//     WriteShellInt(nn);
     if(nn>=0&&nn<sprNum)
        BitBlt(memDC,x,y,s,s,sprDC[nn],0,0,SRCCOPY);
     return r;
   }
   switch(t)
   {
     case TILE_PILE1:
       BitBlt(memDC,x,y,s,s,hdPile[0],0,0,SRCCOPY);
       break;
     case TILE_PILE2:
       BitBlt(memDC,x,y,s,s,hdPile[1],0,0,SRCCOPY);
       break;
     case TILE_PILE3:
       BitBlt(memDC,x,y,s,s,hdPile[2],0,0,SRCCOPY);
       break;
     case TILE_PILE4:
       BitBlt(memDC,x,y,s,s,hdPile[3],0,0,SRCCOPY);
       break;
     case TILE_PILE5:
       BitBlt(memDC,x,y,s,s,hdPile[4],0,0,SRCCOPY);
       break;
     case TILE_EMPTY:
       BitBlt(memDC,x,y,s,s,hdEmpty,0,0,SRCCOPY);
       break;
     case TILE_STONE1:
       BitBlt(memDC,x,y,s,s,hdStone[0],0,0,SRCCOPY);
       break;
     case TILE_STONE2:
       BitBlt(memDC,x,y,s,s,hdStone[1],0,0,SRCCOPY);
       break;
     case TILE_STONE3:
       BitBlt(memDC,x,y,s,s,hdStone[2],0,0,SRCCOPY);
       break;
     case TILE_STONE4:
       BitBlt(memDC,x,y,s,s,hdStone[3],0,0,SRCCOPY);
       break;
     case TILE_STONE5:
       BitBlt(memDC,x,y,s,s,hdStone[4],0,0,SRCCOPY);
       break;
     case TILE_REACTOR:
       BitBlt(memDC,x,y,s,s,hdReactor,0,0,SRCCOPY);
       break;
     case TILE_HOLE:
       BitBlt(memDC,x,y,s,s,hdHole,0,0,SRCCOPY);
       break;
     case TILE_FIRE:
       BitBlt(memDC,x,y,s,s,hdFire,0,0,SRCCOPY);
       break;
     case TILE_BALL:
       BitBlt(memDC,x,y,s,s,hdBall,0,0,SRCCOPY);
       break;
     default:
       r = 0;
   }
   return r;
}

int WINAPI WinMain ( HINSTANCE hThisIn,
                     HINSTANCE hPrevInst,
                     LPSTR lpszArgs,
                     int nWinMode
                   )
{
 FILE *fdef;
 int i,j,k;
 MSG msg;
 WNDCLASS wcl;
 char *poo;
 char str[100],st2[256],*po;
 char **argv;

 hwpar = NULL;

 sprintf(st2,"0x%8.8X 0x%8.8X %i",hThisIn,hPrevInst,nWinMode);

 strcpy(lpszArgs_,lpszArgs);
 hThisInst = hThisIn;

 SetPathes();

 if(strstr(lpszArgs_,"-i")&&strstr(lpszArgs_,"-f2")) fHidden=1;

 wcl.hInstance = hThisInst;
 wcl.lpszClassName = szWinName;
 wcl.lpfnWndProc = WindowFunc;
 wcl.style = CS_DBLCLKS;
 wcl.hIcon = LoadIcon(NULL,IDI_APPLICATION);
 wcl.hCursor = LoadCursor(NULL,IDC_ARROW);
 wcl.lpszMenuName = NULL;
 wcl.cbClsExtra = 0;
 wcl.cbWndExtra = 0;
 wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
 if(!RegisterClass(&wcl)) return 0;

 DDX = 20;
 DDY = 10;

 CreateShell("RW1 Console");
 WriteShell("Robot Warfare 1 v" VERSION " (" __DATE__ ")\n"); 
 WriteShell("Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>\n");
 WriteShell("For more details please visit http://rwar.net\n\n");

 argv = (char**)malloc(sizeof(char*)*100);
 if(argv==NULL) exit(-1);
 for(i=0;i<100;i++) 
 {
   argv[i] = (char*)malloc(sizeof(char)*100);
   if(argv[i]==NULL) exit(-1);
 }

 strcpy(argv[0],"rw1_win.exe");
 i = 1;
 j = 0;
 k = 0;
 poo = lpszArgs_;
 while(*poo)
 {  
    if(!k && *poo==' ')
    {
       while(*poo==' ') poo++;
       argv[i++][j] = 0;
       j = 0;
       continue;
    }
    if(*poo=='"')
    {
       if(k){k=0;argv[i++][j]=0;j=0;}
       else k=1;
       poo++;
       continue;
    }  
    argv[i][j++] = *poo;
    poo++;
 }
 if(j!=0) argv[i++][j] = 0;
#if 0
 fdef = fopen("arg","wt");
 for(j=0;j<i;j++) fprintf(fdef,"|%s|\n",argv[j]);
 fclose(fdef);
#endif
 if(i==1) fEmpty=1;
 else rw1_init(i,argv);
 for(i=0;i<100;i++) free(argv[i]);
 free(argv);

 maxX = DDX*RW1SIZE;
 maxY = DDY*RW1SIZE;
 winX = maxX; // ???
 winY = maxY; // ???
 begX = begY = 0;

 i = 0;
 strcpy(st2,"rw1_win.def");
 fdef = fopen(st2,"rt");
 while(fdef==NULL)
 {
    sprintf(st2,"%s\\rw1_win.def",path);
    fdef = fopen(st2,"rt");
    Sleep(100);
    if(++i==10) break;
 }
 if(fdef!=NULL)
 {
    fgets(str,100,fdef);
    po = strtok(str,":\n");
    if(po!=NULL) winX += atoi(po);
    po = strtok(NULL,":\n");
    if(po!=NULL) winY += atoi(po);
    fclose(fdef);
 }
 unlink(st2);

 hMainWnd = CreateWindow ( szWinName, szWinName,
                           WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX
                           //|WS_VSCROLL|WS_HSCROLL,
                           ,CW_USEDEFAULT, CW_USEDEFAULT,
                           winX, winY,
                           HWND_DESKTOP, NULL,
                           hThisInst, NULL );

// InitCommonControls();

 if(fHidden)
   ShowWindow(hMainWnd,SW_MINIMIZE);
 else 
   ShowWindow(hMainWnd,nWinMode);
 UpdateWindow(hMainWnd);

 while(GetMessage(&msg,NULL,0,0))
 {  TranslateMessage(&msg);
    DispatchMessage(&msg);
 }
 return msg.wParam;
}

LRESULT CALLBACK WindowFunc( HWND hwnd,
                             UINT message,
                             WPARAM wParam,
                             LPARAM lParam
                           )
{
 int i,j,ii,jj,i1,i2,j1,j2;
 char *poo,str[1000],s1[8];
 FILE *fdef;
 HDC hDC;
 RECT rect;
 HBRUSH hOldBrush;
 PAINTSTRUCT ps;
 static int selectX,selectY;
// static int lastX,lastY;
 switch(message)
 {
   case WM_CREATE:
        hDC = GetDC(hwnd);
        memDC = CreateCompatibleDC(hDC);
        hBit = CreateCompatibleBitmap(hDC,maxX,maxY);
        SelectObject(memDC,hBit);
        hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
        SelectObject(memDC,hBrush);
        PatBlt(memDC,0,0,maxX,maxY,PATCOPY);
        for(i=0;i<5;i++)
        {
          sprintf(s1,"PILE%i",i+1);
          hbPile[i] = LoadBitmap(hThisInst,s1);
          hdPile[i] = CreateCompatibleDC(hDC);
          SelectObject(hdPile[i],hbPile[i]);
        }
        hbEmpty = LoadBitmap(hThisInst,"EMPTY");
        hdEmpty = CreateCompatibleDC(hDC);
        SelectObject(hdEmpty,hbEmpty);
        for(i=0;i<5;i++)
        {
          sprintf(s1,"STONE%i",i+1);
          hbStone[i] = LoadBitmap(hThisInst,s1);
          hdStone[i] = CreateCompatibleDC(hDC);
          SelectObject(hdStone[i],hbStone[i]);
        }
        hbReactor = LoadBitmap(hThisInst,"REACTOR");
        hdReactor = CreateCompatibleDC(hDC);
        SelectObject(hdReactor,hbReactor);
        hbHole = LoadBitmap(hThisInst,"HOLE");
        hdHole = CreateCompatibleDC(hDC);
        SelectObject(hdHole,hbHole);
        hbFire = LoadBitmap(hThisInst,"FIRE");
        hdFire = CreateCompatibleDC(hDC);
        SelectObject(hdFire,hbFire);
        hbBall = LoadBitmap(hThisInst,"BALL");
        hdBall = CreateCompatibleDC(hDC);
        SelectObject(hdBall,hbBall);
        ReleaseDC(hwnd,hDC);
        scrX = GetSystemMetrics(SM_CXSCREEN);
        scrY = GetSystemMetrics(SM_CYSCREEN);
        GetClientRect(hwnd,&rect);
        i = winX-rect.right+rect.left;
        j = winY-rect.bottom+rect.top;
        fdef = fopen("rw1_win.def","wt");
        if(fdef!=NULL)
        {
           fprintf(fdef,"%i:%i\n",i,j);
           fprintf(fdef,"%i\n",hwnd);
           fprintf(fdef,"%i:%i\n",winX,winY);
           fclose(fdef);
        }
        if(!fEmpty) 
        {
           rw1_draw();
           SetTimer(hwnd,TIMER_ID,TIMER_MS,NULL);
        }
//        CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)rw1_step,
//                     (LPVOID)NULL,0,&rw1_step_id);
        break;
   case WM_LBUTTONDBLCLK:
        ii = (LOWORD(lParam)+begX)>>SBIT;
        jj = (HIWORD(lParam)+begY)>>SBIT;
        sprintf(str,"WM_LBUTTONDBLCLK %i:%i\n",ii,jj);
        RSend(ii,jj);
        break;
   case WM_LBUTTONDOWN:
        selectX = ((LOWORD(lParam))>>SBIT)<<SBIT;
        selectY = ((HIWORD(lParam))>>SBIT)<<SBIT;
        break;
   case WM_LBUTTONUP:
        i = ((LOWORD(lParam))>>SBIT)<<SBIT;
        j = ((HIWORD(lParam))>>SBIT)<<SBIT;
        if(i==selectX||j==selectY) break;
        if(i>selectX)
        {
           i1 = (selectX+begX)>>SBIT;
           i2 = (i+begX)>>SBIT;
        }
        else
        {
           i2 = (selectX+begX)>>SBIT;
           i1 = (i+begX)>>SBIT;
        }
        if(j>selectY)
        {
           j1 = (selectY+begY)>>SBIT;
           j2 = (j+begY)>>SBIT;
        }
        else
        {
           j2 = (selectY+begY)>>SBIT;
           j1 = (j+begY)>>SBIT;
        }
        //sprintf(str,"%i:%i-%i:%i",i1,j1,i2,j2);
        //WriteShell(str);
        RSelect(i1,j1,i2,j2);
        break;
//   case WM_RBUTTONDOWN:
//        lastX = LOWORD(lParam);
//        lastY = HIWORD(lParam);
//        break;
   case WM_MOUSEMOVE:
        if(wParam & MK_LBUTTON)
        {
           i = ((LOWORD(lParam))>>SBIT)<<SBIT;
           j = ((HIWORD(lParam))>>SBIT)<<SBIT;
           hDC = GetDC(hwnd);
           hOldBrush = (HBRUSH)SelectObject(hDC,GetStockObject(HOLLOW_BRUSH));
           if(i>selectX)
              ii = i+RW1SIZE;
           else
              ii = i;
           if(j>selectY)
              jj = j+RW1SIZE;
           else
              jj = j;
           Rectangle(hDC,selectX ,selectY,ii,jj);
           SelectObject(hDC,hOldBrush);
           ReleaseDC(hwnd,hDC);
        }
/*
        if(wParam & MK_RBUTTON)//!!!
        {
           i = LOWORD(lParam);
           j = HIWORD(lParam);
           begX += i-lastX;
           begY += j-lastY;
           lastX = i;
           lastY = j;
           begX = begX/SCROLLSTEP*SCROLLSTEP;
           begY = begY/SCROLLSTEP*SCROLLSTEP;
        }
*/
        InvalidateRect(hwnd,NULL,0);
        break;
   case WM_PAINT:
        hDC = BeginPaint(hwnd,&ps);
//        if(!JobStep) {
        if(fEmpty)
        {
           strcpy(str,Options);
           j = 0;
           poo = strtok(str,"\n");
           while(poo!=NULL)
           {
              TextOut(hDC,0,j,poo,strlen(poo));
              j += 16;
              poo = strtok(NULL,"\n");
           }
        }
        else BitBlt(hDC,begX,begY,maxX,maxY,memDC,0,0,SRCCOPY);
/*
        GetClientRect(hwnd,&rect);
        i = rect.right - rect.left;
        j = rect.bottom - rect.top;
        sprintf(str,"%i,%i,%i,%i - %i %i\n",
                rect.left,rect.top,rect.right,rect.bottom,i,j);
        WriteShell(str);
        if(i!=maxX||j!=maxY)
        {
           if(i>maxX) i--;//(i-maxX)>>1;
           if(i<maxX) i++;
           if(j>maxY) j--;//(j-maxY)>>1;
           if(j<maxY) j++;
           SendMessage(hwnd,WM_SIZE,SIZENORMAL,i|(j<<16));
        }
*/
        SleepStep = 0;
//        }
        EndPaint(hwnd,&ps);
        break;
   case WM_TIMER:
        if(fReg) break;
        if(TimerJob) break;
        TimerJob = 1;
        globTick++;
        rw1_step(0);
        if(f_f)
        {
           if(f_f<100) for(i=0;i<f_f;i++) rw1_step(0);
           else for(i=0;i<100;i++) rw1_step(0);
        }
        if(EndFight) SendMessage(hwnd,WM_DESTROY,0,0);
        TimerJob = 0;
        InvalidateRect(hwnd,NULL,0);
        break;
   case WM_CHAR:
//           itoa(wParam,ts,10);
//           MessageBox(hwnd,ts,"Char",MB_OK|MB_ICONEXCLAMATION);
        switch(wParam)
        {
           case 1:  // Ctrl-A
             for(i=0;i<NUM;i++)
             {
                 selrobot[i] = 1;
             //    RShow(i);
             }
             break;
        }
        break;
   case WM_KEYDOWN:
        switch(wParam)
        {
           case 112: // F1
                MessageBox(hwnd,
                "RW1 Arena v" VERSION " (" __DATE__ ")\n\n"
                "Copyright (c) 1998-2013 A.A.Shabarshin\n"
                "All rights reserved\n\n"
                "Part of Robot Warfare 1 Game ( http://rwar.net )\n",
                "About",MB_OK|MB_ICONINFORMATION);
                break;
        }
        break;
   case WM_MYCOM:
        switch(wParam)
        {
          case 0: // move watch window
/*
            if(f_v)
            {
               MoveWindow(hwvar,LOWORD(lParam),HIWORD(lParam),
                                320,scrY-HIWORD(lParam),TRUE);
            }
*/
            break;
          case 1: // set parent hwnd
            hwpar = (HWND)lParam;
            break;
          case 2: // make step

            break;
          case 3: // stop me
            SendMessage(hwpar,WM_MYCOM,0,0);
            break;
        }  
        break;
   case WM_DESTROY:
        if(!fEmpty) KillTimer(hwnd,TIMER_ID);
        for(i=0;i<5;i++)
        {
          DeleteObject(hbPile[i]);
          DeleteObject(hbStone[i]);
        }
        DeleteObject(hbEmpty);
        DeleteObject(hbReactor);
        DeleteObject(hbHole);
        DeleteObject(hbFire);
        DeleteObject(hbBall);
        DeleteDC(memDC);
        for(i=0;i<5;i++)
        {
          DeleteDC(hdPile[i]);
          DeleteDC(hdStone[i]);
        }
        DeleteDC(hdEmpty);
        DeleteDC(hdReactor);
        DeleteDC(hdHole);
        DeleteDC(hdFire);
        DeleteDC(hdBall);
        PostQuitMessage(0);
        break;

//   case WM_CLOSE:
//        NeedStop = 1;
//        DestroyWindow(hwnd);
//        break;

   default:
        return DefWindowProc(hwnd,message,wParam,lParam);
 }
 return 0;
}
