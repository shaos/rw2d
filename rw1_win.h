#ifndef __RW1_WIN_H
#define __RW1_WIN_H

#define TILE_PILE1    1
#define TILE_PILE2    2
#define TILE_PILE3    3
#define TILE_PILE4    4
#define TILE_PILE5    5
#define TILE_EMPTY    6
#define TILE_STONE1   7
#define TILE_STONE2   8
#define TILE_STONE3   9
#define TILE_STONE4  10
#define TILE_STONE5  11
#define TILE_REACTOR 12
#define TILE_HOLE    13
#define TILE_FIRE    14
#define TILE_BALL    15

#ifdef __cplusplus
extern "C" {
#endif
int WriteShell(char* str);
int WriteShellInt(int i);
int WriteFatal(char* str);
int WritePixel(int x, int y, long c);
int WriteSprite(char* image);
int WriteTile(int x, int y, int t, int s);
#ifdef __cplusplus
}
#endif

#endif
