bcc=c:\borland
bin=$(bcc)\bin
cc=$(bin)\bcc32
rc=$(bin)\brcc32
link=$(bin)\ilink32
cflags=-c -K -I$(bcc)\include
lflags=-Tpe -aa -m -L$(bcc)\lib
rcflags=-i$(bcc)\include
libs=cw32.lib import32.lib
objs=rw1_win.obj robotwar.obj rw1_duel.obj my_text.obj

all: rw1_win.exe rw1_comp.exe

clean:
    del *.obj
    del *.res
    del *.map
    del *.exe
    del *.tds
    del *.i*

# Update the resource if necessary

rw1_win.res: rw1_win.rc rw1_win.h
    $(rc) $(rcflags) rw1_win.rc

# Update the object file if necessary

rw1_win.obj: rw1_win.c rw1_win.h
    $(cc) $(cflags) rw1_win.c

robotwar.obj: robotwar.c robotwar.h
    $(cc) $(cflags) robotwar.c

rw1_duel.obj: rw1_duel.c rw1_duel.h
    $(cc) $(cflags) rw1_duel.c

my_text.obj: my_text.c my_text.h
    $(cc) $(cflags) my_text.c

rw1_comp.exe: rw1_comp.c robotwar.c my_text.obj
    $(cc) -I$(bcc)\include -L$(bcc)\lib rw1_comp.c my_text.obj

rw1_win.exe: $(objs) rw1_win.res
    $(link) $(lflags) c0w32.obj $(objs),rw1_win.exe,rw1_win.map,$(libs),,rw1_win.res
