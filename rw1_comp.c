/*
 Robot Warfare 2D
 ================

 Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#define _WITHOUT_INTERPRETER
#include "robotwar.c"

int main(int argc,char **argv)
{
   char *po;
   Robot *r;
   printf("\nRobot Warfare 1 Classic Compiler v1.999 (c) 1998-2013 A.A.Shabarshin\n");
   if(argc<2)
   {
      printf("\nUsage:\n");
      printf("\n   RW1_COMP.EXE  FILE.RW1\n\n");
      exit(1);
   }
   po = argv[1];
   r = RobotNew(0,po);
   RobotSave(r,po);
   return 0;
}
